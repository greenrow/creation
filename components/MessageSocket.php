<?php
namespace app\components;

Class MessageSocket{
        
    protected $socket;
    protected  $main_arr_to_send=[];
    
    private static $instance=null;
    private $count = 0;

    protected  function __construct()
    {
      
        $this->socket= socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
         error_reporting('E_ALL');
          socket_connect($this->socket,"127.0.0.1",7777);
        if (!$this->socket) {
        echo "error connection";
       }
     
    }
    



    public static function singleton()
    {
       if (null === self::$instance){
            echo 'Создание нового экземпляра.';
         
            self::$instance = new self();
            
        }
        return self::$instance;
    }

    public function increment()
    {
        return $this->count++;
    }

    public function __clone()
    {
        trigger_error('Клонирование запрещено.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Десериализация запрещена.', E_USER_ERROR);
    }
    
    public function send_message($friend_id,$user_id,$message){
          $this->main_arr_to_send['user_id']=$user_id;
         $this->main_arr_to_send['fr_id']=$friend_id;
          $this->main_arr_to_send['text_message']=$message;
    
         $data=json_encode($this->main_arr_to_send);
            //get data om scket//
         $send_data=socket_write($this->socket,$data);


    }


}