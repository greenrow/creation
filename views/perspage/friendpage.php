<?php
use app\assets\Settings;
$this->title='Личная страница '.$nick;
$this->params['breadcrumb'][] =$this->title;
$this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyDCwmNsO67x5VPyYEKfTEGUHU--6AfVtyQ&libraries=places',['position'=>Yii\web\View::POS_END,'depends' =>'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/googlemap.js',['depends' => '\app\assets\SecAsset']); 
Settings::register($this);
?>

<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet"> 
<style>
    .label{font-size:0.9em;}
    .bootstrap-tagsinput{text-align:left;}
</style>


<div id='user_page' data-user-nick="<?php echo $nick;?>" class=" clear col-xs-12">
 

    <div <?php if($userinfo_arr['background'] != NULL){ ?> data-background-path="<?php echo  $userinfo_arr['background'];?>" style="background:url(<?php echo  $userinfo_arr['background'];?>)  center/cover"<?php } ?> class="user_profile_header<?php if($userinfo_arr['background'] == NULL){ ?> user_profile_fon <?php } ?>">
        
        
        <?php if($owner){?>
        
        <p class="  edit_userpage_fon "><span class="glyphicon glyphicon-picture"></span>Изменить фон <input type='file' class='edit_userpage_fon_input' /></p>
            <p class="reset_userpage_fon"><span class="glyphicon glyphicon-picture"></span>Сбросить фон</p>
                   <p id="edit_user_info"><span class="glyphicon glyphicon-edit"> Редактировать информацию</span></p>
            
        <?php }else{
           
            if($friend_status==1){  $message_par='Удалить из друзей';
                
            }else if($friend_status==0) {$message_par='';}
            else{ $message_par='Добавить в друзья';}?>
              <p id="userpage_nav"><span data-user-id="<?php echo $owneruserid;?>" class=" open_message_area"> Написать сообщение</span><span data-user-id="<?php echo $owneruserid;?>" data-friend-status="<?php echo  $friend_status ?>" class="user_add_friend"><?php echo $message_par ;?></span><span data-user-page-nick='<?php  echo $nick;?>' class="subscribe">Подписаться</span></p>
       <?php }?>
 
    
                
        <div class="userpage_foto">
     
                 <p class='user_nickname' data-user-id='<?php echo $owneruserid;?>'><?php echo $nick?></p>
                    <div class="user_profile_ava cropfoto_area">

                        <img  alt="" src="<?php echo $userinfo_arr['ava'];?>"/>

                    </div>
       </div>
        <div class='addfriend_mess_wrap'>

        </div>
    
     </div>
<div class="user_info col-xs-12">
    <div class='userpage_menu_wrap'>
<div class="project_header_top_adaptive_userpage userpage_menu" >
    
    <div>
           <div class="items_nav">
                   <span class="perpage_info active ">Личная информация</span>
                         
                       <span class="perspage_projects"> Проекты</span>
        
   
  
                
            </div>
    </div>
</div>
    </div>
     
     <div class="profile_content_wrap container_content_menu ">
     
               <div class='user_profile_all_informaton_wrap hide show' >
                <!--this div is hide. show only when edit pers info-->
      
                    
                 
                   <div class="personalinfo">
                           <h4> Личная информация</h4>
                       <?php 
                     
                       if(sizeof($userinfo_arr)>0){ 
                           
                 
                           $item='';
                          
                           ?>
                
               
                      
                       
             <div class='input_user_info '>
                    
<?php 

foreach($userinfo_arr as $key=>$value){
    
       switch ($key){

           case 'name':
                $item='Имя';$inputtype='text'; break;
           case 'surname':
                $item='Фамилия';$inputtype='text'; break;
           case 'age':
                $item='возраст';
                $inputtype='text';break;
           case 'website':
                $item='Сайт';$inputtype='url'; break;
           case 'email':
                $item='Email'; $inputtype='email';break;

           default:  $item='';
       }
       
      
       
             if( $item !='')  {?>   
                                          <div class=" col-xs-12 input-group user-setting_input">
                                                       <div class="noedit_userinfo_symv noedit_userinfo input-group-addon" ><?php echo $item;?> </div>
                                                       <input disabled name='user-<?php echo $key?>'  value="<?php echo $value ?>" type="<?php echo $inputtype; ?>" id="user_<?php echo $key?>" class=" noedit_userinfo  form-control" aria-describedby="basic-addon2">
                                         </div>
                       

            <?php }


     } ?>

                        </div>
                          <?php   }?>
                           
                          
                 
                   </div>  
                 
                   <div class="interests">
                           <h4> Интересы</h4>
                           
                         <div class='interes_div '>
                                <p class="hideinteres_input interes_hide">Ваши интересы (введите текст и нажмите Enter)</p>

                            <div class="interes_val">
                              
                                <?php 

                                if(sizeof($interests)>0){
         
                                   foreach($interests as $key){

                                       ?> <span data-tag-id = "<?php echo  $key['id'];?>"  class="tag label label-info pers_page_interes"><?php echo $key['interes_val'] ?><span class="hideinteres_input_inline removetag" data-role="remove"></span></span><?php
                                   }

                                }else{ ?> <p class='noinfo'>Информация не найдена</p> <?php }?>
                          
             
                           </div>        

                                <p  value=''  class="hideinteres_input form-control" ></p>
                        </div>
                           
                           
                   </div>
                   <div class="location ">
                       <h4> Локация</h4>
                       
   <p class='hide_location_input'>Укажите ваш город</p>

   <p class='hide_location_input'><input  id="user_city" type="text" /> </p>
      
                       
                        <?php if (sizeof($location) > 0){?>

                           <p class="perspage_city_name"><?php echo $location['location']?></p>
                            <div  class='col-xs-12 map_wrap' >    
                                 <span class="hidden lat_val" ><?php echo $location['lat']?></span> <span class="hidden lng_val" ><?php echo $location['lng']?></span><span class="hidden" id="location_val" ></span>
                              <div id="map-canvas"></div>
                            </div>


                        <?php } else{ ?>
                        <p>Информация не найдена</p><?php }?>
                      
                   </div>

     
                </div>
 
                <div class='user_profile_projects_wrap col-xs-12 hide '>
          
                    <div class='userpage_projects clear'>
                        <div id='project_list' class="projects"></div>
                
            
                    </div>
                </div>
         
</div>
    <div class='hide mybtn btn_bottom_right cancel_user_info'>
               Закрыть
                </div>
<div class='hide mybtn btn_bottom_right save_user_info'>
               Сохранить
                </div>
</div>
          
        </div>

