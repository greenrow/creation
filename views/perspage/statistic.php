            


<script>
    document.addEventListener("DOMContentLoaded",function(){
                    if(document.querySelector('.user_statistic') != undefined){document.querySelector('.user_statistic').parentNode.classList.add('user_menu_div_click')};

 
    })
      </script>
      <style>
          .charts-tooltip{color:grey;font-weight:bold;padding:15px;}

      </style>

<?php 
    
    use yii\helpers\Url;

        $this->title='Статистика';
        
        $this->params['breadcrumb'][] = $this->title;  ?>     


      <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

   
     <div class="project_header_top_adaptive">
         <div>
             <div class="items_nav hide">
                 <span class="active">Проекты</span>
                 <span>Категории</span>
             </div>
         </div>
     </div>

    <div id='main_statistic_wrap' class='container_content' >
  
        <div class='show' id="chart_div">

        </div>

            <div  id="colls_div">

        </div>
    </div>
       <script type="text/javascript">
        
        //alter fon for main page layer//


    // Load the Visualization API and the piechart package.
    

    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
         $.getJSON('/perspage/chartsdata',function(jsonData){
          
          var max_count_categ=jsonData.max_count_categ;
          var pie=jsonData['pie'];
          var colls=jsonData['colls'];
          

                    // Create our data table out of JSON data loaded from server.
       
         
           if(pie){
                   var data = new google.visualization.DataTable(pie);
                    var opt = {
                chartArea:{width:'90%',left:'10px'},
                          backgroundColor:{fill:'#fbfbfb',stroke:'none'},
                          colors:['#984dab','green'],
                          legend:{textStyle:{color:'#878787'},position:'right'},

                     pieSliceText: 'value' 

                 };

                    // Instantiate and draw our chart, passing in some options.
                    var item=document.getElementById('chart_div');
                    var chart = new google.visualization.PieChart(item);
 
//                 
                    chart.draw(data, opt);
                       
           }
           
            if(colls){
                    var data1 = new google.visualization.DataTable(colls),
                    gridcount;
                    if (max_count_categ>12){
                        gridcount=8
                    }
                    else if (max_count_categ>8){
                        gridcount=6
                    }
                    else{gridcount=4}
                 
                     var opt1 = {

                        legend:{position:'none'},
                       backgroundColor:{fill:'none',stroke:'none'},
                       chartArea:{width:'70%',left:'25%',padding:"20",height:'250'},
                       hAxis:{title:'Количество проектов',  gridlines:{count:gridcount},  textStyle:{color:"#473a71"},titleTextStyle:{fontSize:14,bold:1,color:"#34363f"},viewWindow:{min:0}},
                       vAxis:{title:'Категории',     textStyle:{color:"#878787"},titleTextStyle:{color:"#34363f",bold:1,fontSize:14}},
                      

                  };

                     // Instantiate and draw our chart, passing in some options.
         
                    var chart1 = new google.visualization.BarChart(document.getElementById('colls_div'));
                    chart1.draw(data1, opt1);
             
                    
               
           }
    
          
      })
              

    }


</script>