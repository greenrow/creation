<?php
$this->title='Ваши друзья';



$this->params['breadcrumb'][] =$this->title;


?>

<script>
    document.addEventListener("DOMContentLoaded",function(){
    document.querySelector('.user_friends').parentNode.classList.add('user_menu_div_click');


    })
       
</script>

<div class="project_header_top_adaptive ">
    
    <div>

         <div data-length='3' class='items_nav hide'>
              <span class="added_friends_all active">Все</span>
      <span class="added_lately_list">Недавно добавленные</span>
             <span class="accept_friend_show">Запросы на добавление</span>          
             <span class="request_friend_show">Ваши запросы</span>
         </div>
    </div>
</div>
 
        
<div class='container_content ' id="friendlist">
              <p class='friend_list_request_nav  input_nav'>
            <input  type="search" placeholder='Введите имя друга' />
         </p>

        <div id='friend_list_accept' class='col-xs-12 show'>
                
        </div>
       

        <div class='added_lately_friend_list col-xs-12 ' >

        </div>
        <div class='accept_friend_list col-xs-12'>


        </div>

        <div class='request_friend_list col-xs-12'>


        </div>

 </div>
    

<script>

    function getAjax(){
          var xmlhttp;
          try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
          } catch (e) {
            try {
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
              xmlhttp = false;
            }
          }
          if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
            xmlhttp = new XMLHttpRequest();
          }
          return xmlhttp;
}

var httpRequest=getAjax();
  var url='/perspage/friendslist',
  friend_request_to_me='',
  current_friends='',
  request_friend='',
  lately_friend='';

httpRequest.onreadystatechange=function(){
     
      if (httpRequest.status == 200 && httpRequest.readyState ==4) {
                  
                var data= JSON.parse(httpRequest.responseText);
                var accept_data=data['accept_friends'];
                var request_data=data['request_friends'];
                var lately_friends=data['lately']
                var accept_data_length=accept_data.length;
                var request_data_length=request_data.length;

                
                if(accept_data_length>0){
                    for(var i=0;i<accept_data_length;i++){

                        if(accept_data[i]['submit_friend']=='0'){
                            friend_request_to_me+='<div class="peoples_item"><div class="peoples_item_ava" style=" background:url('+ accept_data[i]["ava"]+') center/contain no-repeat"> </div> <p><a target="_blank" href="/users/'+accept_data[i]['nickname']+'">'+accept_data[i]['nickname']+'</a></p><p><span class="accept_friend glyphicon glyphicon-ok"></span><span class="cancel_friend glyphicon glyphicon-remove"></span></p></div>';

                        }
                        else{

                            current_friends+='<div class="peoples_item"><div class="peoples_item_ava" style=" background:url('+ accept_data[i]["ava"]+') center/contain no-repeat"> </div> <p><a  class="curent_friend_nick" target="_blank" href="/users/'+accept_data[i]['nickname']+'">'+accept_data[i]['nickname']+'</a></p></div>';
                        }

                    }
                 }
            
                 if(request_data_length>0){
                    for(var i=0;i<request_data_length;i++){

                        if(request_data[i]['submit_friend']=='0'){
      
                            request_friend+= '<div class="peoples_item"><div class="peoples_item_ava" style=" background:url('+ request_data[i]["ava"]+') center/contain no-repeat"> </div> <p><a target="_blank" href="/users/'+request_data[i]["nickname"]+'">'+request_data[i]["nickname"]+'</a></p></div>';
                        }

                      
                        else{
                           
                                                       current_friends+='<div class="peoples_item"><div class="peoples_item_ava" style=" background:url('+ request_data[i]["ava"]+') center/contain no-repeat"> </div> <p><a class="curent_friend_nick"  target="_blank" href="/users/'+request_data[i]["nickname"]+'">'+request_data[i]["nickname"]+'</a></p></div>';
                        }

                    }
                 }
                 
                 if(lately_friends.length>0){
                    var length=lately_friends.length
                    for(var i=0;i<length;i++){
                
                           lately_friend+='<div class="peoples_item"><div class="peoples_item_ava" style=" background:url('+ lately_friends[i]["ava"]+') center/contain no-repeat"> </div> <p><a target="_blank" href="/users/'+lately_friends[i]["nickname"]+'">'+lately_friends[i]["nickname"]+'</a></p></div>';
                        }

                 }
        }
        
       
    }
    
    
    
    
    

httpRequest.open('GET',url,true);
httpRequest.send();

window.onload=function(){
//var list=document.querySelectorAll('.friends_lists_main>div');
//
//Array.prototype.forEach.call(list,function(el,i){
//    
//    el.addEventListener('click',function(e){
//        var target=e.target;
//        while(!target.classList.contains('friend_div')){
//          target=target.parentNode;
//        }
//        if(target ){
//            var friend_nick=target.getAttribute('data-friend-nickname');
//
//            location.href='/users/'+friend_nick
//
//
//        }
//
//    })
//})
//        
                

    document.querySelector('.accept_friend_list').innerHTML=friend_request_to_me;
    document.querySelector('.request_friend_list ').innerHTML=request_friend;
    document.getElementById('friend_list_accept').innerHTML=current_friends;
    
    document.querySelector('.added_lately_friend_list').innerHTML=lately_friend;
          
      

}

</script>