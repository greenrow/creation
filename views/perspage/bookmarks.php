<?php
$this->title='Ваши закладки';
$this->params['breadcrumb'][] =$this->title;
?>

<script>
    document.querySelector('.user_bookmarks').parentNode.classList.add('user_menu_div_click');

</script>

<div class="project_header_top">
    
    <div>
           <div class="items_nav" >
                   <span class="bookmark_peoples active ">Люди</span>
                         
                       <span class="bookmark_projects"> Проекты</span>
        
   
                      <span class="bookmark_likes"> Что вам понравилось  </span>
                
            </div>
    </div>
</div>

<div id="bookmarks" class="container_content">
   
           



               <div class="bookmark_peoples_content show">
                     <?php if(sizeof($subscribers) > 0){
                         
                         foreach($subscribers as $key){?> 
                     <div class="bookmarks_peoples_item_wrap">
                           <div class="peoples_item">
                               <div class="peoples_item_ava" style=" background:url(<?php echo $key['ava']?> ) center/contain no-repeat;">

                               </div>
                               <p><?php echo $key['nickname']?></p>
                           </div>
                     </div>
                             
                         <?php } ?>
                          
                    <?php }?>
               </div>
               
                <div class="bookmark_projects_content">
                      <?php if (sizeof($bookmarks) >0){
                          foreach($bookmarks as $key){?>     
                      <div class="bookmarks_bookmarks_item_wrap">
                                   <div class="bookmarks_bookmark_item ">
                                       <div style=" background:url(<?php echo $key['ava']?> ) center/cover no-repeat;"class='bookmark_project_foto'>
                                           
                                       </div>
         
                                <p class='bookmark_project_name'><?php echo $key['name']?></p>
                           
                                      
                                   </div>
                                   
                               
                                   <div class="bookmark_author">
                                         <p><?php echo $key['nickname']?></p>
                                         <p><img width='65%' src="<?php echo $key['userava'];?>" /></p>
                                   </div>
                             </div>

                       <?php }  ?>    <?php }  ?>
                          
        
               </div>
               
                <div class="bookmark_likes_content">
                                            <?php if (sizeof($likes) >0){
                          foreach($likes as $key){?>     
                               <div class="bookmarks_bookmarks_item_wrap">
                                   <div class="bookmarks_bookmark_item ">
                                       <div style=" background:url(<?php echo $key['ava']?> ) center/cover no-repeat;"class='bookmark_project_foto'>
                                           
                                       </div>
         
                                <p class='bookmark_project_name'><?php echo $key['name']?></p>
                           
                                      
                                   </div>
                                   
                               
                                   <div class="bookmark_author">
                                         <p><?php echo $key['nickname']?></p>
                                         <p><img width='65%' src="<?php echo $key['userava'];?>" /></p>
                                   </div>
                             </div>

                       <?php }  ?>    <?php }  ?>
                          
               </div>
 
    

    
</div>