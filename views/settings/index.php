<?php 

    $this->title='Настройки';
$this->params['breadcrumb'][] =  $this->title;    
$this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyDCwmNsO67x5VPyYEKfTEGUHU--6AfVtyQ&libraries=places',['position'=>Yii\web\View::POS_END,'depends' =>'yii\web\YiiAsset']);

     $this->registerJsFile(Yii::$app->request->baseUrl.'/js/googlemap.js',['depends' => '\app\assets\SecAsset']); ?>

            <script>
document.addEventListener("DOMContentLoaded",function(){
    document.querySelector('.user_settings').parentNode.classList.add('user_menu_div_click');
})
      </script>
      <style>
     .alter_filter_fon{opacity:0.6 !important;background-color:#777172!important}
    </style>   
    
    <div class="project_header_top_adaptive">
        <div>
            <div class="items_nav hide  " data-length='3'>
                <span class="active">Личная информация</span>
                <span>Проекты</span>
               <span>Безопасность</span>
            </div>

        </div>
    </div>

   <div class='general_settings container_content '>

 
        <div class=" disabled mybtn btn_bottom_right save_general_settings">
            Сохранить
        </div>



    
        <div class="personal_page_settings hide show">

            <p style='margin-top:40px;' class="strong">Кто может просматривать вашу личную страницу</p>
                <select id="perpages_rule" name="personalpage_show_rule">
                    <option <?php if ($perspage_rule==0){ ?>selected data-current-value<?php };?> value='all'>Все</option>
                    <option <?php if ($perspage_rule==1){ ?>selected data-current-value<?php };?> value='friend'>Только друзья</option>
                    <option <?php if ($perspage_rule==2){ ?>selected data-current-value<?php };?> value='some_friend'>Некоторые друзья</option>
                    <option <?php if ($perspage_rule==3){ ?>selected data-current-value<?php };?> value='nobody'>Никто</option>
                
                
                </select>
               <div class='selected_friend_list_wrap   <?php  if ($perspage_rule!=2){ echo ' hide';}?>'> 
            
                <p class="selected_friend_list">
                    <?php 
                    
                   if(array_key_exists('perspage',$friendlist)){
                      
                        foreach($friendlist['perspage'] as $key=>$value){?>
             
                        <span data-current-value><span><?php echo $value ;?></span><span class="delete_friendlist_item"><sup></sup></span></span>
                        <?php   
                        
                        } 
                   }?>
                  
                    
                   

                    
                    </p>
                
                <p class="<?php if ($perspage_rule!=2) echo 'hide ';?>  more_settings_friend a_style">Добавить еще</p>
               </div>
        </div>

            

         
    
    <div class="project_settings hide">
        
     
     
        
              <p style='margin-top:40px;'  class="strong">Кто может просматривать ваши проекты</p>
                <select id='project_show_rule' name="project_show_rule">
                    <option <?php if ($project_rule==0){ ?>selected data-current-value<?php };?> value='define_in_project'>Как указано в настройках проекта</option>
                    <option <?php if ($project_rule==1){ ?>selected data-current-value<?php };?> value='all'>Все</option>
                    <option <?php if ($project_rule==2){ ?>selected data-current-value<?php };?>  value='friend'>Только друзья</option>
                    <option <?php if ($project_rule==3){ ?>selected data-current-value<?php };?> value='some_friend'>Некоторые друзья</option>
                    <option <?php if ($project_rule==4){ ?>selected data-current-value<?php };?> value='nobody'>Никто</option>
                   
                </select>
                  <div class='<?php if ($project_rule !=3) echo ' hide ';?> selected_friend_list_wrap '> 
                  
                  <p class="selected_friend_list">
                  
                           <?php if(array_key_exists('project',$friendlist)){
                     
                            foreach($friendlist['project'] as $key=>$value){?>
                       
                      <span data-current-value><span><?php echo $value ;?></span><span class="delete_friendlist_item"><sup></sup></span></span>
                         <?php   } 
                  
                    
                    }?>  
                      
                  </p>
                  
                  <p class="<?php if ($project_rule!=3) echo ' hide ';?> more_settings_friend a_style">Добавить еще</p>
                  </div>
            
              
                
                <p style='margin-top:40px;'  class="strong">Кто может комментировать ваши проекты </p>
                <select id='comment_show_rule' name="comment_show_rule">
                    <option <?php if ($comment_rule==0){ ?>selected data-current-value<?php };?> value='define_in_project'>Как указано в настройках проекта</option>
                    <option <?php if ($comment_rule==1){ ?>selected data-current-value<?php };?> value='all'>Все</option>
                    <option <?php if ($comment_rule==2){ ?>selected data-current-value<?php };?> value='friend'>Только друзья</option>
                     <option <?php if ($comment_rule==3){ ?>selected data-current-value<?php };?> value='some_friend'>Некоторые  друзья</option>
                    <option <?php if ($comment_rule==4){ ?>selected data-current-value<?php };?> value='nobody'>Никто</option>
                </select>
        
              
              <div class=' <?php if ($comment_rule!=3) echo 'hide ';?> selected_friend_list_wrap '> 
                    <p class="selected_friend_list">
                        
                        
               
                    <?php if(array_key_exists('comment',$friendlist)){
                        $size=sizeof($friendlist['comment']);
                 
                        foreach($friendlist['comment'] as $key=>$value){?>
                       
                                 <span data-current-value><span><?php echo $value ;?></span><span class="delete_friendlist_item"><sup></sup></span></span>
                         <?php   } 
                  
                    
                    }?>
              
                    </p>
                    
                    
                    
                    <p class=" <?php if ($comment_rule !=3) echo 'hide';?>more_settings_friend a_style">Добавить еще</p>
                  </div>
                
             <div class='project_settings_checkbox'>
            <p style='margin-top:50px;font-weight:bold'>Вы можете удалить все ваши проекты</p>
            <p><input name='proj_del' value='del' data-current-value id='del_proj' type='radio'/> <label for='del_proj'>Удалить все проекты</label> </p>
                 <p class='clear_checkboxes'>Очистить</p>
        </div>


    </div>
        
        



        
           <div  class="hide security_settings">
            <p style='margin-top:40px;'  class="strong">Вы можете изменить пароль</p>
            <p><label for='old_passw'>Введите текущий пароль </label> <input name='old_passw' id="old_passw"  value='' type='password'/> </p>
            <p><label for='new_passw'>Введите новый пароль </label> <input disabled name='new_passw' id="new_passw"  value='' type='password'/> </p>
         

            <p><label for='new_passw_repeat'>Повторите пароль </label> <input disabled name='new_passw_repeat' id="new_passw_repeat"  value='' type='password'/> </p>
        </div>



    
       
</div>
    <div class='container_content_only_width'>
     <div class="settings_friend_list_wrap hide">
            <div class='menu_bkg'>
            <input type='search' class="settings_friend_list_search " />
            </div>
            <div class='settings_friend_list'>


                <?php foreach($friends as $key){?>
                <div  class='hover_light'><div class="settings_friend_list_nick"><?php echo $key['nickname'];?> </div>
               <div><img  width='70px' src='<?php echo $key['ava'];?>' /></div></div>
               <?php } ?>

            </div>

        </div>


    </div>