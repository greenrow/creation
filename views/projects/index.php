<script>
document.addEventListener("DOMContentLoaded",function(){
    document.querySelector('.user_projects').parentNode.classList.add('user_menu_div_click');
})



//
//document.querySelector('.proj_sort_menu>li[data-sort-type="active"]').classList.remove('hide');
//document.querySelector('.proj_sort_menu>li[data-sort-type="no_active"]').classList.remove('hide')

    
    </script>


<?php
use yii\helpers\Url;

$this->title=' Ваши проекты';
$this->params['breadcrumb'][] =  $this->title;?>     
  
            <div class="filter_main_menu">
    
            <div>

                 <div data-length="3" class="items_nav hide">
                      <span class="collection menu_item" data-index="0">Колелекции</span>
                      <span class='add_menudiv_item'>
                        <span class="sort menu_item " data-index="1">Сортировать</span>
                         <span class="date_div menu_item " data-index="2">Дата</span>
            
                      </span>
                      <span class=" hide arrow_dop_div glyphicon glyphicon-chevron-down"></span>
                 </div>
            </div>
</div>
        
<div class="header_dop_nav hide">

    <div class='header_dop_nav_filter_wrap'>
        
    </div>
    </div>
        

         <div class="col-xs-12 filter_content">
                 
                 <!--collection-->
            <div class='project_category_main'>
                 <h5 class="txt_cntr">Категории:</h5>
                <p  class="parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>

                <div class="categ_list_wrap">
                    
                    <div class='categ_list'>
                           <?php echo Yii::$app->runAction('site/showcateg');?>
                       </div>
                </div>  
            </div>
                 

                     
                     <!--calendar-->
                     
                        <div class='project_date_check'>
                                <p  class="parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>
                       
                            <h5 class="txt_cntr">Дата:</h5>
                                <ul class="proj_sort_menu col-xs-12">
                                    <li class="parent_up2_close col-xs-12 proj_date_sort" data-date="thisday">За текущий день</li>
                                    <li class="parent_up2_close col-xs-12 proj_date_sort" data-date="week">За неделю</li>
                                    <li class="parent_up2_close col-xs-12  proj_date_sort" data-date="month">За месяц</li>
                                    <li class="parent_up2_close col-xs-12 proj_date_calendar" data-date="calendar">Выбрать дату</li>
                                </ul>
               
                        </div>
                                      
                        <div class="calendar_data">
                                  <p  style='text-align:right' data-remove-class='{"alter_fon":"show"}' data-add-class='{"project_date_check":"show"}'  class="data_param parentclose"><span class='glyphicon glyphicon-remove'></span></p>
                                 
                        </div>
                     
                              <!--sort-->
                     
                        <div  class='project_sort_check '>
                             <p  data-remove-class='{"alter_fon":"show"}' class=" data_param parentclose_style"><span class='  glyphicon glyphicon-remove'></span></p>
                       
                                 <h5 class="txt_cntr">Сортировать:</h5>
                                <ul class="proj_sort_menu col-xs-12">
                                    <li class="parent_up2_close col-xs-12 proj_sort" data-sort-type="like">самые популярные</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort" data-sort-type="viewed">самые просматриваемые</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort hide" data-sort-type="active">активные</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort hide" data-sort-type="no_active">неактивные</li>
                              
                                </ul>
               
                        </div>
      
        </div>
                    

    <div class=" medium_main_area medium_main_area_user">
           
        <div class="col-xs-12 filter_info_wrap">
  
            <!--filter_rez-->
            
               <div class='filter_rezult'>  
                  <div class='project_sort_info '>

<!--date-->
                    <div class="sort_project_date close_filter_rezult_text">

                         <div class="alter_filter_fon">

                          </div>
                        <div class="sort_wrap">
<!--                            <p class='close_date_sort ' ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                            <p class='txt_cntr'>Дата</p>
                            <div class="project_time selected_filter_text"></div>
                        </div>
                     </div>

<!--loc-->

                      <div class="sort_project_loc">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_loc_sort'  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Локация</p>
                            <div class="map_project_city selected_filter_text"></div>
                          </div>
                      </div>

<!--collection-->
                      
                       <div class="sort_project_collection">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_collection_sort'  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Коллекция</p>
                            <div class="selected_main_categ_list sort_text selected_filter_text"></div>
                          </div>
                      </div>
<!--sorttype-->

            <div class="sort_project_type">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_sort_type '  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Сортировка</p>
                            <div class="selected_main_sort_type selected_filter_text"></div>
                          </div>
                      </div>
                  </div>
             </div>
        </div>


    </div>
      <div class="project_actions_wrap">
            <div id="add_project">
                <p><span  class='glyphicon glyphicon-plus-sign'> </span><br/>добавить проект</p>
            </div>
            <div class="edit_user_project">
                <p><span class='glyphicon glyphicon-edit'>
                    
                    </span><br/>редактировать</p>
            </div>
      </div>

      <div  class="  project_user_wrap" data-project='ok'>
     

          
                
           
               

            <div id="project_list" >
              
                            
             
            </div>
         
            </div>

         
    <div class='add_project_div hide'>

  </div>
          
          <div class="edit_project_wrap hide">
              <p class="parentclose_style"><span class="glyphicon glyphicon-remove"></span></p>
              <p class="edit_proj_name"></p>
              

          <div class="edit_project">
                    <p style='margin-top:40px;'  class="strong">Кто может просматривать ваши проекты</p>
                <select id='project_show_rule' name="project_show_rule">

                </select>
                    <div class="selected_friend_list_wrap hide" >
                     <p class="selected_friend_list ">

                    </p>
                    
                      <p  class=" data-project-edit more_settings_friend a_style ">Добавить еще</p>
                    </div>
        

                
              </div>
              
                            <div class="edit_comment">
             <p style='margin-top:40px;'  class="strong">Кто может комментировать ваши проекты </p>
                <select id='comment_show_rule' name="comment_show_rule">

                </select>
                   <div class="selected_friend_list_wrap hide" >
                     <p class="selected_friend_list ">

                    </p>
                            <p  class=" data-project-edit more_settings_friend a_style ">Добавить еще</p>
                    </div>
        

                
              </div>
              <div class="project_age_rule">
                    <p style='margin-top:40px;'  class="strong">Проект содержит информацию  для лиц 18+</p>
                    <input type ="checkbox" />
              </div>
              
               
               <div class="disabled mybtn btn_bottom_right save_edit_project">
                   Сохранить
               </div>
          </div>
          
          <div class="edit_projectcontent_wrap">
              
                        <div class="action_editcontent"><p class="save_edit_projectcontent a_style">Сохранить</p><p data-hide-bck='.alter_fon' class="parent_up2_close a_style">Закрыть</p></div>
    
                        <div class="edit_projectcontent_top">
                            
              <div class="project_ava">
       
                <p>Обложка проекта<p>
                <p><img class=" col-xs-12  img-rounded img-responsive" ></p>

             </div>
                            <p>Название проекта</p>
                  
                  <div class="edit_project_name">
                      
                  </div>
                                <p>Категории проекта</p>
                  <div class="edit_project_categ">
                      <div class="add_project_cat add_selected_categ"></div>
                  </div>
                                
                   <div class=" tag_div">
                                              <p>Теги проекта</p>
                        <input id="tag_edit_proj" value="" data-role="tagsinput" class="form-control" style="display: none;"/>

                </div>
 
              </div>
              <div class="edit_projectcontent_medium">
                  <div class="top_add_project_menu">


                    <div class="edit_projectcontent_menu">

                     <div class="project_foto_upload">
                         <input class="project_foto" multiple="" name="filesarr[]" type="file"> <span class="addproj_foto_text">Фото</span><br><span class="glyphicon glyphicon-picture"></span> </div>
                     <div id="proj_text" class="slidedown">  <span>Текcт</span> <br><span class="glyphicon glyphicon-font "></span>

                         </div>
                         <div id="embed_video" class="slidedown"> <span>Встроенное видео</span><br><span class="glyphicon glyphicon-chevron-left"></span>

                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </div>



                         <div class="change_pos "><span>Изменить положение</span> <br><span class="glyphicon glyphicon-sort"></span>
                         </div>
                         <div class="choose_background_proj "><span>Выбрать фон</span><br><span class="glyphicon glyphicon-adjust"></span>
                         </div>

                     </div>
                                 <div class="row pickercolor_wrap  ">

                    <div id="proj_colorpicker" ></div>
                </div>
            


                </div>
                  <div id="main_content" class="edit_content">

                  </div>
              </div>
          </div>
          
          
                     
                 
        <div class="settings_friend_list_wrap hide">
        <div class='menu_bkg'>
        <input type='search' class="settings_friend_list_search " />
        </div>
        <div class='settings_friend_list'>
            
            
            <?php foreach($friends_arr as $key){?>
            <div  class='hover_light'><div class="settings_friend_list_nick"><?php echo $key['nickname'];?> </div>
           <div><img  width='70px' src='<?php echo $key['ava'];?>' /></div></div>
           <?php } ?>
      
        </div>
        
    </div>
        

                                   <div class='add_project_category_list col-xs-10'>
                    <p  class="close_categ_list parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>

                    <div class="categ_list_wrap">

                        <div class='categ_list'>
                               <?php echo Yii::$app->runAction('/projects/showcateg');?>
                           </div>
                    </div>  
                    
                      <p  class="mybtn parentclose save_categ ">Сохранить</p>
                </div>
