  
<script>
//    document.querySelector('.medium_main_area').style.display='none';
//        document.querySelector('.main_top_background').style.display='none';


    </script>
    <?php
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/projects_js.js',['depends' => '\app\assets\SecAsset']); 
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/texteditor.js',['depends' => '\app\assets\SecAsset']); 

use yii\helpers\Url;
$this->title='Создание проекта';

$this->params['breadcrumb'][] =  [
        'label' => 'Проекты',
          'url' => ['/'.Yii::$app->user->getIdentity()->nickname.'/projects']
]; 

$this->params['breadcrumb'][] =$this->title;

?>   


                     <div id='hidden_id_project'>

        </div>
    
                            <!--categ list absolute pos on window size-->
                                   <div class='add_project_category_list col-xs-10'>
                    <p  class="close_categ_list parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>

                    <div class="categ_list_wrap">

                        <div class='categ_list'>
                               <?php echo Yii::$app->runAction('/projects/showcateg');?>
                           </div>
                    </div>  
                    
                      <p  class="mybtn parentclose save_categ ">Сохранить</p>
                </div>

    
       <div class='status_project'>

        </div>
<div  class="add_project_wrap ">
                 <div class="close_add_proj .parentclose_style">
             <span class="glyphicon glyphicon-remove"></span>   
         </div>

                <!--Обложка проекта-->
    <div class="col-xs-12 project_nav_wrap">
    <div class='project_nav '>
        <div data-step='0'  class='add_ava'>
            <div class="col-xs-12 project_nav_svg">
                <object id='svgnav_0'  type='image/svg+xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                <p>Шаг 1</p>
            </div>
           
        </div>
        <div data-step='1' class='add_proj_content  '>
              <div class="col-xs-12 project_nav_svg">
                <object    type='image/svg+xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                 Шаг 2
            </div>
        </div>
        <div data-step='2' class='addit_info'>
                   <div class="col-xs-12 project_nav_svg">
                <object type='image/svg+xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                 Шаг 3
            </div>

        </div>
        
        <div data-step='3' class='publish_proj'>
                   <div class="col-xs-12 project_nav_svg">
                <object type='image/svg+xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                 Шаг 4
            </div>

        </div>
    </div>

        <div class='next_add_project_item'>   Далее     </div>
   
             <div class='next_add_project_item_back'>   Назад</div>
         
    <div class="error_add_project hide">
            
        </div>
        

    </div>
             <!--project_content-->
     <div class='project_canvas col-xs-12'>

         <div  class='project_main_info' data-show='0' style='display:block'>
                
             <div class='project_ava'>
       
                <h5>Обложка проекта</h5>
                <p><img  class=' col-xs-12  img-rounded img-responsive'  src="/images/nofoto.png"/></p>

             </div> 
             
             <div class="project_title_desc">
      
                 <div class='row'>
                    <h5>Название</h5>
                     <input name="project_title" type='text'  class='col-xs-12 col-md-10 current_proj_title' ><br/>
                </div>
                 <div class='row'>
                    <h5>Описание</h5>
                    <textarea rows='5' name='project_desc' class='col-xs-12 col-md-10 proj_desc'></textarea></p>
                </div>
             </div>

        </div>
                        <!--содержимое проекта-->
                
        <div  class="add_project_content">
             <div class="top_add_project_menu" >

                 
                 <div class="project_menu_wrap">
                     
                 <div class="project_foto_upload">
                     <input class='project_foto' multiple name='filesarr[]' type="file"/> <span class='addproj_foto_text'>Фото</span><br/><span  class="glyphicon glyphicon-picture"></span> </input></div>
                 <div  id="proj_text" class='slidedown'>  <span>Текcт</span> <br/><span class='glyphicon glyphicon-font '></span>

                     </div>
                     <div id="embed_video" class='slidedown'> <span>Встроенное видео</span><br/><span class='glyphicon glyphicon-chevron-left'></span>

                            <span  class='glyphicon glyphicon-chevron-right'></span>
                        </div>



                     <div class='change_pos '><span>Изменить положение</span> <br/><span class="glyphicon glyphicon-sort" ></span>
                        </div>
                     <div class='choose_background_proj '><span>Выбрать фон</span><br/><span class='glyphicon glyphicon-adjust'></span>
                        </div>
                     
                 </div>


            </div>
                
            <div class="row pickercolor_wrap  ">

                <div id="proj_colorpicker" ></div>
            </div>


            <div id='main_content'  class="main_content">

           </div>
            <div class="additional_content">
                
            </div>

         </div>
                        <!--дополнительная инфа-->
         <div class="additional_project_info">
                           
                <div class="col-xs-10 col-xs-push-1 col-md-8 col-md-push-1">
    
                        <h5> Категория   </h5>
                    
              
                         <div class="col-xs-12  add_project_cat add_selected_categ"></div>
                        
              
                     


                         
     
     
                     
                </div>
                        
              <div class="col-xs-10 col-xs-push-1 col-md-8 col-md-push-1">
        
                 
                

                 <div class='col-xs-12 tag_div'>
                    <h5>Теги (введите текст и нажмите Enter)</h5>
                        <input id="tag_proj" value='' data-role="tagsinput" class="form-control" />

                </div> 
            </div>
                           
          </div>
                        

                        
                         <!--дополнительная настройки-->
        <div class="additional_project_settings">
                         
                <div class=" project_access_rules ">
                  <div class="addproject_settings">
        
     
     
        
              <p style='margin-top:40px;'  class="strong">Кто может просматривать ваши проекты</p>
                <select id='project_show_rule' name="project_show_rule">

                    <option  value='all'>Все</option>
                    <option value='friend'>Только друзья</option>
                    <option value='some_friend'>Некоторые друзья</option>
                    <option  value='nobody'>Никто</option>
                   
                </select>
                  <div class='selected_friend_list_wrap hide'> 
                  
                  <p class="selected_friend_list">

                      
                  </p>
                  
                  <p class=" more_settings_friend a_style">Добавить еще</p>
                  </div>
            
    
                
                <p style='margin-top:40px;'  class="strong">Кто может комментировать ваши проекты </p>


                <select id='comment_show_rule' name="comment_show_rule">
           
                    <option value='all'>Все</option>
                    <option value='friend'>Только друзья</option>
                     <option value='some_friend'>Некоторые  друзья</option>
                    <option  value='nobody'>Никто</option>
                </select>
        
              <div class='selected_friend_list_wrap hide '> 
                    <p class="selected_friend_list">

              
                    </p>
                    
                    
                    
                    <p class=" more_settings_friend a_style">Добавить еще</p>
                  </div>
                
                </div>
            
                <div class=" secure_settings">
                    <div class="age_project_rule">
                          <h5>
                        Проект содержит информацию  для лиц старше 18 лет.
                    </h5>
                        <p><input name='age_proj_rule' type="checkbox" ></p>
                    </div>
                </div>
          </div>
            
            
                         <div class='saveproject_div'>
            
            <div class='preshow_proj'>
              <span class='glyphicon glyphicon-eye-open'></span><br/>
                        
                         Просмотреть
            </div>
                <div  id="submit_project">
                    <span class='glyphicon glyphicon-floppy-disk'></span><br/>
                        Сохранить
                    </div>
                <div id="submit_publish">
                    <span class='glyphicon glyphicon-ok'></span><br/>Опубликовать
                   

</div>
 </div>
        </div>         
</div>
             
             
          <div class="settings_friend_list_wrap hide">
        <div class='menu_bkg'>
        <input type='search' class="settings_friend_list_search " />
        </div>
        <div class='settings_friend_list'>
            
            
            <?php foreach($friends_arr as $key){?>
            <div  class='hover_light'><div class="settings_friend_list_nick"><?php echo $key['nickname'];?> </div>
           <div><img  width='70px' src='<?php echo $key['ava'];?>' /></div></div>
           <?php } ?>
      
        </div>
        
    </div>
</div>