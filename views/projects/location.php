
      <style>
    #map-canvas {
        height: 200px;
        margin: 0px;
        padding: 0px;
      
          width:100%;
    
      }


      #user_city {
        background-color: #fff;
        padding: 0 11px 0 13px;
        width: 400px;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        text-overflow: ellipsis;
      }

    #user_city:focus {
        border-color: #4d90fe;
        margin-left: -1px;
        padding-left: 14px;  /* Regular padding-left + 1. */
        width: 401px;
      }

      .pac-container {
        font-family: Roboto;
      
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }


    </style>   

<div class="col-md-12 map">            


                             <div  class='col-md-10 col-md-push-1 map_wrap' >    
                                
                              <div style='height:300px;z-index:1000' id="map-canvas"></div>
                            </div>

        
                                           <span class="hidden lat_val"><?php echo $lat?></span> 
                                           <span class="hidden lng_val"><?php echo $lng?></span><span class="hidden location_val" ><?php echo $loc?></span>
                
                </div>
    

   
     
            
