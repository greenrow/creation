  <?php 
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <?= Html::csrfMetaTags() ?>
    <title>Регистрация</title>
    
    <style>
         body{
            background-image:url('/images/fon/wine-cork.png');
            background-color:#000a03;
        
        }

            #loginform{
            position:relative;height:500px;left:35%;background-color:#FFF;width:450px;margin-top:200px;padding:20px; background-image:url('/images/fon/white-sand.png');border-radius:10px;
            background-color:#7a3823;box-shadow:0 0 10px grey;
        }
        #regform{overflow:visible;}
        
        .login_table{color:#000;;font-weight:400}
        .login_table td{padding:10px;min-width:30px;width:auto}
      .login_table  input{width:95%;padding:10px;z-index:100;position:relative;}
        .login_table  input:hover{border:1px solid #F2EF3E}
              .label_td{width:60%;text-align:right;color:#e0cfc9}
        #getreg{left:30%;cursor:pointer;text-align:center;background-color:#EA5656;color:#fff;position:relative;border:none;border-radius:3px;width:170px;height:50px;padding:10px;}
        
    </style>
    <script>
        
        setTimeout(function(){
            
            location.href='/';
        },3000)
        </script>
            
</head>
    <body>
        <div id='loginform' clas=" col-md-3 col-md-push-4 ">
        <p class='good_reg'>Поздравляем <?php echo $login;?>,вы успешно зарегестрировались на нашем сайте!</p>
        <p><a href='/'>Перейти на главную страницу</a></p>
        </div>
      

    </body>
    </html>