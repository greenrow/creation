<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\CutAsset;

CutAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
<head>
    <?php $this->head() ?>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <?= Html::csrfMetaTags() ?>
    <title>Авторизация</title>

    <style>
 body{
height:100%;
              background-color:#fff;
        
        }
       #loginform{
           background-color:#fff;
            position:fixed;;padding:10px;
           top:calc(50% - 132px);left:0;right:0;;width:96%;max-width:500px;;
           border:1px solid #d4dde9;color:#000;margin:auto;height:264px
        }
        .form{overflow:hidden;}
        .password_recovery{margin-top:20px;}
    .login_table{color:#6E6770;;font-weight:700}
        .login_table tr  td{padding:10px;min-width:30px;}
        .login_table  input{width:95%;padding:10px;color:#6E6770;border:1px solid #b9b9b9;}

#loginform .back_href{position:absolute;right:10px;bottom:-2px;}

#getauth{float:right;width:60px}
.editional_actions{float:left;overflow:hidden;width:50%}

    </style>
    <script>
        var referrer=document.referrer;
        var referfrom_userpage=/^https?:\/\/.+\/users\/.+/i;

             
        function errorbrd(elem){
            
            elem.css('border','1px solid red')
        }
        function checkdata(){
            var err_obj='';
            var authname=$('.auth_name').val().trim();
            var authpassw=$('.auth_passw').val().trim();
            var data={'name':authname,'passw':authpassw};
            $.ajax({
                    url:'site/chekdata',
                    type:"POST",
                    data:data,
                    contentType:'application/x-www-form-urlencoded; charset=utf-8',
                    dataType:"json",
       
                    success: function(data){
            if(referrer.match(referfrom_userpage) != null){
                window.location.href=referrer;

            }


else if(data['auth']){window.location.href='/'+data['nick']}
                           if(data['auth_err']){
                               $('.auth_error').css('display','block')
                           

                              if(data['auth_err']['login']){   
                                  $('.auth_error').text(data['login']);
                                  errorbrd($('.auth_name'))
                                  $('.auth_error_text').text(data['auth_err']['login'])
                              }

                                 if(data['auth_err']['passw']){   
                                  $('.auth_error').text(data['passw']);
                                  errorbrd($('.auth_passw'));
                                              $('.auth_error_text').text(data['auth_err']['passw'])
                              }
                          }
                    }
    })
    
 
        
    }
    


    </script>
        

</head>
<body>
<?php  $this->beginBody()?>
     <div id="loginform"> 
               
            
      


        <form class='form' action="" method="POST" name="loginform">
            <table class='login_table col-xs-12'>
                <tr>
                <td>Логин</td>
                <td><input type="text" value='' name="name" class="long_field auth_name" ></td>
          
            </tr>

            <tr>
                <td>Пароль</td>
                <td><input type="password" name="passw" value='' class="long_field auth_passw"></td>
                 
                   
                
            </tr>

            <tr>
                <td>
                    <input type="hidden" value="<?=Yii::$app->request->getCsrfToken()?>" />
                </td>
            </tr>
        </table>
            <div class="auth_error">
                     <p class='parentclose_style'><span class="glyphicon glyphicon-remove"></span></p>
                <p class='auth_error_text'></p>
            
            </div>
        
   
                     <p class='mybtn' onclick='checkdata()' id='getauth'>Войти</p>
        </form>
         <div class="editional_actions">
         <div class='password_recovery'>
             <a class='a_style_dark' href="<?php echo Url::to(['/site/passwrecover/'])?>">Восстановление пароля</a>
</div>
     <div class='regit'>
            <a class='a_style_dark' href="<?php echo Url::to(['/site/register/'])?>">Регистрация</a></p>
        </div>
         </div>
         <p class='back_href'> <a class='a_style_dark' href='/'> На главную</a></p>
     
     </div>
    

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>