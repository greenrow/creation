    <?php 
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\CutAsset;

CutAsset::register($this);

?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
<head>
    <?php $this->head() ?>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <?= Html::csrfMetaTags() ?>
    <title>Регистрация</title>

    <script>
            
         function checklogin(el){
             var login=el.value.trim();
             $.post('/site/checkregister',{'check_login':login},function(data,el){
                 if(data){
                       var datajson=JSON.parse(data);
                    $('.auth_error').css('display','block');
                    $('.auth_error_text').text(datajson);
                    $('.reg_name').addClass('login_error_input');

                 }else{   $('.auth_error').css('display','none');$('.reg_name').removeClass('login_error_input');}

            })
      
         }
         
       function checknickname(el){
   
                
             var nickname=el.value.trim();
           
            
             $.post('/site/checkregister',{'check_nickname':nickname},function(data,el){
                 
                 if(data){
                       var datajson=JSON.parse(data);
                    $('.auth_error').css('display','block');
                    $('.auth_error_text').text(datajson);
                    $('.reg_nickname').addClass('login_error_input');
                 
                    
                 }else{   $('.auth_error').css('display','none');$('.reg_nickname').removeClass('login_error_input');}
                 
   
            })
        
         }
         
        function checkemail(el){
   
                
             var email=el.value.trim();
           
  
             $.post('/site/checkregister',{'check_email':email},function(data,el){
                 
                 if(data){
                       var datajson=JSON.parse(data);
                    $('.auth_error').css('display','block');
                    $('.auth_error_text').text(datajson);
                    $('.reg_email').addClass('login_error_input');
                 
                    
                 }else{   $('.auth_error').css('display','none');$('.reg_email').removeClass('login_error_input');}
                 
   
            })
        
         
     }
    function register(){
             var regdata=$('#regform')
             $.post('/site/checkregister',function(data){
                 
                 if(data){
                      var datajson=JSON.parse(data);
                     $('.auth_error').css('display','block')
                      $('.auth_error').text(datajson);
                   if(datajson['reg_ok']);
                    
                 
               

                }
      
        })
        
    }

    </script>

    <style>
        
                body{

            background-color:#fff;
        
        }

        #loginform{
            background-color:#f3f3f3;
            position:fixed;;padding:10px;;top:30%;
           border:1px solid #d4dde9;color:#000;
        }
        #regform{overflow:hidden;}
        .password_recovery{margin-top:20px;}
.login_table{color:#6E6770;;font-weight:700}
        .login_table tr  td{padding:10px;min-width:50px;}
        .login_table  input{width:95%;padding:10px;color:#6E6770;border:1px solid #b9b9b9}
 
              .label_td{width:20%;text-align:right;color:#6E6770;;font-weight:700}
              .label_val{width:80%}
        #getauth{cursor:pointer;text-align:center;background-color:#EA5656;color:#fff;position:relative;;border:none;border-radius:3px;;width:80px;height:50px;padding-top:15px;float:right}
#loginform .back_href{position:absolute;right:10px;bottom:-2px;}
    </style>

</head>
<body>

<div class='col-xs-6 col-xs-push-3 col-md-4 col-md-push-4 col-lg-3 col-lg-push-4' id="loginform">
               

        <form  id='regform' action="checkregister" method="POST" name="regform[]">
            <table class='col-xs-12 login_table'>
                <tr >
                    <td class='label_td'>Логин</td>
                      <td class='label_value'><input type="text" value='' name="regform[login]" class="long_field reg_name" required  onblur='checklogin(this)'></td>
                    
             <div class="auth_error">
                     <p class='parentclose_style'><span class="glyphicon glyphicon-remove"></span></p>
                <p class='auth_error_text'></p>
            
            </div>
                    
                </tr>
                <tr>
                     <td class='label_td'>Псевдоним</td>
                      <td class='label_val'><input type="text" value='' name="regform[nickname]"  onblur='checknickname(this)' class="long_field reg_nickname" ></td>
                 
                </tr>
                
                <tr>
                     <td class='label_td'>email</td>
                      <td class='label_val'><input type="email" name="regform[email]" value='' class="long_field reg_email" onblur='checkemail(this)' required ></td>
                  
               
                </tr>

                <tr>
                     <td class='label_td'>Пароль</td>
                    <td class='label_val'><input type="password" name="regform[passw]" value='' class="long_field reg_passw"  required ></td>
                    
                </tr>
           
                <tr>
                    <td colspan="2">
                        <input type="hidden" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    </td>
                </tr>
                
    
                   

                               
                           
                    
         

            </table>
                  <input id='getreg' class='mybtn col-xs-6 col-xs-push-3' type='submit' value='Зарегистрироваться'/>
        </form>

    <p class='back_href'> <a href='/'> На главную</a></p>
     </div>
    <?php $this->endBody() ?>
</body>
    </html>
<?php $this->endPage() ?>
    