        <?php 
    use yii\helpers\Url;
    $this->registerJsFile('https://www.google.com/jsapi',['position'=>Yii\web\View::POS_END,'depends' =>'yii\web\YiiAsset']);
        $this->title='Личный кабинет';
        
        $this->params['breadcrumbs'][] = $this->title;  ?>          
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
    
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
      var jsonData = $.getJSON('/personalpage/chartsdata',function(jsonData){

                    // Create our data table out of JSON data loaded from server.
       
        
           if(jsonData['pie']){
                   var data = new google.visualization.DataTable(jsonData['pie']);
                    var opt = {
                          width:400,
                          height:240,

                     pieSliceText: 'value' ,

                 };

                    // Instantiate and draw our chart, passing in some options.
                    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                    chart.draw(data, opt);
           }
           
            if(jsonData['colls']){
                    var data = new google.visualization.DataTable(jsonData['colls']);
                     var opt = {
                           width:400,
                           height:240,
                   
                      'title':'Категории',
                      pieSliceText: 'value' ,
                      label:'false'

                  };

                     // Instantiate and draw our chart, passing in some options.
                     var chart = new google.visualization.ColumnChart(document.getElementById('colls_div'));
                     chart.draw(data, opt);
           }
    
          
      })
 
    }
    </script>
<div style='margin-left:30%;' id="chart_div">
    
</div>
    
    <div style='margin-left:30%;' id="colls_div">
    
</div>




            
                       

      
                               
     
                 
               
                 
         


   
 

                  
        
 