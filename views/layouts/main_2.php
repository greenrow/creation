
<?php 

use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\SecAsset;

use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
SecAsset::register($this);
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1,maximum-scale =1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    

</head>
<body>


    



    <header class="main_header clear">
                <div class='logosvg vibrate'>
                    <object id='logo'  width="90%" data="/images/svg/logo.svg" type="image/svg+xml"></object>
                           <div class="hover-svg">
                                        <object class='hover_obj' id='svg_hover_collection'  width="100%" data="/images/svg/lighting.svg" type="image/svg+xml"></object>
                                    </div>  
                </div>  
                    <?php if(Yii::$app->user->isGuest){?>
                              
          
            
                 <div class="enter_exit_btn">
                     <a class=' a_menu login_in' href='<?php echo Url::toRoute(['/login']);?>'><span  style='margin-right:10px;font-size:1.1em' class='glyphicon glyphicon-log-in'></span>Войти</a>
                 </div>
         
          
             <!--if user is login--> 
             <?php }else{?> 

  
       
                    <div class="enter_exit_btn">
                        <a class='a_menu login_out' href='<?php echo Url::toRoute(['/logout']);?>'><span  style='margin-right:10px;font-size:1.1em' class='glyphicon glyphicon-log-out'></span>Выйти</a>
                 </div>
            
       
        <?php } ?>
            <div class="filter_main_menu">
    
            <div>

                 <div data-length="3" class="items_nav hide">
                      <span class="collection menu_item" data-index="0">Колелекции</span>
                      <span class='add_menudiv_item'>
                        <span class="sort menu_item " data-index="1">Сортировать</span>
                         <span class="date_div menu_item " data-index="2">Дата</span>
                         <span class="loc_div menu_item" data-index="3">Локация</span>
                      </span>
                      <span class=" hide arrow_dop_div glyphicon glyphicon-chevron-down"></span>
                 </div>
            </div>
</div>
        
<div class="header_dop_nav hide">

    <div class='header_dop_nav_filter_wrap'>
        
    </div>
    </div>

    </header>


     




     <!--filter_content-->
           <div class="col-xs-12 filter_content">
                 
                 <!--collection-->
            <div class='project_category_main'>
                 <h5 class="txt_cntr">Категории:</h5>
                <p  class="parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>

                <div class="categ_list_wrap">
                    
                    <div class='categ_list'>
                           <?php echo Yii::$app->runAction('site/showcateg');?>
                       </div>
                </div>  
            </div>
                 
                                             <!--карта-->
                   
                <div class='sort_map_wrap'>
                     <p    data-remove-class='{"alter_fon":"show"}' class="data_param parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>
                        <p class=" clearboth"><input class='autocomplete_input col-xs-12' type="text" value=''/></p>
                            <div class=" col-xs-12   sort_map">
                            </div>
                        <div class=" col-md-10 col-xs-12 error_div">

                        </div>
                        <p class=" clearboth sort_select_proj_loc mybtn col-xs-3 col-xs-push-9">
                           Выбрать
                        </p>
                </div>
                     
                     <!--calendar-->
                     
                        <div class='project_date_check'>
                                <p  class="parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>
                       
                            <h5 class="txt_cntr">Дата:</h5>
                                <ul class="proj_sort_menu col-xs-12">
                                    <li class="parent_up2_close col-xs-12 proj_date_sort" data-date="thisday">За текущий день</li>
                                    <li class="parent_up2_close col-xs-12 proj_date_sort" data-date="week">За неделю</li>
                                    <li class="parent_up2_close col-xs-12  proj_date_sort" data-date="month">За месяц</li>
                                    <li class="parent_up2_close col-xs-12 proj_date_calendar" data-date="calendar">Выбрать дату</li>
                                </ul>
               
                        </div>
                                      
                        <div class="calendar_data">
                                  <p  style='text-align:right' data-remove-class='{"alter_fon":"show"}' data-add-class='{"project_date_check":"show"}'  class="data_param parentclose"><span class='glyphicon glyphicon-remove'></span></p>
                                 
                        </div>
                     
                              <!--sort-->
                     
                        <div  class='project_sort_check '>
                             <p  data-remove-class='{"alter_fon":"show"}' class=" data_param parentclose_style"><span class='  glyphicon glyphicon-remove'></span></p>
                       
                                 <h5 class="txt_cntr">Сортировать:</h5>
                                <ul class="proj_sort_menu col-xs-12">
                                    <li class="parent_up2_close col-xs-12 proj_sort" data-sort-type="like">самые популярные</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort" data-sort-type="viewed">самые просматриваемые</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort hide" data-sort-type="active">активные</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort hide" data-sort-type="no_active">неактивные</li>
                              
                                </ul>
               
                        </div>
      
        </div>
    <div class="medium_main_area main_page">
           
        <div class="col-xs-12 filter_info_wrap">
            
       
            
            <!--filter_rez-->
                <div class='filter_rezult'>  
                  <div class='project_sort_info '>

<!--date-->
                    <div class="sort_project_date">

                         <div class="alter_filter_fon">

                          </div>
                        <div class="sort_wrap">
<!--                            <p class='close_date_sort ' ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                            <p class='txt_cntr'>Дата</p>
                            <div class="project_time selected_filter_text"></div>
                        </div>
                     </div>

<!--loc-->

                      <div class="sort_project_loc">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_loc_sort'  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Локация</p>
                            <div class="map_project_city selected_filter_text"></div>
                          </div>
                      </div>

<!--collection-->
                      
                       <div class="sort_project_collection">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_collection_sort'  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Коллекция</p>
                            <div class="selected_main_categ_list sort_text selected_filter_text"></div>
                          </div>
                      </div>
<!--sorttype-->

            <div class="sort_project_type">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_sort_type '  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Сортировка</p>
                            <div class="selected_main_sort_type selected_filter_text"></div>
                          </div>
                      </div>
                  </div>
             </div>
       
        </div>


    </div>





                 <!--event data-->
              <div class="event_data_wrap">
            
<!--              
                 <div class="col-xs-2  event_data_icon">
                     <span class="glyphicon glyphicon-info-sign"></span><span class='event_count'><?php echo Yii::$app->runAction('site/getevents');?></span>
                       
        

                </div>-->
                 
                             
                    <div class="event_data">
                   
                        <div class="show_event_data">
                           
                        </div>
                        
                        <div class="submit_addfriend">
                           <p><span class="submit_friend glyphicon glyphicon-ok"></span><span class="cancel_friend glyphicon glyphicon-remove"></span></p>
                        </div>
                    </div>
          
       
                        <div class="events">
                            <div style=''class='opacity_wrap'>
                                
                            </div>
                            
                        </div>

                
              </div>
                 
       
  
                    <div class='alter_fon hide'></div>    
                     <?php if(!Yii::$app->user->isGuest){?>

    <!--open_loadimage_area-->
           <div class='open_load_image_area'>  
<p class="parentclose_style">
<span class="glyphicon glyphicon-remove"></span>
</p>
                    
                <div class='col-xs-12' style='z-index:10000;' id="image-cropper">
                        <!-- The preview container is needed for background image to work -->
                              <div class=" cropit-image-preview-container">
                                <div class="cropit-image-preview"></div>
                              </div>

                              <input type="range" class="cropit-image-zoom-input" />

                              <input type="file" class="cropit-image-input" />
                              <div class="col-xs-6 col-xs-push-3 btn_load_wrap"> 
                                  <div  class="mybtn_load select-image-btn">Загрузить</div>
                              </div>
                              
                
                          
                       
                               

                </div>
     <div class="ava_save  mybtn parentclose">Сохранить</div>
                    <p style="display:none;" class='crop_src'></p>
           </div>
    

       <!--status_var-->
       <p id='chek_if_user_is_not_guest' login='login_true'></p>
        <!--aside-->
             <aside class='col-md-12  aside_user_page'>
   
                 <nav class='col-md-12 aside_user_page_nav'>

                        <div class=" col-md-12 header_user_menu" >
                
                    <div class='user_ava'>
                        <p class="user_nickname" data-user-id='<?php echo Yii::$app->user->identity->id?> '><a href="/users/<?php echo Yii::$app->user->identity->nickname?>"><?php echo Yii::$app->user->identity->nickname?></a></p>
                    
                    </div> 

               


                        <div class="user_menu ">
      
                           <div>
                                <div class="user_projects left_menu_div">   
                                    <object width='80%' data='/images/svg/project_user.svg'></object><br/>
                                </div>
                                    
                                <p> <a  href="<?php echo Url::to(['projects/index','nick'=>Yii::$app->user->identity->nickname])?>"></a></p>
                                           <p>Проекты</p>
                            </div>
                 


                            <div>
                                <div class="user_messages left_menu_div">   
                                    <object width='70%' data='/images/svg/message.svg'></object><br/>
                                </div>
                                    
                                <p> <a href="<?php echo Url::to(['perspage/messagelist','nick'=>Yii::$app->user->identity->nickname])?>"></a></p>
                                <p>Сообщения <span class="noviewed_event noviewed_mess_event"><?php echo Yii::$app->runAction('perspage/countmess');?> </span></p>
                            </div>
                            
                            <div>
                                <div class="user_friends left_menu_div">   
                                    <object type='image/svg+xml' width='100%' data='/images/svg/friends.svg'></object><br/>
                                </div>
                                <p><a id='friend_list' href="<?php echo Url::to(['perspage/friends','nick'=>Yii::$app->user->identity->nickname])?>"></a> </p>
                                <p>Друзья <span class="noviewed_event noviewed_friend_event "><?php echo Yii::$app->runAction('/perspage/countfriend');?> </span></p>
                            </div>   
                            
                            
                            <div>
                                <div class="user_news left_menu_div">   
                                   <object width='90%' data='/images/svg/settings.svg'></object><br/>
                                </div>
                                <p><a href="<?php echo Url::to(['/perspage/news','nick'=>Yii::$app->user->identity->nickname])?>"></a></p>
                                <p>Новости</p>
                             </div>
                            
                            <div>
                                <div class="user_settings left_menu_div">   
                                   <object width='90%' data='/images/svg/settings.svg'></object><br/>
                                </div>
                                <p><a href="<?php echo Url::to(['/settings/index','nick'=>Yii::$app->user->identity->nickname])?>"></a></p>
                                <p>Настройки</p>
                             </div>
                            
                    <div>
                          <div class="user_statistic left_menu_div">   
                                   <object width='90%' data='/images/svg/statistic.svg'></object><br/>
                         </div>
                                <p><a href="<?php echo Url::to(['/perspage/statistic','nick'=>Yii::$app->user->identity->nickname])?>"></a></p>
                                 <p>Статистика</p>
                     </div>
                    </div>
                         
                    <div class="additional_nav hide">
                         <p class=" absolute_block addit_event_wrap"></p>
                         <object width='30px' data='/images/svg/hidden_list.svg'></object><br/>
                         <div class="hidden_aside_divs  toogle">
                             <div class="aside_divs">
                                 
                             </div>
                             
                         </div>
 
                    </div>
       
 
      
            </div>

                       
                </nav>

            </aside>
    <?php }; ?>

        <div class="project_detail_wrap clear">
          <div class="project_detail clear">
                <div class="col-xs-12 project_header">
                    <div class="col-xs-10 project_name">

                    </div>

                    <div class="col-xs-2">
                        <p class='close_curent_project '><strong>Закрыть</strong></p>
                    </div>
                </div>
              <div class="col-xs-12 project_info">
          <div class="project_likes"></div>
                  <div class="project_views"></div>
                          <div class=" project_bookmark_wrap"></div>
                            

              </div>
              <div class="col-xs-12 project_description"></div>

                            
        <div class='project_detail_show clear col-xs-12  '>
            <div class="col-xs-12 project_files">

            </div>


        </div>
                   <div class="correct_submit_info col-xs-2">
     
                    <div class="correct_submit_info_text">
                    </div>
                    
                    
                </div>
              
                            <div class="col-xs-12 project_likeit">
                  <div class='project_likeit_wrap'></div>
                  
              </div>
              
              
              <div class="project_bottom_area col-xs-12">
                    <div class=" col-xs-8 col-xs-push-2 add_project_comment"></div> 
              <div class=" col-xs-8 col-xs-push-2 users_project_comments"></div>     
              </div>
        </div>
      
        
        
        <!--userinfo-->
              <div class='project_user_info col-xs-2 '>
 
            <div class="project_user_avatar ">

            </div>
                   
   

                 
            <div class='col-xs-12 project_category_info '>

                   </div>
            <div class='col-xs-12 project_date'>

             </div>  


        </div>
</div>
        
          <!--breadcrumbs-->
  <?php 
    
    if(isset($this->params['breadcrumb']) ){
        
  ?>  <div class="breadcrumb"><?php
   
echo Breadcrumbs::widget([
     'homeLink' => [ 
                      'label' => 'Главная',
                      'url' => Yii::$app->homeUrl,
                 ],
     'activeItemTemplate'=>'<li class="active_text">{link}</li>',
'links' => isset($this->params['breadcrumb']) ? $this->params['breadcrumb'] : [],

'itemTemplate' => "<li><i>{link}</i></li>\n"
]);

?>    </div> <?php
    }

    ?>
    


        
        <div class="container main_page_layer">
  
            
    
        
            
            <div class="message_box col-xs-4 col-xs-push-4">
    <p class="close_window close_message open_message_list">Закрыть</p>
    
    <div class="message_header">
    
    </div>
    <div class="message_body_wrap col-md-12">
        <div class="message_body refer_project "> </div>
    </div>
    

        <textarea class="message_input_area"></textarea>


         <p class="send_message">Отправить</p>

</div>
                <?= $content ?>
            
            
        </div>
        
          <div class='clear'></div>
<p class="project_sort_info_click"></p>

<div class="fileloader_info">
    
</div>
<div class="adaptive_filter_rezult_content">
    <div class="adaptive_filter_main_menu">
        
    </div>
    <div class="adaptive_filter_content">
    </div>
</div>
<footer class=" main_footer">
    fdfdf
</footer>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
