
<?php 

use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\SecAsset;

use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
SecAsset::register($this);
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1,maximum-scale =1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    

</head>
<body>


    



    <header class="main_header clear">
                <div class='logosvg vibrate'>
                    <object id='logo'  width="90%" data="/images/svg/logo.svg" type="image/svg+xml"></object>
                           <div class="hover-svg">
                                        <object class='hover_obj' id='svg_hover_collection'  width="100%" data="/images/svg/lighting.svg" type="image/svg+xml"></object>
                                    </div>  
                </div>  
                    <?php if(Yii::$app->user->isGuest){?>
                              
          
            
                 <div class="enter_exit_btn">
                     <a class=' a_menu login_in' href='<?php echo Url::toRoute(['/login']);?>'><span  style='margin-right:10px;font-size:1.1em' class='glyphicon glyphicon-log-in'></span>Войти</a>
                 </div>
         
          
             <!--if user is login--> 
             <?php }else{?> 

  
       
                    <div class="enter_exit_btn">
                        <a class='a_menu login_out' href='<?php echo Url::toRoute(['/logout']);?>'><span  style='margin-right:10px;font-size:1.1em' class='glyphicon glyphicon-log-out'></span>Выйти</a>
                 </div>
            
       
        <?php } ?>
            <div class="filter_main_menu">
    
            <div>

                 <div data-length="3" class="items_nav hide">
                      <span class="collection menu_item" data-index="0">Колелекции</span>
                      <span class='add_menudiv_item'>
                        <span class="sort menu_item " data-index="1">Сортировать</span>
                         <span class="date_div menu_item " data-index="2">Дата</span>
                         <span class="loc_div menu_item" data-index="3">Локация</span>
                      </span>
                      <span class=" hide arrow_dop_div glyphicon glyphicon-chevron-down"></span>
                 </div>
            </div>
</div>
        
<div class="header_dop_nav hide">

    <div class='header_dop_nav_filter_wrap'>
        
    </div>
    </div>

    </header>


     




     <!--filter_content-->
           <div class="col-xs-12 filter_content">
                 
                 <!--collection-->
            <div class='project_category_main'>
                 <h5 class="txt_cntr">Категории:</h5>
                <p  class="parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>

                <div class="categ_list_wrap">
                    
                    <div class='categ_list'>
                           <?php echo Yii::$app->runAction('site/showcateg');?>
                       </div>
                </div>  
            </div>
                 
                                             <!--карта-->
                   
                <div class='sort_map_wrap'>
                     <p    data-remove-class='{"alter_fon":"show"}' class="data_param parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>
                        <p class=" clearboth"><input class='autocomplete_input col-xs-12' type="text" value=''/></p>
                            <div class=" col-xs-12   sort_map">
                            </div>
                        <div class=" col-md-10 col-xs-12 error_div">

                        </div>
                        <p class=" clearboth sort_select_proj_loc mybtn col-xs-3 col-xs-push-9">
                           Выбрать
                        </p>
                </div>
                     
                     <!--calendar-->
                     
                        <div class='project_date_check'>
                                <p  class="parentclose_style"><span class='glyphicon glyphicon-remove'></span></p>
                       
                            <h5 class="txt_cntr">Дата:</h5>
                                <ul class="proj_sort_menu col-xs-12">
                                    <li class="parent_up2_close col-xs-12 proj_date_sort" data-date="thisday">За текущий день</li>
                                    <li class="parent_up2_close col-xs-12 proj_date_sort" data-date="week">За неделю</li>
                                    <li class="parent_up2_close col-xs-12  proj_date_sort" data-date="month">За месяц</li>
                                    <li class="parent_up2_close col-xs-12 proj_date_calendar" data-date="calendar">Выбрать дату</li>
                                </ul>
               
                        </div>
                                      
                        <div class="calendar_data">
                                  <p  style='text-align:right' data-remove-class='{"alter_fon":"show"}' data-add-class='{"project_date_check":"show"}'  class="data_param parentclose"><span class='glyphicon glyphicon-remove'></span></p>
                                 
                        </div>
                     
                              <!--sort-->
                     
                        <div  class='project_sort_check '>
                             <p  data-remove-class='{"alter_fon":"show"}' class=" data_param parentclose_style"><span class='  glyphicon glyphicon-remove'></span></p>
                       
                                 <h5 class="txt_cntr">Сортировать:</h5>
                                <ul class="proj_sort_menu col-xs-12">
                                    <li class="parent_up2_close col-xs-12 proj_sort" data-sort-type="like">самые популярные</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort" data-sort-type="viewed">самые просматриваемые</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort hide" data-sort-type="active">активные</li>
                                    <li class="parent_up2_close col-xs-12 proj_sort hide" data-sort-type="no_active">неактивные</li>
                              
                                </ul>
               
                        </div>
      
        </div>
    <div class="medium_main_area main_page">
           
        <div class="col-xs-12 filter_info_wrap">
            
       
            
            <!--filter_rez-->
                <div class='filter_rezult'>  
                  <div class='project_sort_info '>

<!--date-->
                    <div class="sort_project_date">

                         <div class="alter_filter_fon">

                          </div>
                        <div class="sort_wrap">
<!--                            <p class='close_date_sort ' ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                            <p class='txt_cntr'>Дата</p>
                            <div class="project_time selected_filter_text"></div>
                        </div>
                     </div>

<!--loc-->

                      <div class="sort_project_loc">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_loc_sort'  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Локация</p>
                            <div class="map_project_city selected_filter_text"></div>
                          </div>
                      </div>

<!--collection-->
                      
                       <div class="sort_project_collection">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_collection_sort'  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Коллекция</p>
                            <div class="selected_main_categ_list sort_text selected_filter_text"></div>
                          </div>
                      </div>
<!--sorttype-->

            <div class="sort_project_type">
                            <div class="alter_filter_fon">

                          </div>
                          <div class="sort_wrap col-xs-12">
<!--                                  <p class='close_sort_type '  ><span class='glyphicon glyphicon-remove'></span></span></p>-->
                                     <p class='txt_cntr'>Сортировка</p>
                            <div class="selected_main_sort_type selected_filter_text"></div>
                          </div>
                      </div>
                  </div>
             </div>
       
        </div>


    </div>


                 
    

          <!--breadcrumbs-->
  <?php 
    
    if(isset($this->params['breadcrumb']) ){
        
  ?>  <div class="breadcrumb"><?php
   
echo Breadcrumbs::widget([
     'homeLink' => [ 
                      'label' => 'Главная',
                      'url' => Yii::$app->homeUrl,
                 ],
     'activeItemTemplate'=>'<li class="active_text">{link}</li>',
'links' => isset($this->params['breadcrumb']) ? $this->params['breadcrumb'] : [],

'itemTemplate' => "<li><i>{link}</i></li>\n"
]);

?>    </div> <?php
    }

    ?>
    


        
        <div class="container main_page_layer">

                <?= $content ?>
            
            
        </div>
          <div data-load-status='0' data-position='0' class="project_endline"></div>
        
<!--include laouyt-->
<?php $this->beginContent('@app/views/layouts/include_layout.php'); ?>
<?php $this->endContent(); ?>
<!--end include layout-->
<footer class=" main_footer">
    fdfdf
</footer>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
