<?php 

use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\SecAsset;

use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
SecAsset::register($this);
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1,maximum-scale =1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    

</head>
<body>
     
       <div class="mainpage_back_glass">       </div>
    <div class='body_back'></div>
    
    
   
        
    <header class="col-xs-12 main_header ">

      
                <div class='logosvg vibrate'>
                 <object id='logo'  width="90%" data="/images/svg/logo.svg" type="image/svg+xml"></object>
                    
                </div>  
          
        
        
                    <?php if(Yii::$app->user->isGuest){?>
                              
          
            
                 <div class="enter_exit_btn">
                        <a class=' a_menu login_in' href='<?php echo Url::toRoute(['/login']);?>'><span  style='margin-right:10px;font-size:1.1em' class='glyphicon glyphicon-log-in'></span>Войти</a>
                 </div>
         
          
             <!--if user is login--> 
             <?php }else{?> 

       
                    <div class="enter_exit_btn">
                        <a class='a_menu login_out' href='<?php echo Url::toRoute(['/logout']);?>'><span  style='margin-right:10px;font-size:1.1em' class='glyphicon glyphicon-log-out'></span>Выйти</a>
                 </div>
            
       
        <?php } ?>
            
        


    </header>
        
       

    
    <?php if(isset($this->params['breadcrumb']) ){
        
  ?>  <div class="breadcrumb"><?php
   
echo Breadcrumbs::widget([
     'homeLink' => [ 
                      'label' => 'Главная',
                      'url' => Yii::$app->homeUrl,
                 ],
     'activeItemTemplate'=>'<li class="active_text">{link}</li>',
'links' => isset($this->params['breadcrumb']) ? $this->params['breadcrumb'] : [],

'itemTemplate' => "<li><i>{link}</i></li>\n"
]);

?>    </div> <?php
    }

    ?>
    


        
        <div class="container main_page_layer main_page_layer_user">

                <?= $content ?>
            
            
        </div>
                <div data-load-status='0' data-position='0' class="project_endline"></div>  
<!--include laouyt-->
<?php $this->beginContent('@app/views/layouts/include_layout.php'); ?>
<?php $this->endContent(); ?>
<!--end include laouyt-->
<footer class=" main_footer">
    fdfdf
</footer>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
