
        function iframe_create(){
               var content="<iframe  frameborder='no' class='iframe_texteditor' srcdoc='<!DOCTYPE html \n<html><head><style>"+iframe_text_editor_style()+"</style></head><body id=\"iframe_body\" contenteditable=\"true\" </body>'> \n\
                </iframe>"
        
               document.body.insertAdjacentHTML('afterBegin',content)
           }
           function iframe_text_editor_style(){
               
               var style=".iframe_project_text{width:100%;height:100%}\n\
                body{min-height:140px;overflow:auto;white-space:nowrap}.emptynode{width:auto;height:auto;display:inline;position:relative;z-index:10}\n\
.inputnode{position:absolute;border:none;bottom:0px;left:0px;z-index:1}"
               
              return style; 
           }
         function iframe_content(){
             
var content="<div  class=\"iframe_project_text\"></div></body>"
             return content;
         }
         
        function texteditor_content(){
            var texteditor_content="\
                <div class='text_editor'>\n\
                <div class='fontfamily_editor'></div><div class='fontsize_editor'><span class='glyphicon glyphicon-text-size'></span></div><div class='fontweight_editor'><span class='glyphicon glyphicon-bold'></span>\n\
</div><div class='fontstyle_editor'><span class='glyphicon glyphicon-italic'></span></div><div class='textleft_editor'><span class='glyphicon glyphicon-align-left'></span></div>\n\
<div class='textright_editor'><span class='glyphicon glyphicon-align-right'></span></div><div class='textcenter_editor'><span class=' glyphicon glyphicon-align-center'><span></div>\n\
<div class='fontcolor_edior'><span class='glyphicon glyphicon-text-color'></span></div><div class='textmargin_edior'></div>\n\
                </div>\n\
            "
               return texteditor_content;
        }
              
      //global functions//
    function hasClass( elem, className ) {
            return elem.className.split( ' ' ).indexOf( className ) > -1;
}
      //insertAfter //
function insertAfter(elem, refElem) {
  var parent = refElem.parentNode;
  var next = refElem.nextSibling;
  if (next) {
    return parent.insertBefore(elem, next);
  } else {
    return parent.appendChild(elem);
  }
}
    var text_editor_obj={
         
            styles:{

                 bold:"strong",
                cursiv:"em",
                size:{
                    normal:function(text){
                        text.css('font-size','')
                    },
                       big:function(text){
                        text.css('font-size','1.1em')
                    },   
                    small:function(text){
                        text.css('font-size','0.9em')
                    },
                },
                color:function(text,color){
                    text.css('color',color)
                },
                align:{
                    left:function(text){
                        text.css('text-align','left')
                    },
                    right:function(text){
                                text.css('text-align','right')
                    },
                    center:function(text){
                           text.css('text-align','center')
                    }
                }

         
            },
            del_tag:function(text){
                var regex = /(<([^>]+)>)/ig;
	        return text.replace(regex, "")
            },
            move_carret_to_end:function (el,root) {
         el.focus();
                if (typeof window.getSelection != "undefined"
                        && typeof document.createRange != "undefined") {
                    var range = document.createRange();
//             
//                    range.selectNodeContents(el);
                    range.setStart(el.lastChild,0)
                     range.collapse(true);
                    var sel = this.find_selection_text(root);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
//                } else if (typeof document.body.createTextRange != "undefined") {
//                    var textRange = document.body.createTextRange();
//                    textRange.moveToElementText(el);
//                    textRange.collapse(false);
//                    textRange.select();
//                }
            },
  
            find_selection_text:function(iframe_root){return findselectiontext(iframe_root)},
            
            make_fragment:function(range,root,recursiv){
                            var document_fragment=new DocumentFragment();                
                            var fragment=range.extractContents();//replace rage into documentfragment object --has no parent!! //
                            var fragment_child=fragment.childNodes;
                            var fragment_firstchild=fragment.firstElementChild;
                            var fragment_lastchild=fragment.lastElementChild;
                            var fragment_text=fragment.textContent;
                            var emptynode1=root.createTextNode('');
                            //if right node NOT equal to leftnode
                          
                            var leftnode_html=fragment_firstchild.innerHTML;
                            var rightnode_html=fragment_lastchild.innerHTML;
//                            
//                            fragment.replaceChild(emptynode1,fragment.children[0]);
//                            fragment.replaceChild(emptynode1,fragment.childNodes[fragment_child.length -1]);
                            //reset 1 amd last node
       
                            var leftnode,rightnode;
                            if(recursiv =='ok'){
                                console.info('fragment recursiv-->add elem');
                                var emptynode=root.createElement('<span>')
                                document_fragment.appendChild(emptynode)
                                emptynode.insertAdjacentHTML("afterend",leftnode_html)
                            }else{  leftnode=root.createTextNode(leftnode_html);
                            document_fragment.appendChild(leftnode);
                             }

                            rightnode=root.createTextNode(rightnode_html);

                            //added nodes to ragments
                            
                            document_fragment.appendChild(fragment);
                            document_fragment.appendChild(rightnode);
                           
                            //delete empty style nodes

                            console.info(' left node NOT equal right node  1 elem of fragment '+fragment_firstchild.nodeName + " last elem of fragment is "+fragment_lastchild.nodeName)
                        return document_fragment;
                         
            },
            
            get_parent: function (obj, parentTagName,styleNode,range,root,recursiv,recurse_startnode,recurse_endnode) {
               if(recurse_startnode != undefined) alert('recuse nide'+recurse_startnode.nodeName)
      
                var item_node_text=obj.textContent;//sected text
   
                var _this=this;
                this.obj=obj;
//                var item_length=this.obj.textContent.length //length of selected text
//                alert (item_length);
                this.recursiv=recursiv;
                var range_length=range.toString().length;//selected length
           
                var rangetext=range.toString();//selected text
                var rangetext_code_of_first=rangetext.charCodeAt(0);//get charcode of first elem
                var rangetext_code_of_last=rangetext.charCodeAt(rangetext.length-1);//get charcode of last elem
                
                var rangeStart=range.startContainer; //return textnode were range start
                var rangeEnd=range.endContainer; alert('start container '+rangeStart.parentNode.nodeName+' endContainer'+rangeEnd.parentNode.nodeName)
                
                var rangenode_start=recurse_startnode == undefined ? range.startContainer.parentNode : recurse_startnode;//return Start parentnode of textnode//
                var rangenode_end=recurse_endnode==undefined ? range.endContainer.parentNode :recurse_endnode;//return End parentnode of textnode//
           
                
                var start_range_text= rangenode_start.textContent;
                var start_range_length=start_range_text.length;
                
                var end_range_text= rangenode_end.textContent;
                var end_range_text_length= rangenode_end.textContent.length;
                
                   /*find text position and offset if have only ony range node selected*/
                var range_left_offset=range.startOffset;
                var range_right_count_from_left=range.endOffset;
//                var range_right_offset =rangeEnd.length-range_right_count_from_left;

                var status,regexp,textnode,innerdata,itemnode;
                var child_elem_val;
                var stylenode_text=styleNode.nodeName.toUpperCase();    
                                  /*remove left white space*/
                if(rangetext_code_of_first == "160"){//if whitespaces
                        console.info('has left whitespaces, range length  '+rangetext.length+" range startnode length "+rangeStart.length+"range nodename "+rangeStart.nodeName+" range left offset "+range_left_offset)
                        console.info('startOffset '+range.startOffset)
                        var i=0;
                        var cur_range=range.toString().charCodeAt(0);
                        var whitespase_fragment=new DocumentFragment();
                
                //find whitespces and delete it
                        while(cur_range=='160'){
                            range.setStart(rangeStart,i+range.startOffset);
                            cur_range=range.toString().charCodeAt(0);
                            console.info('replace left whitespace into fragment '+i);
                            alert('del left whiespace ',i)
                            i++;
                        }
                                                //rewrite var
//                         rangenode_start=rangenode_end
                }

                            console.info('<BEGIN-------------------->');
                            console.info('STYLE IS '+stylenode_text);


//                    if(rangetext_code_of_last =="32"){
//                        var endrange_length=rangeEnd.length;
//                        var range_length=range.toString().length;
//                        var endindex_of_range=range_left_index+range_length-1;
//                   
//                        
//                        console.info('has right-whitespaces, right offset is '+text_right_offset+ " left _offset is "+range_left_index+' end -index of range is '+endindex_of_range);
//                        console.info('range length '+range_length+" parent node length is "+endrange_length)
//                      
//                        var cur_range=range.toString().charCodeAt(endindex_of_range);
//                        console.info('end range charcode is '+cur_range);
//                        
//                          var i=0;
//                        //chkec if range >parent node text length
//                        var range_index=endrange_length > range_length ? endindex_of_range : endrange_length-1;
//                        while(cur_range=='32'){
//                            
//                            range.setEnd(rangeEnd,range_index-i);
//                 
//               
//                            i++;
//                            cur_range=range.toString().charCodeAt(endindex_of_range-i);
//                            console.info('remove right  whitespace '+i+" har code is "+cur_range);
//                        }
//                    }
         

      
                    if(this.obj.tagName=='BODY'){      
                        alert('body')
                        var fragment=range.extractContents();//replace rage into documentfragment object --has no parent!! //
                        var fragment_text=fragment.textContent;
                     //***********move fragment into style node***************//   
                        styleNode.appendChild(fragment);range.insertNode(styleNode);
                        console.info('STYLE ADD')
                        console.info('FIND BODY break;');return false;
                    }
                   
//                    if( this.recursiv === 'true' && this.obj.tagName == parentTagName && rangenode_end.nodeName == parentTagName ){
//                        status ='replaced';/*important ststus var*/
//                        console.info('recursiv - TRUE');
//                        console.info('parser find '+this.obj.tagName);
//                        
//                        if(range_right_count_from_left<range_length){
//                            console.info(' left node  NOT equal right node');
//                        
//                        }else{
//                            console.info(' left node  equal right node');
//                        }
//    
//                        if(range_right_offset == 0 && range_left_offset ==0){ //if no text left and right
//                            console.info('has no text above range')
//                            var doc_fragment=this.make_fragment(range,root,'ok');
//                            this.obj.parentNode.replaceChild(doc_fragment,this.obj)
//                           console.info('content replaced')
//                        }
//                    }
      
                    else if(rangenode_start.tagName  == parentTagName && rangenode_end.tagName == parentTagName )//must remove nodes
                    
                    {
                              
                        status ='replaced';/*important ststus var*/
                        console.info('recursiv - FALSE')
                            alert('no recurs')
                        console.info('left and right node have same style') ;

                        if(range_right_count_from_left<range_length){
                            console.info(' left node  NOT equal right node');
                        
                        }else{
                            console.info(' left node  equal right node');
                        }

                              /*****find parent nodes as style node****/
                              
   
                         
                         
                        if(range.startContainer.textContent.length>0){
                            alert('select full ')
                            var doc_fragment=range.extractContents()
                            alert('inner first elem'+doc_fragment.firstElementChild);

                               this.obj.parentNode.replaceChild(doc_fragment,this.obj)
                                               alert('must changes')
                           console.info('content replaced')
                       
                           
                        }
//                        else if(range_right_offset !=0 && range_left_offset ==0){
//
//                            var tmpfragment=new DocumentFragment();
//                            var text_plus_left_offset=rangetext.length+range_left_offset;
//                            var text_left=start_range_text.substring(0,range_left_offset);
//                            var leftnode=root.createTextNode(text_left);
//                            var rightnode=root.createTextNode(text_right);
//                            var leftstrong=root.createElement(this.obj.nodeName);
//                            var rightstrong=root.createElement(this.obj.nodeName);
//                            
//                            leftstrong.appendChild(leftnode);
//                            rightstrong.appendChild(rightnode);
//                            tmpfragment.appendChild(leftstrong);
//                            tmpfragment.appendChild(fragment);
//                            tmpfragment.appendChild(rightstrong);
//                            
//                            /*replace parent node*/
//                            this.obj.parentNode.replaceChild(tmpfragment,this.obj)
//                            console.info('content replaced')
//
//                        }
//                        else if(range_right_offset == 0 && range_left_offset !=0){   
//                            console.info(' has left offset '+range_left_offset)
//                            insertAfter(fragment,this.obj);
//                      console.info('content replaced')
//                            
//                        }
//                        else if(range_right_offset !=0 && range_left_offset !=0){
//                            
//                               var tmpfragment=new DocumentFragment();
//                            var text_plus_left_offset=rangetext.length+range_left_offset;
//                            var text_left=start_range_text.substring(0,range_left_offset);
//                            var text_right=start_range_text.substring(text_plus_left_offset);
//                             console.info('range has left and right siblings '+' left is '+text_left+' right is '+text_right)
//                            var leftnode=root.createTextNode(text_left);
//                            var rightnode=root.createTextNode(text_right);
//                            var leftstrong=root.createElement(this.obj.nodeName);
//                            var rightstrong=root.createElement(this.obj.nodeName);
//                            leftstrong.appendChild(leftnode);
//                            rightstrong.appendChild(rightnode);
//                            tmpfragment.appendChild(leftstrong);
//                            tmpfragment.appendChild(fragment);
//                            tmpfragment.appendChild(rightstrong);
//                            
//                            /*replace parent node*/
//                            this.obj.parentNode.replaceChild(tmpfragment,this.obj)
//                            console.info('content replaced')
//
//                        }
//                        

                    }
                    
                else{
                         alert('recurs');this.get_parent(obj.parentNode, parentTagName,styleNode,range,root,'true',rangenode_start.parentNode,rangenode_end.parentNode)}
                },

            edit_styles:function(parent,styleNode,frame_root,event_classname){
                var _this=this;
                var root=frame_root.contentDocument;//document root//
                var elem_parent= root.getElementById('iframe_body')//body elem//
                var seltext=this.find_selection_text(frame_root);//return selection text with nodes
               
                //*************range**************//
                var range=seltext.getRangeAt(0);//return rage object for mainpulation with selection text//

                var rangenode_start=range.startContainer.parentNode;
                var stylenode_text=styleNode.nodeName.toUpperCase();    

                  this.get_parent(rangenode_start,stylenode_text,styleNode,range,root);
      


                    this.move_carret_to_end(rangenode_start,frame_root);
                       //style elem length//       
       
                    
                    console.info('<---------END------------------->')
                    
            },
            
    change_selection_text:function(parent,root,event_classname){
     
                switch (event_classname){
                    case "fontweight_editor":

                    var _node = root.contentDocument.createElement(this.styles.bold);
                    this.edit_styles(parent,_node,root,event_classname);
                    break;
                
                    case "fontstyle_editor":
                          
                    var _node = root.contentDocument.createElement(this.styles.cursiv);
                    this.edit_styles(parent,_node,root,event_classname);
                    break;
                } 
            },
                 find_parent_on_click:function(root){
                     root.contentDocument.body.addEventListener('click',function(e){
                         
                          alert(e.target.parentNode)
                     })
                     

            }
            
    }
     
     

 /*global var*/
 var selection_text;
 
 function findselectiontext(iframe){
       if (iframe.contentWindow.getSelection) {
    t = iframe.contentWindow.getSelection();
  } else if (iframe.contentDocument.getSelection) {
    t = iframe.contentDocument.getSelection();
  } else if (iframe.contentDocument.selection) {
    t = iframe.contentDocument.selection.createRange().text;
  } else{
      t='select error'
  }
   return t;
 }
      var global_scope_i=static_i();
      
      function static_i(){
          
           var i=0;
           
            return function(){    
                  
                   return i++;

             }
      }
              
          
 function findChildElem(parent,elem){
                var i=0;
  var counter=global_scope_i();
  var child_length=(parent.childNodes.length)-1
    
        
          
                    if(parent.childNodes[counter].nodeName==elem){
                        parent.childNodes[counter].remove()
                    }
                  
if(counter<child_length){
    
    findChildElem(arguments)
}
                
          
      }

//function getSelectionText(event_classname,node) {
//    var seltext=findselectiontext();
//    var range=seltext.getRangeAt(0);
//   switch (event_classname){
//       
//        case "fontweight_editor":
//   }
//    var _node = document.createElement(node);
//
//    range.surroundContents(_node);
//}

/*parent widow*/




    

//          
//         var current_class=$(this).attr('class');
//         switch(current_class){
//             
//             case 'textright_editor':
//               text_editor_events.align.right(selection_text);break;
//         
//                      
//             case 'textleft_editor':
//               text_editor_events.align.left(selection_text);break;
//                         
//             case 'textcenter_editor':
//               text_editor_events.align.center(selection_text);break;
//         }
//         
//         
