 var FileLoader=function (){
    console.info( 'создано обьектов ***', FileLoader.stat);
    var success_arr=[],
    not_upload_img_arr=[];
    var thisarg=arguments;
    //check args
    if(thisarg[0] != undefined){
        console.info('type of 1 arg ',typeof(thisarg[0]));
        var root=thisarg[0].trim(),rootitem=document.querySelector(root),
        img_length= document.querySelectorAll(root +' img').length,
        files=document.querySelectorAll(root+' img');
        for(var i=img_length,j=i-1; i>0;i--,j--){//send dinamic prop 
                console.log(' load img is ',files[j].complete== true ,' src ',files[j])
                files[j].setAttribute('fljs-img-index',j);
                if(files[j].complete== false){
                    not_upload_img_arr.push({});
                    var l=not_upload_img_arr.length;
                    files[j].setAttribute('arrayindex',l-1);
                    not_upload_img_arr[l-1].foto=files[j].getAttribute('src');
                    not_upload_img_arr[l-1].delindex=files[j].getAttribute('fljs-img-index')
                    files[j].addEventListener('load',function(e){
                        success_arr.push('1')
                        not_upload_img_arr[e.target.getAttribute('arrayindex')].status=1;
                    })
                     files[j].addEventListener('error',function(e){
                       not_upload_img_arr[e.target.getAttribute('arrayindex')].status=0;;
                    })
                }
            }
    }else{console.error('FileLoader error : не найден родительский контейнер');return false;}

    if(FileLoader.objarr.length >0){
       var status=1;
       for(var i=0;i<FileLoader.objarr.length;i++){
                if( FileLoader.objarr[i].root == root){//if object exists
                   status=0;
                   FileLoader.objarr[i].this.not_upload_img=not_upload_img_arr;//add dinamic prop
                   FileLoader.objarr[i].this.success_arr=success_arr;//add dinamic prop
                       console.log(' OBJECT ',FileLoader.objarr[i].root , 'exisis return -',  FileLoader.objarr[i].this, ' current object index is ',FileLoader.currentobject);
                   return FileLoader.objarr[i].this //return old object
                }
        }
        //if new obj//
        if(status){
                FileLoader.objarr.push({root:root,this:new FileLoader.Root(root)});
                FileLoader.objarr[FileLoader.objarr.length-1].this.success_arr=success_arr;
                FileLoader.objarr[FileLoader.objarr.length-1].this.not_upload_img=not_upload_img_arr;
                FileLoader.currentobject=FileLoader.objarr.length-1;
                 console.log(' OBJECT in NEW  ','exisis return -',   FileLoader.objarr[FileLoader.objarr.length-1].this);
                return FileLoader.objarr[FileLoader.objarr.length-1].this ; 
        }

    }else{//first call
        FileLoader.objarr.push({root:root,this:new FileLoader.Root(root)});FileLoader.currentobject=FileLoader.objarr.length-1;
        FileLoader.objarr[FileLoader.objarr.length-1].this.success_arr=success_arr;
        FileLoader.objarr[FileLoader.objarr.length-1].this.not_upload_img=not_upload_img_arr;
                console.log(' OBJECT in FIRST CALL  ',FileLoader.objarr[i].root , 'exisis return -',   FileLoader.objarr[FileLoader.objarr.length-1].this, ' current object index is ',FileLoader.currentobject);
        return FileLoader.objarr[FileLoader.objarr.length-1].this;
    }
}

 // namespace FileLoader
FileLoader.stat=0; 
FileLoader.objarr=[];

/***************Root obj************/
FileLoader.Root=function(root){
    console.log('Objectarr',FileLoader.objarr)
    this.root=root;
    FileLoader.stat++;
}
  /**********call area obj***********/  
FileLoader.Root.prototype.area=function(root){
    if(root == undefined){
           console.error('аргумент root конструктора обьекта Area не определен');
           return false;
    }
    if(!this._areaobj){
        var mainroot=this;
        this._areaobj=new FileLoader.Area(mainroot,root);
        console.log('AREA Object creating')
    }
    return  this._areaobj;//return singelton area object
}
FileLoader.Root.prototype.getroot=function(){
  return this.root;
}
FileLoader.Root.prototype.getareaobj=function(){
  return this._areaobj;
}
         /**********success***********/  
FileLoader.Root.prototype.success=function(){
    console.log('sucess arr',this.success_arr)
    var _this=this,
    root=this.root,
    args=arguments,
    i=0,
    timeout,
    files=document.querySelectorAll(root+' img'),
    img_length= files.length;

    timeout=setTimeout(function successfunc(){
        i++;
        if(i >600){clearTimeout(timeout);console.error('Превышено время загрузки изображений');return false;}
        console.info(i)
        console.info('size of load_arr',_this.success_arr.length)
        console.info('size of files length',_this.not_upload_img.length,'not upload image arr',_this.not_upload_img );
        if(_this.success_arr.length == _this.not_upload_img.length ){//if all images loaded
            setTimeout(function(){
                if(args[0] !=undefined){
                    console.info('ALL IMAGES UPLOADED')
                    args[0] ();//call param func
                }
            },400);
        }else{
            setTimeout(successfunc,10)
        }
    },10)
}
       
 /**********area obj***********/  

FileLoader.Area=function(mainroot,root){
   var el=document.querySelector(root);
   if(el.style.display=='none'){el.removeAttribute('style')}
   var buttons=document.createElement('div');
   buttons.classList.add('jsfl-buttons-wrap');
   buttons.setAttribute('style','position:absolute;bottom:-45px;height:45px;width:100%;padding:0px;background-color:#10121e;z-index:100')
   var files_wrap=document.createElement('div');
   files_wrap.setAttribute('style','width:100%;overflow:hidden;z-index:-2;position:relative;margin-bottom:50px;height:inherit;background-color:#5b5960;background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAkCAQAAACtIJtXAAABc0lEQVRIx41VIZbFIBCL4z0Eaup21Ao0unf4YlUPwGE49or2FxgCLbjpPEgyaQAAfH7tBpDQL4Ecm+0rDsCxAYimHTlAoF1pB1upuLM9jt+K63AoJIehKQLFTe8orkOXauu9POJ9bA5QCCET2W312MfCja69rZVXu2oOECqqQiaHJyZfnOBQKqQfqF3y2a3wtE6RCXwOx9bvLyFS1zfcAECKY+05wFvijNs1zOIQx5GP2kVygDTDTNR6aWLUkVoO0JFMi06oddQMc+mOF9Zh8lV0U+v01an10jkwHa1jtSkOwD704VSDWMqkxZUYtuuy3rFNRxZJPk1TS6l1El6MvVafrJOo9bRNLaH/wRfdTjHsK9h9m9Cx20Am8lV0q/ifhK2VTLih6IRoavE6vU6Bvx/64PG6vuFW4//poVpwSw8PledPG7fOYyA8NCzS4k4tpdHbBPIqLeYYukC2YXtXPSLwofE/yFtcL1+LTrh1qPeYxeI/Pt5vuBkXfBQAAAAASUVORK5CYII=")');
   files_wrap.classList.add('jsfl-files-wrap');
   var file=document.createElement('div');
   file.setAttribute('style','overflow:auto;width:calc(100% + 20px);height:95%;padding-right:15px;');
   files_wrap.appendChild(file)
   el.appendChild(buttons);
   el.appendChild(files_wrap);
   el.style.display='block';
   this._privatesettings={
        arearoot:root,
        btnroot:buttons,
        filesroot:file,
        mainroot:mainroot.root,
   }
   
       //add one ev.lst on del btn
    file.addEventListener('click',function(e){
        var elem=e.target;
        var main_root_= document.querySelector(mainroot.root);               //event listener
        var area=document.querySelector(root);
        if(e.target.classList.contains('jsfl-del-item')){
            if(main_root_.children.length>0){
                var elemindex_to_del=elem.getAttribute('jsfl-pos');
                var item_to_remove=main_root_.querySelector("img[fljs-img-index='"+elemindex_to_del+"']");
                if(item_to_remove){      
                    var parent=item_to_remove.parentNode.parentNode;
                    parent.removeChild(item_to_remove.parentNode);
                }
            }
           file.removeChild(e.target.parentNode.parentNode.parentNode);
        }
    })
  
 
    if(document.querySelector(root).style.display=='none'){document.querySelector(root).style.display='block'}
}

            /**********call btn obj***********/  
FileLoader.Area.prototype.buttons=function(arg){
    if(!this._btnobj){
        this._btnobj=new FileLoader.Buttons(arg);
   }
    return this._btnobj; //return Button object via singelton
}
           
/**********call files obj**********/
FileLoader.Area.prototype.files=function(arg){
    if(typeof(arg) !='object'){console.error('files должно быть обьектом');return false;}
    return  new FileLoader.Files(arg);//return Files object
 }
FileLoader.Area.prototype.getobj=function(objname){
      return this[objname]
}

  /**********buton obj***********/  
FileLoader.Buttons=function(arg){
    console.log('BTN OBJ PROTO',FileLoader.Buttons.prototype)
    var areaobj_call=this.getareaobj.call(FileLoader.objarr[FileLoader.currentobject].this);
    var areaobj_settings=areaobj_call._privatesettings;
    var area=areaobj_settings.arearoot,
    btnroot=areaobj_settings.btnroot,
    mainroot=areaobj_settings.mainroot,
    btn_arr=arg.split(','),
    change_recurse=0;
    for(var z=0;z<btn_arr.length;z++){
        console.info('btn val',btn_arr[z]);
        if(btn_arr[z] == 'close') {
            var text=document.createTextNode('Закрыть');
            var close_div=document.createElement('div');
            close_div.classList.add('jsfl-btn','jsfl-close-file')
            close_div.appendChild(text);
            close_div.addEventListener('click',function(){
                var main_root=document.querySelector(mainroot),
                main_root_l=main_root.children.length,
                f_length= document.querySelector(area).querySelector('.jsfl-files-wrap>div').children.length;
                if(main_root_l==f_length ){
                    main_root.innerHTML='';
                }else if(main_root_l <1) {  
                    document.querySelector(area).querySelector('.jsfl-files-wrap>div').innerHTML='';document.querySelector(area).style.display='none';
                }else{
                    for(var g=main_root_l; g > main_root_l-f_length  ; g--){
                        main_root.removeChild(main_root.children[g-1])
                    }
                }
                document.querySelector(area).querySelector('.jsfl-files-wrap>div').innerHTML='';document.querySelector(area).style.display='none'

            })
            btnroot.appendChild(close_div);   
        }
        if(btn_arr[z] == 'load'){
            var load_text=document.createTextNode('Загрузить');
            var load_div=document.createElement('div');
            load_div.appendChild(load_text);
            load_div.classList.add('jsfl-btn','fileloader-load-btn')
            btnroot.appendChild(load_div);
          }
        if(btn_arr[z] == 'save'){
            var save_text=document.createTextNode('Сохранить')
            var save_div=document.createElement('div');
            save_div.classList.add('jsfl-btn','jsfl-save-file');
            save_div.addEventListener('click',function(){
                    //add textarea
                var files_root=document.querySelector(area).querySelector('.jsfl-files-wrap>div'),
                main_root=document.querySelector(mainroot),
                main_root_img=main_root.getElementsByTagName('img'),
                main_root_l=main_root_img.length,
                f_length=files_root.children.length,
                textarea_all=files_root.querySelectorAll('textarea');
                if(textarea_all !=null){
                    var dinamic_elems_create=function(i,j){
                        if(textarea_all[j-1].value !=''){
                            var textarea_val=textarea_all[j-1].value,
                            textarea;
                            textarea=textarea_all[j-1].getAttribute('rewrite') ? document.createElement('textarea') : document.createElement('p');
                            textarea.setAttribute('style','width:100%');
                            textarea.classList.add('jsfl-desc')
                            var txt=document.createTextNode(textarea_val);
                            textarea.appendChild(txt);
                            main_root_img[i-1].parentNode.appendChild(textarea);
//                                    insertBefore(textarea,main_root_img[i].nextSibling);
                        }
                    }
                    
                   if(main_root_l==0){ 
                       document.querySelector(area).querySelector('.jsfl-files-wrap>div').innerHTML='';document.querySelector(area).style.display='none';
                    }
                    
                     var diff=main_root_l-f_length,increment;
                        for(var i=main_root_l,j=f_length;diff==0 ? i>0: i>diff; i--,j--){
                                 dinamic_elems_create(i,j)
                        }
                             console.info('root h '+main_root.offsetHeight+ ' wind h '+window.innerHeight)
                    
                 }
               document.querySelector(area).querySelector('.jsfl-files-wrap>div').innerHTML='';document.querySelector(area).style.display='none'
            })
            save_div.appendChild(save_text);
            btnroot.appendChild(save_div);
        }
    }

    if(btn_arr.length>0){
        //all btn style in one
        var btncss=document.createElement('style');
        var btnclass=document.createTextNode(".jsfl-btn:hover{background-color:#33464d !important;}.jsfl-btn{transition: all  0.4s ease-in;}");
        btncss.appendChild(btnclass)
        var btnarr=document.querySelectorAll('.jsfl-btn');
        document.querySelector(area).appendChild( btncss);
        Array.prototype.forEach.call(btnarr,function(element){
        var btnwrap_h=document.querySelector('.jsfl-buttons-wrap').style.height;

            element.setAttribute('style','line-height:'+btnwrap_h+';cursor:pointer;background-color:#141a23;color:#c0c0c0;margin:0px;width:33.3%;height:100%;text-align:center;border-right:1px solid #49818a;border-radus:3px;bottom:0px;position:relative;float:right');

        })
    }


  }
FileLoader.Buttons.prototype=Object.create(FileLoader.Root.prototype);
FileLoader.Buttons.prototype.constructor=FileLoader.Buttons;
//actions//
FileLoader.Buttons.prototype.actions=function(arg){
    var areaobj=this.getareaobj.call(FileLoader.objarr[FileLoader.currentobject].this);
    var area=areaobj._privatesettings.arearoot
    console.log(' AREA obj settings is',areaobj);
    var change_fnc=function(e){  
        if(e.target.classList.contains('fileloader-load-btn')){
           var event = document.createEvent('MouseEvents');
           // Define that the event name is 'click'.
            event.initEvent('click', true, true);
            load1.dispatchEvent(event)
        }
                                 
//        setTimeout(function(){    
//            var area=document.querySelector(area).querySelector('.jsfl-files-wrap');
//            var h=area.scrollHeight;
//            area.scrollTop=h
//        },600)
               
    }
    

    for(var i in arg){
        if(i == 'load'){
            var load1=document.querySelector(arg[i])
            if(load1 ==undefined){console.error('неверно укзаны селеткор для действия загрузки');return false;}
            var load=document.querySelector(area).querySelector('.fileloader-load-btn'),
            files_arr=[];
        }

    }
    if(load != undefined){
         load.addEventListener('click',function(e){
             change_fnc(e);
         });
     }

}
      
 /****Files obj*****/
FileLoader.Files=function(arg){
    console.info('*******File object running********')
    if(arg==undefined || arg.length ==0 || typeof(arg) !='object'){console.error('jsfl error неверно указан параметр обькта File');return false;}
    var root=FileLoader.objarr[FileLoader.currentobject].this,
    areaobj_call=this.getareaobj.call(root),
    areaobj_settings=areaobj_call._privatesettings,
    area=areaobj_settings.arearoot,
    rightdiv,
    leftdiv,
    btnroot=areaobj_settings.btnroot,
    mainroot=areaobj_settings.mainroot,
    files_root=areaobj_settings.filesroot,
    file_upload_arr=root.upload_img,
    files_length=root.not_upload_img.length, 
    desc_rule;
    if(document.querySelector(area).style.display='none'){document.querySelector(area).style.display='block'}
    var progress=function(leftdiv,i){
        console.info('start progress func')
           //progress
        var progress=document.createElement('div'),files_not_upload=root.not_upload_img;
        var img_index=files_not_upload[i]
        progress.classList.add('file_progress');
        progress.setAttribute('style',"position:relative;left:0px;top:0px;height:inherit;width:0;background-color:#55B253");
        var progress_wrap=document.createElement('div');
        progress_wrap.classList.add('progress_wrap');
        progress_wrap.appendChild(progress);
        progress_wrap.setAttribute('style','border:1px solid grey;width:100%;position:relative;height:20px;margin:0 0 10px 0;top:0px;')
        leftdiv.appendChild(progress_wrap)
    //animation
        var _this=this;
        var anim=function (i){
            console.info('i',i)
            var start=new Date().getTime(),
            delay=500,
            result=0,
            progress_el_list=document.querySelector(area).querySelector('.jsfl-files-wrap>div').lastChild,
            item=progress_el_list.getElementsByClassName('progress_wrap')[0].getElementsByClassName('file_progress')[0],
            item_width=item.parentElement.offsetWidth,
            duration =item_width,
            progress_limit=duration-duration/2;
            console.info('duration',duration)
            console.info('progress item',item)
            setTimeout(function progressfunc(){
                if(result >=duration-10){delay=0;}
                else if(result >  progress_limit){ delay=2000;}
                var now = new Date().getTime()-start;
                var progress=now/delay;   
                result+=progress*duration;
                 console.info('progress ',i ,' ',progress)
                 console.info('result ',i,' ',result)
                item.style.width=+result+'px';   
                if( img_index.status==0){
                    item.style.width=duration+'px';
                    item.style.backgroundColor='red';
                    item.innerHTML='<p style="opacity:0.6; text-align:center;">ошибка при загрузки</p>';
                }else if( img_index.status==1){      
                    console.info('image ',i,'loaded');
                    item.style.width=duration+'px';
                    item.innerHTML='<p style="opacity:0.6; text-align:center;">загрузка завершена</p>';
                }else{
                    setTimeout(progressfunc,10)
                }
            },10) 
        }
                   console.info('progres created')
                   anim(i)//call progress bar
  }
    var foto=function(rightdiv,i){
        var files_not_upload=root.not_upload_img;
        console.info('*******fileinfo active -foto******');
        var div=document.createElement('div');
        div.classList.add('fileloader_file_foto');
        div.setAttribute('style','text-overflow:ellipsis;width:100%;background-color:#7F827F;color:#fff;overflow:hidden;padding:0px;float:right')
        var img=document.createElement('img');
        img.setAttribute('style','height:100%;width:100%')
        console.log('now i ',i, 'notupload arr is ',files_not_upload)
        img.setAttribute('src',files_not_upload[i].foto)
        div.style.height='100%';
        div.appendChild(img);
        rightdiv.appendChild(div)
        console.info('fileinfo active -foto div created')
   }
    var description=function(leftdiv,y){
        console.info('*******fileinfo active -description******');
        var div=document.createElement('textarea');
        div.setAttribute('jsfl-pos',y)
        div.classList.add('jsfl_textarea');
        div.setAttribute('style','outline:none;border:none;background-color:#fff;width:100%;background-color:#fff;color:#000;overflow:hidden;padding:0px;height:60px;')
        div.setAttribute('placeholder','Введите описание файла');
        leftdiv.appendChild(div)
        console.info('filname div created')
}
    var deletebtn=function(rightdiv,l){
        var del_text=document.createTextNode('x'),
        del_text_p=document.createElement('p'),
        wrapdiv=rightdiv,
        wrap_div_h=files_root.style.height,
        opacity_div=document.createElement('div'),
        del_div=document.createElement('div'),
        delindex=rightdiv.parentNode.getAttribute('delindex')
        del_text_p.setAttribute('jsfl-pos',delindex)
        del_text_p.appendChild(del_text);
        del_text_p.classList.add('jsfl-del-item');
        del_text_p.setAttribute('style','font-weight:bold;color:#fff;font-size:1.2em;text-align:center;cursor:pointer;position:absolute;;z-index:100;width:100%;opacity:0.9;top:40%');
        opacity_div.setAttribute('style','opacity:0.5;background-color:#323232;height:100%;width:100%;position:relative;top:0px;left:0px;');
        del_text_p.style.lineHeight=wrap_div_h;
        del_div.setAttribute('style','position:absolute;right:0px;;top:0px;width:25px;height:100%;');
        del_div.appendChild(del_text_p)
        del_div.appendChild(opacity_div);
        wrapdiv.appendChild(del_div);
   }
 
    var x_l=document.querySelectorAll(mainroot+' img').length,  argitems=[],w;
    var mainroot_width=document.querySelector(mainroot).offsetWidth;
    if(mainroot_width>600){w='25%'}else if(mainroot_width<400){w='20%'}

    console.log('is undefuned ',root.filesarr == undefined)
    if(root.filesarr == undefined){
       root.filesarr=[] ;
       for(var z in arg){
            switch(z){
                case 'progress':
                     root.filesarr.push(['progress',progress]);break;
                case 'foto':
                     root.filesarr.push(['foto',foto]);break;
                case 'description':
                      root.filesarr.push(['description',description]);break;
                case 'delete':
                       root.filesarr.push(['deletebtn',deletebtn]);break;
            }
        }
    }
    var argitems_length=root.filesarr.length,files_not_upload=root.not_upload_img;
    for(var i=files_length-1;i>=0;i--)  {  
        var main_wrap=document.createElement('div'),
        background_div=document.createElement('div'),
        delindex=files_not_upload[i].delindex;
        main_wrap.classList.add('progress_files_wrap');
        main_wrap.setAttribute('delindex',delindex);
        background_div.setAttribute('style','position:absolute;left:0px;top:0px;background-color:#1d3b45;opacity:0.4;width:100%;height:100%;z-index:-1;padding:10px;');
        main_wrap.appendChild(background_div);
        main_wrap.setAttribute('style','width:100%;margin:15px 0px;position:relative;');
        if(arg['description'] != undefined){main_wrap.style.height='100px'}
        else if(arg['foto'] != undefined ){main_wrap.style.height='80px'}else{main_wrap.style.height='60px'}
        leftdiv=document.createElement('div');
        leftdiv.setAttribute('style','float:left;width:65%;height:100%;position:relative;z-index:2')
        rightdiv=document.createElement('div');
        rightdiv.setAttribute('style','float:right;width:35%;height:100%;')
        main_wrap.appendChild(leftdiv);
        main_wrap.appendChild(rightdiv);
        files_root.appendChild(main_wrap);
        for(var j=0;j<argitems_length;j++){
             var div=root.filesarr[j][0]=='progress' || root.filesarr[j][0]=='description' ? leftdiv :rightdiv;
             var item=root.filesarr[j][1];
             root.filesarr[j][1].call(root,div,i)
        }
    }
}
FileLoader.Files.prototype=Object.create(FileLoader.Root.prototype);
FileLoader.Files.prototype.constructor=FileLoader.Files;

FileLoader.Files.prototype.settings=function(arg)   {
    
    if(typeof(arg) !='object')  {console.log('lsfl-error','Files-Settings','неверный тип параметра,должен быть обьект');return false;}
    if(arg['desc-rewrite']==true){
        var txtarea_list=document.querySelectorAll('.jsfl_textarea');
        if(txtarea_list.length >0){
            var l=txtarea_list.length;
            for(var i=0;i<l;i++){
                txtarea_list[i].setAttribute('rewrite',1)
                
            }
        }
        }
                
 }

