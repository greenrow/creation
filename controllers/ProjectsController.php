<?php
namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\art\User;
use app\models\art\Project;
use app\models\art\Category;
use app\models\art\Categ_projects;
use app\models\art\Project_foto;
use app\models\art\Project_area;
use app\models\art\Likes;
use app\models\art\Userinfo;
use app\models\art\Map_coords;
use app\models\art\General_settings;
use app\models\art\Interests;
use app\models\art\Tag;
use app\models\art\Project_tag;
use app\models\art\Comments;
use app\models\art\Friends;
use app\models\art\Subscribers;
use app\models\art\News;
use app\models\art\Bookmarks;
use app\models\art\Project_settings;
use app\models\art\Setting_friendlist_showproject;
use app\models\art\Setting_friendlist_perpage;
use app\models\art\Setting_friendlist_commentproject;
use app\models\art\Settings_project_friendlist_comment;
use app\models\art\Settings_project_friendlist_showproject;

$rrr=777;
class ProjectsController extends Controller

{
    
    protected function getuserid($nickname){
        
         $user=new User;
            $user_id=$user->find_id_nickname($nickname);
            return $user_id;
    }
    
    protected function user_id(){
        
        return Yii::$app->user->getid();
    }
    public $rr='3';
    
    public $showproj=[];
        
    public $layout = 'personalpage';
 
   public function actionIndex($nick){
       
       if ($nick && $nick !=Yii::$app->user->identity->nickname){
     
           return $this->render('forbidden_error',['message'=>'Доступ к данному разделу запрещен']);
         
       }else{   

           $freinds_obj= new Friends;
           $friends_arr= $freinds_obj->getacceptedFriends($this->user_id());
           return $this->render('index',['friends_arr'=>$friends_arr]);

       }

    }
        
   public function actionGetallprojects(){
        $proj= new Project;
        $rez=$proj->get_all_publish_proj();
        foreach($rez as $key){  
            $this->publish_project_info['project_all'][$key['id']]= $key;
            $categ=$this->get_cur_proj_cat($key['id']);
            $this->publish_project_info['project_all'][$key['id']]['category']=  $categ;
          //очищяем массив
            $categ_arr=array();  
        }
   
       return json_encode($this->publish_project_info);
    }
    
    
    public function actionShowcateg(){
           $txt='';
            $obj=new Category;
           $rez=$obj->getall_category();
            foreach($rez as $key){

                    $txt=$txt. '<div><div class="add_proj_categ_div categ_wrap">'.$key['name'].'</div></div>';
                 }

        return $txt;
    }
    

        public function actionAddtitleptoj(){
                $avatarload=new Userpage;
                $ava_get=$avatarload->getAvatar();
                if($ava_get==''){
                        $ava_get='/images/no_foto.png';
                }
                return   $ava_get;
        }

        public function actionTemplateaddproject(){
             $freinds_obj= new Friends;
            $friends_arr= $freinds_obj->getacceptedFriends($this->user_id());
            return $this->render('addproject',['friends_arr'=>$friends_arr]);
        }

         public function actionRemove_project(){
            
                $proj_id=Yii::$app->request->post('projid');
                $proj=new Project();
                $rez=$proj->remove_project($proj_id);
        }
        public function actionProjects(){
            
                return $this->render('projects');
        }

        public function actionAddfoto(){

                $files=[];
                $filetype=[];
             $tempdir= $_SERVER['DOCUMENT_ROOT'].'/uploads/files/temp/'.Yii::$app->user->getid().'/';
             if (!is_dir($tempdir)){
                 mkdir($tempdir);
             }
             $imgdir='uploads/files/temp/'.Yii::$app->user->getid().'/';
     
                $file_arr=$_FILES['filesarr'];
                  
                $files_length=sizeof($_FILES['filesarr']['name']);

                    for($i=0;$i< $files_length;$i++){
                          $type=$file_arr['type'][$i];
                                if($type=='image/png' || $type=='image/jpeg' ){
                                    $name=$file_arr['name'][$i];
                                    $image_type=substr($file_arr['name'][$i],-3);
                                    $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$image_type;
                                    $full_filename="$imgdir$file_date_name";
                                    $full_path="$tempdir$file_date_name";
                                    $image=$file_arr['tmp_name'][$i]; 
                          
                                    $uploadfile=  move_uploaded_file($image,$full_path);
                                    if($uploadfile){
                                        $files['foto'][$i]['path']=$full_filename;
                                        $files['foto'][$i]['fotoname']=$file_date_name;
                                     }
                                     
                                }else{

                                        $files['foto'][$i]['error']='Неверный формат файла\n\выберите файл с расширением png или jpeg';
                                }
                    }

                return json_encode($files);

        }
        protected function removecategory($categ_id,$proj_id){
           $cateprogobj=new Categ_projects();
            $rez=$cateprogobj->del_category($categ_id,$proj_id);
            return $rez;
        }
       protected function replacefiles($files_arr){
            $itemdir=$_SERVER['DOCUMENT_ROOT'].'uploads/files/temp/'.Yii::$app->user->getid().'/';
            $newdir=$_SERVER['DOCUMENT_ROOT'].'uploads/files/';
            $dir_elelms_arr=scandir($itemdir);
            $dir_elelms_arr_l=sizeof($dir_elelms_arr);
//            $equakelems=array_intersect($files_arr,$dir_elelms_arr);
//            $equal_elems_arr_l=sizeof($equakelems);
            $files_arr_l=sizeof($files_arr);

            for($i=0;$i<$files_arr_l;$i++){
                   $rez=copy($itemdir.$files_arr[$i],$newdir.$files_arr[$i]);
             }
             
             for($i=0;$i<$dir_elelms_arr_l;$i++){
                 if($dir_elelms_arr[$i] != '.' && $dir_elelms_arr[$i] != '..'){
                       unlink($itemdir.$dir_elelms_arr[$i]);
                 }

             }
            if(rmdir($itemdir)){
                return 1;
            };
        }

        public function actionPublish_proj(){
                $proj_id=$_POST['proj_id'];
                $proj= new Project;
                $proj->publish_proj($proj_id);
        }
        
        public function actionGetprojcat($proj_id){
                $categ_project=  new Categ_projects;
                $category_rez=$categ_project->get_cat($proj_id);
                return      $category_rez;
        }
        
        public function actionGetpublishstatus($proj_id){
                $project=  new Project;
                $status_rez= $project->find_publish($proj_id);
                return $status_rez;
        }
        
        
        

        //Добавляем проект//
        public function actionAddproject(){
            
            

         
         function set_friend_list($class,$post_filed,$settings_arr,$proj_id){
                    $user_id=Yii::$app->user->getid();      
                $settings_project= new $class;
                      $size=sizeof($settings_arr[$post_filed]['friendlist']);
          
                      $values_arr=[];
                       $user=new User;
                    for($i=0;$i<$size;$i++){
                          $value=$settings_arr[$post_filed]['friendlist'][$i];
                          $friend_id=$user->find_id_nickname($value);
                          if(gettype($friend_id=='integer')){
                            array_push($values_arr,"(".$proj_id.','.$friend_id.")");  
                          }
                     }
                     $expl_string=implode(',',$values_arr);
                    
                     $settings_project->set_items($expl_string); 
         }
         
         
                $name=Yii::$app->request->post('name');
                $ava=Yii::$app->request->post('ava');
                $bckg=Yii::$app->request->post('bckg');
                $desc=Yii::$app->request->post('desc');

                $age_rule=Yii::$app->request->post('age_rule');
                $settings_arr=Yii::$app->request->post('settings');

                $age_rez=$age_rule =='ok' ? 1 : 0;
                $date= sprintf('%04d,%02d,%02d',date('Y'),date('m'),date('j'));
                
                
                $proj= new Project;
                $proj_settings=new Project_settings;
                     
                
                //proj settings
                
             if(array_key_exists('project_show_rule', $settings_arr)){
                $showproj;
                $show_val=$settings_arr['project_show_rule']['val'] ;
                     
                  switch($show_val){
                      case('all'):
                          $showproj=0;break;
                      case('friend'):
                          $showproj=1;break;
                      case('some_friend'):
                          $showproj=2;break;
                      case('nobody'):
                          $showproj=3;break;
                  }
            } 
            
            if(array_key_exists('comment_show_rule', $settings_arr)){
                $comm;
                $show_val=$settings_arr['comment_show_rule']['val'] ;
                     switch($show_val){
                         case('all'):
                            $comm=0;break;
                        case('friend'):
                            $comm=1;break;
                         case('some_friend'):
                            $comm=2;break;
                        case('nobody'):
                            $comm=3;break;
                      
                    }
            } 
            

                $proj->projset($name,$ava,$bckg,$desc,$this->user_id(),$date);
           
                $proj_id=$proj->proj_id($this->user_id());
                
                
                if(array_key_exists('friendlist', $settings_arr['comment_show_rule'])){

                         set_friend_list("app\models\art\Settings_project_friendlist_comment",'comment_show_rule',  $settings_arr,$proj_id);
                }
                                
                if(array_key_exists('friendlist', $settings_arr['project_show_rule'])){

                         set_friend_list("app\models\art\Settings_project_friendlist_showproject",'project_show_rule',  $settings_arr,$proj_id);
                }
                
                
                
                $proj_settings->set($proj_id,$showproj,$comm,$age_rez);
                return   $proj-> proj_id($this->user_id());
        }

        //изменяем проект

        public function actionChangeproject(){
            $projectinfo=Yii::$app->request->post('main_project_arr');
            $proj_id=$projectinfo['projid'];
            $delitems_list;
            if(array_key_exists('delitems',  $projectinfo)){
                
                foreach($projectinfo['delitems'] as $key=>$val){
                    if(sizeof($val) !=0){
                        $delitems_list=sizeof($val)>1?implode(',',$val) :$val[0];
        
                            switch ($key){
                                case 'categ':
                                    $this->removecategory($delitems_list,$proj_id);
                                    break;
                                case 'tag':
                                   
                                    $this->deltag_proj($delitems_list);
                                     break;

                                case 'area':
                                     $this->delarea_proj($delitems_list,$proj_id);
                                     break;

                                case 'foto':
                  
                                    $this->delfoto_proj($delitems_list,$proj_id);
                                     break;
                                case 'fotodel':
                                  
                                    $this->removefile($val);//array of src items
                                
                                    break;
                                         }
                    }
                }
            }
            
            if(array_key_exists('newitems',  $projectinfo)){
                
                foreach($projectinfo['newitems'] as $key=>$val){
                    if(sizeof($val) !=0){
                       if($key !='foto'){
                           $items_list=sizeof($val)>1?implode(',',$val) :$val[0];
                       }else{
                      
                       }
                           
        
                            switch ($key){
                                case 'categ':
                                    $this->addcategory_proj($proj_id,$val);
                                    break;
                                case 'tag':
                                   
                                    $this->Addtag_proj($proj_id,$val);
                                     break;

                                case 'area':
                                     $this->Addarea_proj($val);
                                     break;

                                case 'foto':
                    
                                    $this->Addfoto_proj($val);
                                     break;
                          
                                         }
                    }
                }
            }

        
            if(array_key_exists('name',  $projectinfo)){  
                        $name=$projectinfo['name'];
                        $this->changehproject_name($name,$proj_id);
            }        
             if(array_key_exists('updatearea',  $projectinfo)){  
                 
                   $arealist=implode(',',$projectinfo['updatearea']);
                        $this->changeareaval($arealist);
             }
             
             if(array_key_exists('ava',  $projectinfo)){
                 
                 $this->changehproject_ava($projectinfo['ava'],$proj_id);
                 $changeava=$projectinfo['ava'];
             } 
                         
            if(array_key_exists('newsort',  $projectinfo)){
           
              foreach($projectinfo['newsort'] as $key=>$val){
                $items_list=sizeof($val)>1?implode(',',$val) :$val[0];
    
                    if(sizeof($val) !=0){
                        
                        switch($key){

                            case 'area':
                             $this->changeareasort($items_list);//array of src items
                                break;
                            case 'foto':
                                   $this->changefotosort($items_list);
                                break;
                        }

                    }
                   
              }
                  
              }
            if($changeava){
                
                return json_encode(["ava"=>$changeava]);
            }
                                   
                      

        }
        

        //добавлем контент проекта//
     public function actionSetprojectcontent(){
        $rez_arr=Yii::$app->request->post('main_project_arr');
        $status;
        $servermessage;
        $proj_id=$rez_arr['project_id'];
        $proj_id !=false && sizeof($rez_arr) > 0 && $proj_id !=null  ? $status=1: 0;
        if($status){
                
                if(array_key_exists ('category',$rez_arr)){
                    $this->Addcategory_proj($proj_id,$rez_arr['category']);
                }
                if(array_key_exists ('tags',$rez_arr)){
                    $this->Addtag_proj($proj_id,$rez_arr['tags']);
                }
                if(array_key_exists ('area',$rez_arr)){

                    $this->Addarea_proj($rez_arr['area']);

                }

                if(array_key_exists ('img',$rez_arr)){
                    $this->Addfoto_proj($rez_arr['img']);
                }

                 //send info to sbscribers
                
                $Subscriber= new Subscribers;
                $hasSubscriber=$Subscriber->find_subscribers($this->user_id());
                if ($hasSubscriber){
   
                    $news=new News();
                    
                    forEach($hasSubscriber as $key){
                           $news->set_news_project($key['subscriber_id'],$proj_id);
                    }
                    
                }
                
                 $servermessage='Проект успешно добавлен.';

        } else{$servermessage='В процессе добавления проекта возникли ошибки. Попробуйте еще раз..';
        
        
        }

        return $servermessage;
    }

        
                //********Записывае данные о проекте**********//
        
                            //категория//
                
        protected function Addcategory_proj($proj_id,$categ_arr ){
                $category = new Category;
                $categ_project = new Categ_projects;

                foreach($categ_arr as $key){
//                        if($category->find_categ($key)==0){
//                                $category->setcateg($key);  
//                        }
                        $categ_id = $category->getcateg_id($key); 
                        $categ_project->setcateg_proj($categ_id,$proj_id);
                }
        }
        protected function changehproject_name($name,$projid){
            $proj= new Project;
             $proj->changeproject_name($name,$projid);
        }
        
      protected function changehproject_ava($ava,$projid){
            $proj= new Project;
             $proj->changeproject_ava($ava,$projid);
        }
                                 //теги//
                
        protected function Addtag_proj($proj_id,$tag_arr ){
            $tags=  new Tag;
            $project_tag= new Project_tag;

            foreach($tag_arr as $key){
                    if($tags->find_tag_count($key)==0){
                        $tags->settag($key);
                    }
                    $tag_id=$tags->gettag_id($key);
                    $project_tag->settag_proj($tag_id,$proj_id);
            }
        }
        
       protected function gettag_proj($proj_id){
        
            $project_tag= new Project_tag;
            $tagarr=$project_tag->get_tag($proj_id);
            return $tagarr;
         
        }
        
       protected function deltag_proj($tag_arr){
        
            $project_tag= new Project_tag;
            $project_tag->deltag_proj($tag_arr);
      
         
        }
        
       protected function delarea_proj($area_arr,$proj_id){
        
            $project_tag= new Project_area;
            $project_tag->delarea_list($area_arr,$proj_id);
      
         
        }
        
        protected function delfoto_proj($foto_arr,$proj_id){
        
            $project_tag= new Project_foto;
            $project_tag->delfoto($foto_arr,$proj_id);
      
         
        }
        
        protected function changeareasort($val){
            
                 $project_tag= new Project_area;
                $project_tag->update_area_pos($val);
            
            
        }
        
             protected function changeareaval($val){
            
                 $project_tag= new Project_area;
                $project_tag->update_area_val($val);
            
            
        }
        
        
       protected function changefotosort($val){
            
                 $project_tag= new Project_foto;
                $project_tag->update_foto_pos($val);

        }
        protected function removefile($src_arr){
            $l=sizeof($src_arr);
             for($i=0;$i<$l;$i++){
               $del_file_path=Yii::getAlias('@webroot').$src_arr[$i];
                  if(file_exists ($del_file_path)){
                           unlink($del_file_path);
                  }
             }
       
         }
        

                            //Фото//
        protected function Addfoto_proj($imgarr){
            $proj_foto = new Project_foto;
            $tmpfoto_arr=[];
            $fileslist=sizeof($imgarr['files'])>1?implode(',',$imgarr['files']) :$imgarr['files'][0];
          
            $proj_foto->setfoto($fileslist);
            //replace files
            $this->replacefiles($imgarr['replacefiles']);
        }
                            //Текст//
        protected function Addarea_proj($area_arr){
            $proj_area = new Project_area;
            $area_list=sizeof($area_arr)>1?implode(',',$area_arr) :$area_arr[0];
            $proj_area->setarea($area_list);
        }
  
            //-----**********-----------показываем данные проекта-----------**************-----//
             
                         //категории проекта//
        protected function ProjectCategory($proj_id){
                $categ = new Category; 
                $category = $categ->getcateg($proj_id);
                    $this->showproj['all']['category']=    $category;
        }
         
                          //личная информация//
//         protected function PersonalInfo($user_id){
//    
//                $userinfo = new Userinfo;
//                  $userinfo_settings= new General_settings;
// 
//                  $getuserinfo = $userinfo->getuserinfo($user_id);
//                    
//                  $user_settings=$userinfo_settings->get_all_info($user_id);
//      
//                 /*прверяем включены ли настройки*/
//                  if(is_array( $user_settings) ){
//      
//                        foreach($user_settings as $key=>$value){
//                         
//                                if ($value == 1 && $key !='id'){
//
//                                    /*убираем название  usinfo_ чтобы ключ сопадал с название поля из таблицы userinfo*/
//                                    $key=substr($key,7);
//
//                                     $this->showproj['all']['userinfo'][$key]= $getuserinfo[$key];
//
//                                  
//                                }
//                        }
//    
//                 }else{
//                           $this->showproj['all']['userinfo'] = $getuserinfo;
//                 }
//                 
//                 /*дополняем именем id, логином и авой*/
//                $this->showproj['all']['userinfo']['nickname']=$getuserinfo['nickname'];
//                $this->showproj['all']['userinfo']['ava']=$getuserinfo['ava'];
//                $this->showproj['all']['userinfo']['user_id']=$getuserinfo['user_id'];
//                                      
//                if(Yii::$app->user->isGuest || Yii::$app->user->id == $getuserinfo['user_id']){
//                    $this->showproj['all']['userinfo']['loginin']= 'no';
//                }else{$this->showproj['all']['userinfo']['loginin']= 'ok';
//                 };
//                        
//                                   
//        }

        protected function PersonalProject($proj_id,$user_id){
                $proj = new Project;
                $foto = $proj->get_projectfoto_details($proj_id,$user_id);
                $area = $proj->get_projectarea_details($proj_id,$user_id);
                $proj_info=$proj->get_project_info($proj_id,$user_id);
                
                /*Общая информация о проекте*/
                $this->showproj['all']['project_info']=$proj_info;
           
                            //если в проекте есnь фото и текст//
                if(count($foto)>0 && count($area)>0){
                    foreach($foto as $key){
                            $this->showproj['all']['project'][] = $key;
                    }
                    foreach($area as $key){
                            $this->showproj['all']['project'][] = $key;
                    }
                    array_multisort($this->showproj['all']['project'], SORT_ASC );
                }
                       
                if(count($foto)>0 && count($area) == 0){
                    foreach($foto as $key){
                            $this->showproj['all']['project'][]=$key;
                    }
               }
                       
                if(count($area)>0 && count($foto)==0){
                        foreach($area as $key){
                                $this->showproj['all']['project'][]=$key;
                        }
                }
         }
         
         public function actionEditprojectcontent(){
           $proj_id = Yii::$app->request->post('proj_id'); 
           $user_id=Yii::$app->user->getid();
           $this->PersonalProject($proj_id,$user_id);
           $this->ProjectCategory($proj_id);
           $tags=$this->gettag_proj($proj_id);
           $this->showproj['tags']=$tags;
           $this->showproj['proj_id']=$proj_id;
           return json_encode($this->showproj);
             
             
         }
 public function actionGetprojectsetting(){
     
        $proj_id = Yii::$app->request->get('proj_id');
        $user_id=Yii::$app->user->getid();
        $setting_friendlist=[];
        $proj_settings = new Project_Settings;
        $user = new User;
        $project= new Project;
        $projsettings=$proj_settings->get($proj_id);
        $proj_info= $project->get_project_info($proj_id,$user_id);
        $proj_name=$proj_info['name'];


       
        
        if($projsettings['show_rule']==2){
                    $project_friendlist= new Settings_project_friendlist_showproject;
                    $rez=$project_friendlist->getitems($proj_id);
                    if(sizeof($rez) > 0){
            
                        $size=sizeof($rez);
                        for($i=0;$i<$size;$i++){
                            $setting_friendlist['project_friend'][$i]=$user->find_nickname_by_id($rez[$i]['friend_id']);
                        }
                    }
              
        }        
                 
                if($projsettings['comment_rule']==2){   
                    $project_friendlist= new Settings_project_friendlist_comment;
                    $rez=$project_friendlist->getitems($proj_id);
                  
                    if(sizeof($rez) > 0){
                     
                        $size=sizeof($rez);
                       
                        for($i=0;$i<$size;$i++){
                            $setting_friendlist['comment_friend'][$i]=$user->find_nickname_by_id($rez[$i]['friend_id']);
                        }
                    }

                }
                
           $setting_friendlist['comment_rule']=$projsettings['comment_rule'];
             $setting_friendlist['project_rule']=$projsettings['show_rule'];
             $setting_friendlist['age_rule']=$projsettings['age_rule'];
             $setting_friendlist['projname']=$proj_name;
          
          return json_encode ($setting_friendlist);
       
        }
        
        
     
 
        public function actionShowproj(){
            $showproj=[];
            $proj_id = Yii::$app->request->get('proj_id');
            $user_nick = Yii::$app->request->get('nick_name');
            $get_views=new Project;
            $time_zone=new Map_coords;
            $userinfo = new Userinfo;
            $friends= new Friends;
            $comment_obj= new Comments;
            $likes= new Likes;
            $bookmarks= new Bookmarks;
            $myid=Yii::$app->user->id ? Yii::$app->user->id :0;
            $uservar='';
            if($myid){
             $uservar =" OR

 
 ( u.id IN(SELECT fr.user_id FROM friends fr WHERE fr.friend_id=".$myid." AND fr.submit_friend=1)
OR
  u.id IN(SELECT fr.friend_id FROM friends fr WHERE fr.user_id=".$myid." AND fr.submit_friend=1)
  )
 
 AND 
 
 (s.comment_rule=2 OR (s.comment_rule=0 and ps.comment_rule=1))   AND p.id=:proj_id

OR (
  (s.comment_rule=3 OR (s.comment_rule=0 and ps.show_rule=2)) AND
  u.id IN( SELECT fr_list.user_id FROM setting_friendlist_commentproject fr_list 
  WHERE fr_list.friend_id=".$myid.") 
)AND p.id=:proj_id";   
             
            }else{$uservar='';}
            
             $commentrule=$comment_obj->commentrule($proj_id,$uservar);

             if($commentrule){
                 
                 $comments=$comment_obj->showcomm($proj_id);
                
             }else{$comments=0;}
            
            
            
                /*my time_zone*/
            $timezone_user=$time_zone->get_time_zone(Yii::$app->user->getid());
            if (sizeof( $timezone_user) == 0){  $timezone_user = 'Europe/Moscow';}
            $date_user_zone = new \DateTime('now', new \DateTimeZone($timezone_user));
            $user_date= $date_user_zone ->format('Y-m-d');
        
           
       
            $user_id=$this->getuserid($user_nick);
            $views=$get_views->getview_of_one_project($proj_id);
            $likes_rez=$likes->get_likes($proj_id);
            
            //bo0kmark
            
       
            if($myid){
                $isbookmark=$bookmarks->findbookmark($myid,$proj_id);
                 $this->showproj['all']['bookmark']= $isbookmark;
           
            }
            
            
            if($comments && sizeof($comments) > 0){
                $this->showproj['all']['comments']= $comments;
            }

               //отображаем категории//
             $this->ProjectCategory($proj_id);

             //отображаем содержимое проекта//
            $this->PersonalProject($proj_id,$user_id );

            //возвращаем масив данных о проекте//
            $ip=$this->getIP();

            $rez=$get_views->getproject_view($ip,$proj_id,$user_date);
            if($rez== 'ip_insert'){ $this->showproj['all']['view']='ok';}

            $this->showproj['all']['loginIn']= Yii::$app->user->isGuest ?    0 :Yii::$app->user->id;


            //loginin or not
            $getuserinfo = $userinfo->getuserinfo($user_id);
            $this->showproj['all']['userinfo']=$getuserinfo;
            $this->showproj['all']['likes']=$likes_rez;
            $this->showproj['all']['views']=$views;
           

            
            $this->showproj['all']['userinfo']['myproject'] = ($myid== $getuserinfo['user_id']) ? 1 :0;
                            //show ddfriend div or not
            if($myid==Yii::$app->user->id ){              $isfriend=$friends->check_friend($myid,$user_id);
            $this->showproj['all']['userinfo']['isfriend']= $isfriend;}

                //nickname
               
                              
                if(Yii::$app->request->isAjax){return json_encode($this->showproj);}else{
                
                    $this->showproj['all']['servercall']='true';
                    return $this->render('showproject');
                };
                
        }    
        
        /*get_ip*/
        
    protected function getIP(){

$headers = array('HTTP_X_FORWARDED_FOR', 'HTTP_X_CLUSTER_CLIENT_IP',
 'HTTP_FORWARDED_FOR', 'HTTP_X_FORWARDED',
 'HTTP_FORWARDED', 'HTTP_VIA', 'HTTP_X_COMING_FROM',
'HTTP_X_COMING_FROM', 'HTTP_COMING_FROM',
'REMOTE_ADDR');
foreach ($headers as $header) {
    if (isset($_SERVER[$header])) {
    return $_SERVER[$header];
    }
}

    }
    
    public function actionSetbookmark(){
        $proj_id=Yii::$app->request->post('proj_id');
        $bookmark= new Bookmarks;
        $user_id=Yii::$app->user->id;
        $bookmark->setprojectbookmark($proj_id,$user_id);
        
    }
    
    public function actionRemovefile(){
        $path=Yii::$app->request->post('path');
         $del_file_path=Yii::getAlias('@webroot').$path;
         
          if(file_exists ( $del_file_path)){
                   unlink($del_file_path);
            }
        
    }
    public function actionLoadcropfile(){
         
        $postfoto=Yii::$app->request->post('crop_foto');
        $oldfoto=Yii::$app->request->post('path');                
        $foto_arr=explode(';',$postfoto);
        $foto_data=explode(',',$foto_arr[1]);
        $foto_type_arr=explode('/',$foto_arr[0]);

        $foto_data=$foto_data[1];

        $foto_type= $foto_type_arr[1];
     
      
  
      if($foto_type=='png' || $foto_type=='jpeg' ){
            $webroot='/uploads/files/';
           $del_oldfoto_path=Yii::getAlias('@webroot').$oldfoto;

            if(file_exists ( $del_oldfoto_path) && $oldfoto !='/images/nofoto.png' ){
                   unlink($del_oldfoto_path);
            }
         
        
            $serverroot= Yii::getAlias('@webroot').$webroot;

            $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$foto_type;
            $fullimgname="$serverroot$file_date_name";

            $data = base64_decode($foto_data);
            $rez=file_put_contents($fullimgname, $data );
            
            /*вывод временный файл*/
            $websrc="$webroot$file_date_name";
            
            
            /*Запись в базу*/
                     /*вывод пути к файлу в js*/
            return  $websrc;

        }else{ 
                 
                 return 'выбран неверный тип файла';
                 
             }
    
    
}


        public function actionGetlocation(){
                $lat = htmlspecialchars($_GET['lat']);
                $lng = htmlspecialchars($_GET['lng']);
                $location = htmlspecialchars($_GET['loc']);
        
                return $this->renderAjax('location',['lat'=>$lat,'lng'=>$lng,'loc'=>$location]);
        }

 
    
  
        
        public function actionGetava(){

            $avatarload=new Userinfo;

             $ava_get=$avatarload->getAvatar();
            if($ava_get=='')
             {
                 $ava_get='/images/no_foto.png';

             }
            return $ava_get; 
         
}

public function actionAddcomm(){
      $date=new \DateTime('now',new \DateTimeZone('Europe/Moscow'));
      $curdate=$date->format('Y-m-d H:i:s');
      $proj_id=Yii::$app->request->post('projid');
      $user_id=Yii::$app->request->post('userid');
      $text=Yii::$app->request->post('text');
      $userinfo= new Userinfo;
      $ava=$userinfo->getAvatar();

      $comment_obj= new Comments;
       $rez=$comment_obj->sendcomm($user_id,$proj_id,$text,  $curdate);
       $arr['comment']=['proj'=>$proj_id,"text"=>$text,'date'=>  $curdate,'ava'=>$ava];
       
       
       if($rez){return json_encode($arr);}
}


public function actionProjectedit(){
    $proj_id=Yii::$app->request->post('proj_id');
    
             function set_friend_list1($class,$post_filed,$settings_arr,$proj_id){
                    $user_id=Yii::$app->user->getid();      
                $settings_project= new $class;
                      $size=sizeof($settings_arr[$post_filed]['friends']);
          
                      $values_arr=[];
                       $user=new User;
                    for($i=0;$i<$size;$i++){
                          $value=$settings_arr[$post_filed]['friends'][$i];
                          $friend_id=$user->find_id_nickname($value);
                          if(gettype($friend_id=='integer')){
                            array_push($values_arr,"(".$proj_id.','.$friend_id.")");  
                          }
                     }
                     $expl_string=implode(',',$values_arr);
                    
                     $settings_project->set_items($expl_string); 
         }

      function del_friend_list($class,$post_filed,$settings_arr,$proj_id){


                 $user_id=Yii::$app->user->getid();
               $friendlist_perspage=new $class;
              $size=sizeof($settings_arr[$post_filed]['del']);
     
              $friend_id_arr=[];
              $user=new User;
              for($i=0;$i<$size;$i++){
                  $value=$settings_arr[$post_filed]['del'][$i];
        
                   $friend_id=$user->find_id_nickname($value);
             
                  if(gettype($friend_id)== 'integer'){
                      array_push($friend_id_arr,$friend_id);
                  }
              }
          
              if(sizeof($friend_id_arr)>0){
              
                 $sepp=implode(',',$friend_id_arr);
                 $friendlist_perspage->del_items($proj_id,$sepp);
              }
         }
         
         
         
    $editarr=Yii::$app->request->post('editarr');
    
    $age_rule=  $editarr['age'];
    
    if(array_key_exists('comm', $editarr)){
        
        $val=$editarr['comm']['val'];
        
            switch($val){

              case('all'):
                  $commproj=0;break;
              case('friend'):
                   $commproj=1;break;
              case('some_friend'):
                   $commproj=2;break;
              case('nobody'):
                   $commproj=3;break;

            }
        
        if(array_key_exists('friends',$editarr['comm'])){
      
            set_friend_list1("app\models\art\Settings_project_friendlist_comment",'comm',$editarr,$proj_id);
        }
        
        if(array_key_exists('del',$editarr['comm'])){
  
            del_friend_list("app\models\art\Settings_project_friendlist_comment",'comm',$editarr,$proj_id);

        }

     
    }
    
    if(array_key_exists('proj', $editarr)){
        
        $val=$editarr['proj']['val'];
        
            switch($val){

              case('all'):
                  $showproj=0;break;
              case('friend'):
                  $showproj=1;break;
              case('some_friend'):
                  $showproj=2;break;
              case('nobody'):
                  $showproj=3;break;

            }
        
        if(array_key_exists('friends',$editarr['proj'])){
            
             set_friend_list1("app\models\art\Settings_project_friendlist_showproject",'proj',$editarr,$proj_id);
        }
        
       if(array_key_exists('del',$editarr['proj'])){
       
            
           $rez=  del_friend_list("app\models\art\Settings_project_friendlist_showproject",'proj',$editarr,$proj_id);

            
            
        }

    }
    
    
    //set val to projseting
    
      $projset= new Project_settings;
            $projset->set($proj_id,$showproj,$commproj, $age_rule);
            return 1;

}


}