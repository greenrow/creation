<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\art\Map_coords;
use app\models\art\Interests;

use app\models\art\Userinfo;
use app\models\art\Friends;
use app\models\art\Events;
use app\components\SocketClient;
use app\components\MessageSocket;
use app\models\art\Message;


class MessageController extends Controller{
    

    public $layout = 'personalpage';    

    public function actionIndex(){
        
        $user_id=Yii::$app->user->getid();
        $message= new Message;
        $rez=$message->getmessagelist($user_id);
       
//
       return $this->render('index',['messagelist'=>$rez]);

    }
    

public function actionGetmessage(){
       $user_id=Yii::$app->user->getid();
        $friend_id=Yii::$app->request->get('friend_id');
        $message=new Message();
        $rez=$message->getmessages($user_id,$friend_id);
        $jsonarr=[];
       
        
       
        foreach ($rez as $key){
            
             $jsonarr['message_info'][$key['id']]['date']=$key['date'];
             $jsonarr['message_info'][$key['id']]['text']=$key['text'];
               $jsonarr['message_info'][$key['id']]['ava']=$key['ava'];
                $jsonarr['message_info'][$key['id']]['login']=$key['login'];
        }
      return  Yii::$app->request->isAjax ? json_encode($jsonarr): $this->render('showmessage',['messarr'=>$jsonarr['message_info']]);
     
       
    
}


}