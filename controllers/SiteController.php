<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\art\User;
use app\models\art\Project;
use app\models\art\Map_coords;
use app\models\art\Userinfo;
use app\models\art\Categ_projects;
use app\models\art\Likes;
use app\models\art\User_session;
use app\components\Test;
use app\components\Websocket;
use app\components\socketClient;
use app\components\RegisterObj;
use app\models\art\Message;
use app\models\art\Category;
use app\models\art\Events;
use app\models\art\Recoverpssw;
use app\models\art\Settings;
use app\models\art\Project_settings;
use app\models\art\Subscribers;



class SiteController extends Controller

{

    public $error_arr=[];
    public $json_arr;
    public $publish_project_info=[];
    public $layout = 'main';
    public $socket;
    
    
    public function actionGetava(){

            $avatarload=new Userinfo;

             $ava_get=$avatarload->getAvatar();
            if($ava_get=='')
             {
                 $rez='/images/nofoto.png';

             }else $rez=$ava_get;
             
             return $rez;

}

    protected function getuserid($nickname){
        
         $user=new User;
            $user_id=$user->find_id_nickname($nickname);
            return $user_id;
    }
        
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['login','logout','regit'],
//                'rules' => [
//                    
//                    [
//                     'actions'=>['login','register'],
//                         'allow' => true,
//                        'roles' => ['?'],
//                        
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ]
//      
//        ];
//    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionGetslider(){
        
        $popular=new Project;
        $all_popular=$popular->get_popular_projects(4);

        $views= $all_popular['view'];
        $likes=$all_popular['like'];

        $count_likes=$all_popular['likes_count'];
        $count_views=$all_popular['views_count'];
  
        $count_view_arr=[];
        $count_like_arr=[];
      
        /*tmp arr likes*/
        foreach($count_likes as $key){
            $count_like_arr[$key['proj']]=$key['count'];
        }
         /*tmp arr views*/
        foreach( $count_views as $key){
            $count_view_arr[$key['proj']]=$key['count'];
        }
//         if(sizeof($views) >0){
//             
//            /*add count view*/
//           for($i=0;$i<sizeof($views);$i++){
//                $views[$i]['count_views']=$count_view_arr[$views[$i]['proj_id']];
//
//            }
//         }
//        if(sizeof($likes) >0){
//              /*add count likes*/
//            for($i=0;$i<sizeof($likes);$i++){
//                $likes[$i]['count_likes']=$count_like_arr[$likes[$i]['proj_id']];
//
//            }
//        }
//      
        $popular_arr['all']=array_merge($views,$likes);
      
        shuffle($popular_arr['all']);
       
        return sizeof($popular_arr['all'] >0)?$popular_arr['all']: '';
    }
    
    
     public function actionSubscribe()    {
         $refer=Yii::$app->request->referrer;
    $subscriber_nick= Yii::$app->request->post('subscriber_nick');
         $subscriber_id=$this->getuserid($subscriber_nick);
            $my_id=Yii::$app->user->getid();
            
         if(!Yii::$app->user->isGuest){
                $subscriber = new Subscribers;
                $rez=$subscriber->subscribe($my_id,$subscriber_id);
                if($rez){return 'Вы подписались на обновления '.$subscriber_nick;}else{
                    
                    return 'Вы уже подписаны на обновления '.$subscriber_nick;
                }
         
          }else{ $rez= 'Для того чтобы подписатьcя на обновления '.$subscriber_nick.' необходимо <a target="_blank" href="/login">авторизоваться</a>';}
          
         if($rez){
             
             return $rez;
         }
     }   
    public function actionIndex($nick=null)
    {      

       if(Yii::$app->user->identity && $nick){
       if ($nick && $nick !=Yii::$app->user->identity->nickname){
     
           return $this->render('forbidden_error',['message'=>'Доступ к данному разделу запрещен']);
         
       }
       } 
       
       if(!Yii::$app->user->identity && $nick){
           
           {return $this->render('forbidden_error',['message'=>'Для доступа к данному разделу необходимо авторизоваться']);}
       }
       
       
         return $this->render('index');
       
       
    }

    /*выводим каьегории*/
    
    public function actionShowcateg(){
   
        $txt='';
        $obj=new Category;
   
        $rez=$obj->getall_category();
            foreach($rez as $key){

                    $txt=$txt. '<div class=""><div class="categ_wrap">'.$key['name'].'</div></div>';
                 }

        return $txt;
    }
    
//        public function actionAddfriend(){
//           $friend_id= Yii::$app->request->post('friend_id');
//           $login= Yii::$app->request->post('login');
//           $ava= Yii::$app->request->post('ava');
//            $user_id=(string)Yii::$app->user->getid();
//           $sock=new socketClient(7777);
//           $rez=$sock->send_user_info($friend_id,$login,$ava,$user_id);
//           
//           if ($rez=='ok'){return 'ok';}
//       
//      
//    }
    

    public function actionLikeinsert(){
               $proj_id = Yii::$app->request->post('proj_id');
               $user_id = Yii::$app->user->getid();
           
               if($user_id != null){         $likes = new Likes;
               $check_likes=$likes->find_user_likes($proj_id,$user_id);
              
               $rez = $likes->get_likes($proj_id);}
               else{ $rez='mustreg';}
      
               return $rez;
        }
    
    protected function getlikes(){
        $likes = new Likes;
        $likesCount = $likes->get_all_likes();
        return  $likesCount;
    }
            
    protected function getIP(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
        

    public function actionGetp($n=null,$b=null,$offset=0){
        $status='';
        $user_var=" AND p.publish=1 )ORDER BY p.id DESC LIMIT 10 OFFSET ".$offset;

    
if(Yii::$app->user->identity && !$n){
 $user_id=Yii::$app->user->identity->id;  
           
            $user_var =" OR
(
	(
		 (
			 (u.id IN(SELECT fr.user_id FROM friends fr WHERE 
			fr.friend_id='".$user_id."' AND fr.submit_friend=1)) 
					OR 
			( u.id IN(SELECT fr.friend_id FROM friends fr WHERE fr.user_id='".$user_id."' AND fr.submit_friend=1))
		)

	 AND (s.project_rule=2 OR (s.project_rule=0 and ps.show_rule=1))
	 
	 
	 )  
	 
	OR 
	  
		(
		   (s.project_rule=3  AND
			u.id IN( SELECT fr_list.user_id FROM setting_friendlist_showproject fr_list 
			WHERE fr_list.friend_id='".$user_id."')
			)

				OR 
			(s.project_rule=0 AND ps.show_rule=2 AND
			 p.id IN( SELECT fr_list.proj_id FROM settings_project_friendlist_showproject fr_list 
			  WHERE p.id=fr_list.proj_id AND fr_list.friend_id='".$user_id."') 
			)
		)
		
	) OR u.id='".$user_id."')AND p.publish=1 ORDER BY p.id DESC LIMIT 10 OFFSET ".$offset;



                
        }

        if(Yii::$app->request->get('loc')){$loc=Yii::$app->request->get('loc');}
        if(Yii::$app->request->get('cat')){$cat=Yii::$app->request->get('cat');}
        if(Yii::$app->request->get('date')){$date=Yii::$app->request->get('date');}
       
        if($n && !$b){
             $user_id=Yii::$app->user->identity->id;  
            $status='me';
//            $user_id=$this->getuserid($n);
            $user_var=" AND u.id='".$user_id."' OR (s.project_rule IN(0,1,2,3) AND ps.show_rule IN(0,1,2,3) AND u.id='".$user_id."') ) ORDER BY p.id DESC LIMIT 10 OFFSET ".$offset  ; 
            
        }
        
        else if($b && $n){
            if(Yii::$app->user->identity){
                $owner_id=$this->getuserid($n);
                $user_id=Yii::$app->user->identity->id;  
//                if($owner_id===$user_id){
//                          $user_var=" AND u.id='".$user_id."' OR (s.project_rule IN(0,1,2,3) AND ps.show_rule IN(0,1,2,3) AND u.id='".$user_id."') ) ORDER BY p.id DESC LIMIT 10 OFFSET ".$offset  ; 
//                          $status='me';
//                }else{
     
                $user_var =" AND u.id='".$owner_id."'  OR
(
	(
		 (
			 (u.id IN(SELECT fr.user_id FROM friends fr WHERE 
			fr.friend_id='".$user_id."' AND fr.submit_friend=1)) 
					OR 
			( u.id IN(SELECT fr.friend_id FROM friends fr WHERE fr.user_id='".$user_id."' AND fr.submit_friend=1))
		)

	 AND (s.project_rule=2 OR (s.project_rule=0 and ps.show_rule=1))
	 
	 
	 )  
	 
	OR 
	  
		(
		   (s.project_rule=3  AND
			u.id IN( SELECT fr_list.user_id FROM setting_friendlist_showproject fr_list 
			WHERE fr_list.friend_id='".$user_id."')
			)

				OR 
			(s.project_rule=0 AND ps.show_rule=2 AND
			 p.id IN( SELECT fr_list.proj_id FROM settings_project_friendlist_showproject fr_list 
			  WHERE p.id=fr_list.proj_id AND fr_list.friend_id='".$user_id."') 
			)
		)
		
	) AND u.id='".$owner_id."') AND p.publish=1  ";
                






                              $status='personalpage';
                

    }
        }else{$user_id='';}

        
            //if req is not for onluy user
        
        $userinfo=new Userinfo;

        $proj= new Project;
        /*views*/
        $allviews= $proj->getallview();
        $view_arr=[];
                $likes_arr=[];
        if(sizeof( $allviews)  >0 ){
            foreach( $allviews as $key){
                $view_arr[$key['proj_id']] = $key['count'];
            }
        }
        /*likes*/

        $likes=$this->getlikes();
       
        if (sizeof($likes >0)){
            foreach ($likes as $key){$likes_arr[$key['proj_id']]=$key['count'];}
        
        }
        $main_query=Yii::$app->request->get('p');
        switch($main_query){
            
            /****all_filter_items*****/
            
 /******loc cat  sort date*****/
            
            
            case 'cat_loc_month_view'://    
 
               $rez=$proj->get_all_proj_by_month_and_cat_and_loc_and_view($cat,$loc,$user_var); 
               break;
           
            case 'cat_loc_month_like'://    
      
               $rez=$proj->get_all_proj_by_month_and_cat_and_loc_and_likes($cat,$loc,$user_var); 
             break;
         
            case 'cat_loc_day_view'://    

               $rez=$proj->get_all_proj_by_day_and_cat_and_loc_and_views($cat,$loc,$user_var); 
             break;
         
                case 'cat_loc_day_like'://    
   
               $rez=$proj->get_all_proj_by_day_and_cat_and_loc_and_likes($cat,$loc,$user_var); 
             break;
         
                     case 'cat_loc_week_view'://    

               $rez=$proj->get_all_proj_by_week_and_cat_and_loc_and_views($cat,$loc,$user_var); 
             break;
         
                case 'cat_loc_week_like'://    
   
               $rez=$proj->get_all_proj_by_week_and_cat_and_loc_and_likes($cat,$loc,$user_var); 
             break;
           
               case 'cat_loc_calendar_view'://    
            
               $rez=$proj->get_all_proj_by_calendar_and_cat_and_loc_and_views($cat,$loc,$date,$user_var); 
             break;
         
                case 'cat_loc_calendar_like'://    
   
               $rez=$proj->get_all_proj_by_calendar_and_cat_and_loc_and_likes($cat,$loc,$date,$user_var); 
             break;
         
      
            
            
       /****** cat  sort date*****/
            
            
            case 'cat_month_view'://    
 
               $rez=$proj->get_all_proj_by_month_and_cat_and_view($cat,$user_var); 
               break;
           
            case 'cat_month_like'://    
      
               $rez=$proj->get_all_proj_by_month_and_cat_and_likes($cat,$user_var); 
                break;
            
             case 'cat_month_active'://    
      
               $rez=$proj->get_all_proj_by_month_and_cat_and_active($cat,$user_var); 
                break;
            case 'cat_month_no_active'://    
      
               $rez=$proj->get_all_proj_by_month_and_cat_and_noactive($cat,$user_var); 
                break;
            case 'cat_day_view'://    

               $rez=$proj->get_all_proj_by_day_and_cat_and_views($cat,$user_var); 
                break;
            
            case 'cat_day_active'://    

               $rez=$proj->get_all_proj_by_day_and_cat_and_active($cat,$user_var); 
                break;
            case 'cat_day_no_active'://    

               $rez=$proj->get_all_proj_by_day_and_cat_and_noactive($cat,$user_var); 
                break;
         
         
            case 'cat_day_like'://    
   
               $rez=$proj->get_all_proj_by_day_and_cat_and_likes($cat,$user_var); 
                break;
         
             case 'cat_week_view'://    

               $rez=$proj->get_all_proj_by_week_and_cat_and_views($cat,$user_var); 
                break;
         
            case 'cat_week_like'://    
   
               $rez=$proj->get_all_proj_by_week_and_cat_and_likes($cat,$user_var); 
                break;
            
              case 'cat_week_active'://    
   
               $rez=$proj->get_all_proj_by_week_and_cat_and_active($cat,$user_var); 
                break;
            
               case 'cat_week_no_active'://    
   
               $rez=$proj->get_all_proj_by_week_and_cat_and_noactive($cat,$user_var); 
                break;
           
            case 'cat_calendar_view'://    
            
               $rez=$proj->get_all_proj_by_calendar_and_cat_and_views($cat,$date,$user_var); 
                break;
         
            case 'cat_calendar_like'://    
   
               $rez=$proj->get_all_proj_by_calendar_and_cat_and_likes($cat,$date,$user_var); 
               break;
           
            case 'cat_calendar_active'://    
   
               $rez=$proj->get_all_proj_by_calendar_and_cat_and_active($cat,$date,$user_var); 
               break;
           
           case 'cat_calendar_no_active'://    
   
               $rez=$proj->get_all_proj_by_calendar_and_cat_and_noactive($cat,$date,$user_var); 
               break;
         
         
         
             
            case 'cat_loc_calendar':


                $rez=$proj->get_all_proj_by_calendar_and_cat_and_loc($cat,$date,$loc,$user_var); 
               break;
            case 'cat_loc_day':
     
                $rez=$proj->get_all_proj_by_day_and_cat_and_loc($cat,$loc,$user_var); 
               break;
            case 'cat_loc_week':

                $rez=$proj->get_all_proj_by_week_and_cat_and_loc($cat,$loc,$user_var);
                break;
            case 'cat_loc_month':
 
                $rez=$proj->get_all_proj_by_month_and_cat_and_loc($cat,$loc,$user_var);
                break;
       
       /****loc sort date**/
            
            //view//
        
            case 'loc_month_view':
                $rez=$proj->get_all_proj_by_views_and_alldate_and_loc('month',$user_var,$loc);
                 break;
             
            case 'loc_day_view':
                $rez=$proj->get_all_proj_by_views_and_alldate_and_loc('day',$user_var,$loc);
                 break;

             
            case 'loc_week_view':
                $rez=$proj->get_all_proj_by_views_and_alldate_and_loc('week',$user_var,$loc);
                 break;

             
            case 'loc_calendar_view':
                $rez=$proj->get_all_proj_by_views_and_alldate_and_loc($date,$user_var,$loc);
                 break;
             
             //like//
             
            case 'loc_month_like':
                $rez=$proj->get_all_proj_by_likes_and_alldate_and_loc('month',$user_var,$loc);
                 break;
             
            case 'loc_day_like':
                $rez=$proj->get_all_proj_by_likes_and_alldate_and_loc('day',$user_var,$loc);
                 break;

             
            case 'loc_week_like':
                $rez=$proj->get_all_proj_by_likes_and_alldate_and_loc('week',$user_var,$loc);
                 break;

             
            case 'loc_calendar_like':
                $rez=$proj->get_all_proj_by_likes_and_alldate_and_loc($date,$user_var,$loc);
                 break;


            /*****location and date*****/
            case 'loc_month'://month&loc
             
               $rez=$proj->get_all_proj_by_month_and_loc($loc,$user_var); 
               break;
            case 'loc_week'://week&loc
                 
                $rez=$proj->get_all_proj_by_week_and_loc($loc,$user_var); 
                break;
            case 'loc_day'://day&loc
           
                $rez=$proj->get_all_proj_by_day_and_loc($loc,$user_var); 
                break;
            case 'loc_calendar'://calend_data&loc
    
          
                $rez=$proj->get_all_proj_by_calendar_and_loc($loc,$date,$user_var);
                break;
            
            /*****category and loc******/
            case 'loc_cat': 

            $rez=$proj->get_all_publish_proj_by_cat_and_loc($cat,$loc,$user_var);
            break;
        
                    /*****category and date******/
            case 'cat_day': //day
  
           
            $rez=$proj->get_all_proj_by_thisday_and_cat($cat,$user_var);
            break;
            case 'cat_week': //week
   
            $rez=$proj->get_all_proj_by_week_and_cat($cat,$user_var);
            break;
        
            case 'cat_month': //month
     
            $rez=$proj->get_all_proj_by_month_and_cat($cat,$user_var);
            break;
        
            case 'cat_calendar': //calendar

       
  
            $rez=$proj->get_all_proj_by_calendar_and_cat($cat,$date,$user_var);
            break;
        
        
            /*****cat and date******/
            case 'cat_month'://month&loc
          
               $rez=$proj->get_all_proj_by_month_and_loc($loc,$user_var); 
               break;
            case 'loc_week'://week&loc
         
                $rez=$proj->get_all_proj_by_week_and_loc($loc,$user_var); 
                break;
            case 'loc_day'://day&loc

                $rez=$proj->get_all_proj_by_day_and_loc($loc,$user_var); 
                break;
            case 'cat_calendar'://calend_data&collection

 
                $rez=$proj->get_all_proj_by_calendar_and_cat($cat,$date); 
                break;
        
            /*****location*****/
            case 'loc':
               
                $rez=$proj->get_all_proj_by_location($loc,$user_var);
                 break;
             /*****collection*****/
            case 'cat':
             
                $rez=$proj->get_all_publish_proj_by_cat($cat,$user_var);
                break;
            
            /*****date*****/
            
            case 'date_day'://current date//
                $rez=$proj->get_all_proj_by_curdate($user_var);
                break;
            
            case 'date_week'://week//
                $rez=$proj->get_all_proj_by_week($user_var);
                break;
            
            case 'date_month'://month//
                $rez=$proj->get_all_proj_by_month($user_var);
                break;
            
            case 'date_calendar'://calendar//
        
                $rez=$proj->get_all_proj_by_date($date,$user_var);
                break;
            
            /*****sort*****/
            case 'sort_like'://likes//
                $rez=$proj->get_all_proj_by_likes($user_var); 
                break;
            
            case 'sort_view'://viewed//
                $rez=$proj->get_all_proj_by_viewed($user_var);
                break;
            case 'sort_active'://viewed//
                $rez=$proj->get_all_proj_by_active($user_var);
                break;
            case 'sort_no_active'://viewed//
                $rez=$proj->get_all_proj_by_noactive($user_var);
                break;
            
            
            case 'cat_like'://cat_sort_like
                

                $rez=$proj->get_all_proj_by_likes_and_cat($cat,$user_var);
                break;
            case 'cat_view'://cat_sort_view
                

                $rez=$proj->get_all_proj_by_viwed_and_cat($cat,$user_var);
                break;
            case 'loc_cat_view'://loc_cat_sort_view

                $rez=$proj->get_all_proj_by_viwed_and_cat_loc($cat,$loc,$user_var);
                break;
            case 'loc_cat_like'://loc_cat_sort_view

                $rez=$proj->get_all_proj_by_likes_and_cat_loc($cat,$loc,$user_var);
                break;
            
            //****loc sort**/
            
            case 'loc_like':

                $rez=$proj->get_all_proj_by_likes_and_loc($loc,$user_var);
                break;
            
            case 'loc_view':

                $rez=$proj->get_all_proj_by_viwed_and_loc($loc,$user_var);
                break;
            
            
            
            
            //*****sort_date
             
            case 'month_like':

                $rez=$proj->get_all_proj_by_likes_and_alldate('month',$user_var);
                break;
            
            case 'day_like':

                $rez=$proj->get_all_proj_by_likes_and_alldate('day',$user_var);
                break;
             
            case 'week_like':
            
                $rez=$proj->get_all_proj_by_likes_and_alldate('week',$user_var);
                break;
            case 'calendar_like':
            
                $rez=$proj->get_all_proj_by_likes_and_alldate('calendar',$user_var);
                break;
            
            case 'month_view':

                $rez=$proj->get_all_proj_by_views_and_alldate('month',$user_var);
                break;
            
            case 'day_view':

                $rez=$proj->get_all_proj_by_views_and_alldate('day',$user_var);
                break;
             
            case 'week_view':
            
                $rez=$proj->get_all_proj_by_views_and_alldate('week',$user_var);
                break;
            case 'calendar_view':
            
                $rez=$proj->get_all_proj_by_views_and_alldate('calendar',$user_var);
                break;
            
            
            case 'loc_view':

                $rez=$proj->get_all_proj_by_viwed_and_loc($loc,$user_var);
                break;
            
            default:
                $rez=$proj->get_all_publish_proj($user_var);
                
                
        }


        foreach($rez as $key){  
         
            $key['id']=(string)$key['id'];
             $this->publish_project_info['project_all']["project".$key['id']]= $key;
             
                          $this->publish_project_info['project_all']["project".$key['id']]['userinfo']= $userinfo->getuserinfo($key['user_id']);

              $categ=$this->get_cur_proj_cat($key['id']);
              
            
                $this->publish_project_info["status"]=$status;
               $this->publish_project_info['project_all']["project".$key['id']]['category']=  $categ;
               $this->publish_project_info['project_all']["project".$key['id']]['likes']=array_key_exists($key['id'],$likes_arr) ? $likes_arr[$key['id']]:'0';
               $this->publish_project_info['project_all']["project".$key['id']]['views']=array_key_exists($key['id'],$view_arr) ? $view_arr[$key['id']]:'0';

        }
        if($offset==0){
             $this->publish_project_info['nooffset']=0;
        }
    
       return json_encode($this->publish_project_info);
             //очищяем массив//
       
   $this->publish_project_info=array();
          
          
    }
   
     public function actionGetevents(){
       
        $user_id=Yii::$app->user->getid();
        $event=new Events();
        $allevents=$event->show_count_events($user_id);
        
        if ($allevents  >0){   return "+".$allevents;}
     

    }    
    public function actionTesty(){
        
        $this->render('test');
    }
      public function actionFupload(){

            /*временая зона*/

                date_default_timezone_set('Europe/Moscow');
             $image_type=substr($_FILES['ufile']['name'],-3);

              $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$image_type;


                   $full_filename="$filedir$file_date_name";

              $image=$_FILES['ufile']['tmp_name']; 
              $uploadfile=  move_uploaded_file($image,$full_filename);
              if( $uploadfile){

                   return $full_filename;

              }
    }
      


        public function actionGetsocketdata(){
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');

       
            header("Content-Encoding: none; ");
            header("Cache-Control: no-cache");
            header("Access-Control-Allow-Origin: *");

               ob_start();
                    echo "data: ji\n\n";
               ob_flush();
                flush();
               

            }
  
    public function actionChekdata()
    {
            $jsonarr=[];
            $passw= Yii::$app->request->post('passw');
            $login=Yii::$app->request->post('name');
            
               $us=new User;
               $us->find_id_main($login);
                 
                 if( $us->check_login($login)==1 ){
                   $passw_hash= $us->getPassword($login);
                          
                    if(password_verify($passw,$passw_hash)){
        
                        $rez= User::findIdentity($us->find_id_main($login));
                        Yii::$app->user->login($rez);
                        $nickname=Yii::$app->user->identity->nickname;
                        $us_id=Yii::$app->user->getid();

          
                 return json_encode(['auth'=>'ok','nick'=>$nickname]);
                    }else{
                       $this->error_arr['auth_err']['passw'] ='Введен неверный пароль.';
                  }
                }else{
                     $this->error_arr['auth_err']['login']='Введен неверный логин.';
                }
                
                if (sizeof($this->error_arr)>0){
                   
                  return json_encode($this->error_arr);
                  $this->error_arr=array();

                }
    }

    
    


    public function actionLogin()
    {
        
        return $this->renderPartial('login');

    }

    public function actionLogout()
    {
        $user_id=Yii::$app->user->getid();
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

      public function actionRegister()

        {
                       return $this->renderPartial('register');

        }
        
              public function actionPasswrecover()
              
              
        {
                       return $this->renderPartial('passwrecover');

        }
    public function actionRecoverpassw(){
        $email=Yii::$app->request->post('email');
        $recpssw= new Recoverpssw;
        $userid=$recpssw->getuserid($email);
        
        if(!$userid){return  json_encode('введеный email не найден');}else{
        $usernick=$recpssw->getusernick($email);
        $date= $date_user_zone = new \DateTime();
        $user_date= $date_user_zone->format('Y-m-d h-m');
        $stringtohash=$email.$userid.$user_date;
        $hash=password_hash($stringtohash, PASSWORD_DEFAULT);
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'From: admin@creationlive.net' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        mail($email, "My Subject", "Line 1\nLine 2\n Line 3",$headers); 
        
//        $sendmail=Yii::$app->mailer->compose();
//         $sendmail->setFrom('admin@creationlive.net')
//        ->setTo($email)
//        ->setSubject('Creationlive.net::Восстановление пароля')
//        ->setHtmlBody('<b>Добрый день '. $usernick.'!</b>')
//        ->send();
//        if( $sendmail){ $recpssw->setdata($userid,$hash,$user_date);return 'ok';}
//       
       }
            
    }
       public function actionCheckregister()
       {
             $us=new User;
             
             if(Yii::$app->request->post('check_login')){ 
                 $login=Yii::$app->request->post('check_login');
                 
                 if( !$us->check_login($login) ==0){
                    $this->error_arr='Введеный логин уже используется';
                   
             }}
             
            if(Yii::$app->request->post('check_nickname')){ 

                $nickname=Yii::$app->request->post('check_nickname');

                if( !$us->check_nickname($nickname) ==0){
                   $this->error_arr='Введенное псевдоимя уже используется';

                }

             }
             
            if(Yii::$app->request->post('check_email')){ 

                $email=Yii::$app->request->post('check_email');

                if(!$us->check_email($email) ==0){
                   $this->error_arr='Введенное email занят';

                }

             }

       
            if(Yii::$app->request->post('regform')) {
                $data=Yii::$app->request->post('regform');

                if( $us->check_login($data['login']) ==0){
                    
                    $data['passw']=password_hash($data['passw'], PASSWORD_DEFAULT);
                  
                    $us->attributes=$data;
                    $us->save();
                    $rez= User::findIdentity($us->find_id_main($data['login']));
                    Yii::$app->user->login($rez);
                    $us_id=Yii::$app->user->getid();
               
        
                    $userinfo=new Userinfo;
                    $settings= new Settings;
                    $userinfo->setuser_id_in_info();
                    $settings->set_user_id($us_id);

                           
                   
                    return $this->renderAjax('goodreg',['login'=>$data['login']]) ;
                }               
               }
               $err=$this->error_arr;
               
              if(count($err) > 0){  return  json_encode($this->error_arr);}
}

          public function get_cur_proj_cat($proj_id){
                $categ_project=  new Categ_projects;
            
               $category_rez=$categ_project->get_cat($proj_id);
               return      $category_rez;
        }
        


    
      public function actionCheckstatus(){
          
          $id=Yii::$app->request->get('user_id');
          $user_status = new User_session;
          $rez=$user_status ->check_status($id);
          return $rez>0 ? 'online' :'not_online';
          
          
          
          
      }
        

}
    
      