<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\art\Map_coords;
use app\models\art\Interests;
use app\models\art\Project;
use app\models\art\Userinfo;
use app\models\art\Friends;
use app\models\art\Events;
use app\models\art\Message;
use app\models\art\Category;
use app\models\art\User;
use app\models\art\Likes;
use app\models\art\Comments;
use app\models\art\Categ_projects;
use app\models\art\News;
use app\models\art\Bookmarks;
use app\models\art\Subscribers;
use app\components\Test;
use app\components\Websocket;
use app\components\socketClient;

class PerspageController extends Controller{
    
    public $arr=[];
    public $layout = 'personalpage';    
    public $user_projects_arr=[];


    public function actionIndex(){
 
        return $this->render('index');
   }
   
    public function actionBookmarks(){
        $user_id=Yii::$app->user->getid();
        $bookmarks=new Bookmarks;
        $likes=new Likes;
        $subscribers=new Subscribers;
        $bookmark=$bookmarks->getbookmarksfromuser($user_id);

        $like=$likes->getuserlikes($user_id);
        $subscriber=$subscribers->getuser_subscribers($user_id);

        return $this->render('bookmarks',['bookmarks'=>$bookmark,'likes'=>$like,'subscribers'=>$subscriber]);
   }
   
    public function actionStatistic(){

        return $this->render('statistic');
    }

    protected function getuserid($nickname){
        
         $user=new User;
            $user_id=$user->find_id_nickname($nickname);
            return $user_id;
    }


    public function actionMessagelist(){
        $user_id=Yii::$app->user->getid();
        $request_nick=Yii::$app->request->get('nick');
        $request_user_id=$this->getuserid($request_nick);
            if ($user_id === $request_user_id ){
                $message= new Message;
                $rez=$message->getmessagelist($user_id);
        
                return $this->render('messages',['messagelist'=>$rez]);
            }else{
                return $this->render('error',['error'=>'Доступ к данному разделу закрыт']);


            }

    }

       public function get_cur_proj_cat($proj_id){
                $categ_project=  new Categ_projects;
            
                $category_rez=$categ_project->get_cat($proj_id);
                return      $category_rez;
        }
        
        
    public function actionChartsdata(){
                $usercateg=[];
                $incremet=0;
            $colors=[ '#C9BAA1', '#D38C7C', '#7FBBC4','#9C9CBA','#CCA9C6','#998B96'];
            $color_length=sizeof($colors);
            $user_id=Yii::$app->user->getid();
            $obj=new Category;
            $count_proj=$obj->count_proj($user_id);

             $count_categ=$obj->view_categ_charts($user_id);

                foreach( $count_categ['user_categ'] as $key){
                   $usercateg[$key['name']]=$key['count'];

                }


            /*заполняем массив данными по катеорям*/
                    foreach($count_categ['all_categ'] as $key){     
                        $count= array_key_exists($key['name'],$usercateg) ? $usercateg[$key['name']]: 0;
                             $this->arr[]['c']= 

                                    [
                                        ["v"=>$key['name'],"f"=>null],
                                        ["v"=>$count,"f"=>null],
                                        ["v"=>   "fill-color:". $colors[$incremet].";stroke-color: grey; stroke-width: 1","f"=>null]
                                     ];

                              $incremet == $color_length-1 ? $incremet = 0 : $incremet++;;
                        } 

                $json_chart_data['colls'] = [
                     "cols"=> [
                             ["id"=>"","label"=>"Проектов ","pattern"=>"","type"=>"string"],
                              ["id"=>"","label"=>"Проектов ","pattern"=>"","type"=>"number"],
                              ["type"=>"string","role"=>"style"]
                    ],

                    "rows"=>  $this->arr
                ];


                $json_chart_data['pie'] = [
                    "cols"=> [
                          ["id"=>"","label"=>"Topping","pattern"=>"","type"=>"string"],
                          ["id"=>"","label"=>"Проекты","pattern"=>"","type"=>"number"]
                        ],
                    "rows"=>[
                        ["c"=>[["v"=>null,"f"=>'Активные'],["v"=>$count_proj['active'],"f"=>$count_proj['active']],]],
                        ["c"=>[["v"=>null,"f"=>'Неактивные'],["v"=>$count_proj['noactive'],"f"=>$count_proj['noactive']],]],

                    ]
               ];


                $json_chart_data['max_count_categ']= $count_categ['max_count']['count'];


                return  json_encode($json_chart_data) ;



    }
    public function actionGetproj($proj){


           $project= new Project;
                    $rez= $project->get_project_files($proj);
                    return $this->render('project',['project'=>$rez]);


    }

    public function actionGetuserpage($id){

       return $this->render('index');

    }

    public function actionSetmessage(){
        if(Yii::$app->request->post('setmessage')){
            $time_zone=new Map_coords;
            /*my time_zone*/
            $timezone_user=$time_zone->get_time_zone(Yii::$app->user->getid());
            if (sizeof( $timezone_user) == 0){  $timezone_user = 'Europe/Moscow';}
            $date_user_zone = new \DateTime('now', new \DateTimeZone($timezone_user));
            $user_date= $date_user_zone ->format('Y-m-d H:i:s');

            $user_id=Yii::$app->user->getid();
            $friend_id=Yii::$app->request->post('friend_id');
            $text_message=Yii::$app->request->post('setmessage');
            $message_id=Yii::$app->request->post('message_id');

            /*friend_time_zone*/

            $timezone_friend=$time_zone->get_time_zone($friend_id);
                if (sizeof(  $timezone_friend) == 0){   $timezone_friend = 'Europe/Moscow';}
            $date_friend_zone = new \DateTime('now', new \DateTimeZone($timezone_friend));
            $friend_date= $date_friend_zone->format('Y-m-d H:i:s');
    //        return $friend_date;

            /*send mess*/
            $setmessage= new Message();
            $setmessage->insertMessage($user_id,$friend_id,$text_message,$user_date,$friend_date,$message_id);

            $all_date=$user_date."#".$friend_date;
            /*return user_date*/

            return (string)$all_date;
        }

    }

    public function actionGetmessage(){
            $user_id=Yii::$app->user->getid();
            $friend_nickname=Yii::$app->request->get('friend_nick');
            $friend_id=$this->getuserid($friend_nickname);
            $message=new Message();
            $rez=$message->getmessages($user_id,$friend_id);

            $jsonarr=[];
            $messarr=[];



            foreach ($rez['mes'] as $key){

                $messarr[$key['id']]['date']=$key['date'];
                $messarr[$key['id']]['text']=$key['text'];
                $messarr[$key['id']]['ava']=$key['ava'];
                 $messarr[$key['id']]['nickname']=$key['nickname'];

            }
             $jsonarr['message_info']['friend_nickname']=  $friend_nickname;
             $jsonarr['message_info']['friend_id']=  $friend_id;
             $jsonarr['message_info']['messages']=  $messarr;
          return  Yii::$app->request->isAjax ? json_encode($jsonarr): $this->render('showmessage',['messarr'=>$jsonarr['message_info']]);



    }



   static function cmp($a, $b) 
{
   
    if ($a['date'] == $b['date']) {
        return 0;
    }
    return ($a['date'] < $b['date']) ? -1 : 1;

}

    
    public function actionShow_noviewed_events(){
        $jsonarr=[];
        $user_id=Yii::$app->user->getid();
        $event=new Events();
        $message=$event->show_all_events($user_id);

    //      foreach($message as $key){
    //          $key['date'];
    //          $jsonarr[]['data']=$key['date'];
    //      }


    /*sort array to date*/
    usort($message, array($this,"cmp"));
    $json_arr['allevents']=$message;


    return json_encode($json_arr) ;
    }




    public function actionAddfriend(){


        $user_id=Yii::$app->user->getid();
        $friend_id=$this->getuserid(Yii::$app->request->post('friend_nick'));
      

        /*get time_zone*/
        $time_zone=new Map_coords;


                /*friend_time_zone*/
        $timezone_user=$time_zone->get_time_zone($friend_id);
         if (sizeof( $timezone_user) == 0){  $timezone_user = 'Europe/Moscow';}
        $date_friend_zone = new \DateTime('now', new \DateTimeZone($timezone_user));
            /*friend_date*/
        $friend_date=$date_friend_zone ->format('Y-m-d H:i:s');


        $friends=new Friends();

        $addfriend=$friends->setfriend($user_id,$friend_id,$friend_date);




        if(!$addfriend ){return '0';}else{

        }


    }

    public function actionFriendslist(){
                $arr=[];
                $user_id=Yii::$app->user->getid();
                $friends=new Friends();
                $friends=$friends->getfriends($user_id);
                return json_encode( $friends);
    }
    public function actionFriends(){


            if(Yii::$app->request->isGet){
                     return $this->render('friendlist');

            }



    }
    public function actionNews(){
            $user_id=Yii::$app->user->getid();
            $news=new News();
            $project=new Project();
            $comments=new Comments();
            $likes=new Likes();
            $newsarr=$news->getnews($user_id);
            $rez['news']=[];

        forEach($newsarr as $key){

            if ($key['project_id'] != null){

                 $project_rez=$project->getproject_for_news($key['project_id']);
                 $project_rez['text']='добавил новый проект';
                  $project_rez['type_bottsrap_class']="glyphicon glyphicon-plus";
                $rez['news'][]=$project_rez;


            }
            if ($key['comment_id'] != null){$rez['news'][]=$key['comment_id'];}
            if ($key['likes_id'] != null){$rez['news'][]=$key['likes_id'];}
        }

        return $this->render('news',["news" => $rez['news']]);

    }

    /*exprt method fron site controller*/


         public function actionGetevents(){
            $user_id=Yii::$app->user->getid();
            $event=new Events();
            $allevents=$event->show_count_events($user_id);
            return $allevents;

        }    

//public function actionLoadfoto(){
//          $filedir= 'uploads/'.Yii::$app->user->getid().'/temp/';
//      $type=$_FILES['ufile']['type'];
//      return(print_r($_FILES['ufile']));
//      if($type=='image/png' || $type=='image/jpeg' ){
//      
//            $image_type=substr($_FILES['ufile']['name'],-3);
//
//             $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$image_type;
//
//
//                  $full_filename="$filedir$file_date_name";
//
//             $image=$_FILES['ufile']['tmp_name']; 
//             $uploadfile=  move_uploaded_file($image,$full_filename);
//             if($uploadfile){
//            $filetype['foto']=$full_filename;
//                return  json_encode($filetype);
//             }
//             
//        }else{ 
//                 
//                 echo 'выбран неверный тип файла';
//                 
//             }
//    
//}

    public function actionLoadcropfile(){

        $postfoto=Yii::$app->request->post('crop_foto');
        $oldfoto=Yii::$app->request->post('path');
        $foto_arr=explode(';',$postfoto);
        $foto_data=explode(',',$foto_arr[1]);
        $foto_type_arr=explode('/',$foto_arr[0]);
         $userid=Yii::$app->user->getid();
        $foto_data=$foto_data[1];

        $foto_type= $foto_type_arr[1];

        

          if($foto_type=='png' || $foto_type=='jpeg' ){
            
          $webroot="/uploads/files/";
            $serverroot= Yii::getAlias('@webroot').$webroot;
            //delete old foto
            $del_oldfoto_path=Yii::getAlias('@webroot').$oldfoto;
            if(file_exists ( $del_oldfoto_path)){
                   unlink($del_oldfoto_path);
            }
         
//            $iterator = new \FilesystemIterator( $serverroot);
//            foreach($iterator  as $file){
//    
//                unlink( $file);
//            }
//          
             

                $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$foto_type;
                $fullimgname="$serverroot$file_date_name";

                $data = base64_decode($foto_data);
                $rez=file_put_contents($fullimgname, $data );

                /*запись в базу данных*/
                $websrc="$webroot$file_date_name";
                $avatarload=new Userinfo;

                $ava_set=$avatarload->load_ava( $websrc);

                    /*вывод пути к файлу в js*/
                    return  $websrc;

            }else{ 

                     echo 'выбран неверный тип файла';

                 }


    }


    public function actionCountmess(){
           $user_id=Yii::$app->user->getid();
        $rez= new Events;
        $count=$rez-> no_viewed_message($user_id);
        return $count;
    }

    public function actionCountfriend(){
           $user_id=Yii::$app->user->getid();
        $rez= new Events;
        $count=$rez->show_no_submitted_friends($user_id);
    return $count;

    }

    public function actionAcceptfriend(){
        $friend=new Friends();
        $user_id=Yii::$app->user->getid();
       $friend_id=$this->getuserid(Yii::$app->request->post('friend_nick'));
        $rez=$friend->acceptfriends($friend_id,$user_id);
        if($rez){    return 1;}else{return 0;}




    }


    public function actionFriendpage(){

        $friend_nick=Yii::$app->request->get('friend_nick');
        $user_id=$this->getuserid($friend_nick);
        $ownerid= Yii::$app->user->getid();
    if($user_id){ 
        $identeti= Yii::$app->user->getid()? 1 :0;
    //    $user_nick=Yii::$app->request->get('user_nick');

         $user_var=" AND p.user_id='".$user_id."')" ;
        $userinfo=new Userinfo;
        $userproject=new Project;
        $view_arr=[];
        $likes_arr=[];
        $likes = new Likes;
        $likesCount = $likes->get_all_likes();
        

            if (sizeof($likesCount >0)){
                foreach ($likesCount as $key){
                    $likes_arr[$key['proj_id']]=$key['count'];
                 }
            }
            
        $rez=$userproject->get_all_publish_proj($user_var);


        $allviews= $userproject->getallview();
        $view_arr=[];
  
        if(sizeof( $allviews)  >0 ){
                foreach( $allviews as $key){
                    $view_arr[$key['proj_id']] = $key['count'];
                }
         }

        foreach( $rez as $key){  
                $key['id']=(string)$key['id'];

                $this->user_projects_arr['project_all']["project".$key['id']]= $key;
                $categ=$this->get_cur_proj_cat($key['id']);
                $this->user_projects_arr['project_all']["project".$key['id']]['category']=  $categ;
                $this->user_projects_arr['project_all']["project".$key['id']]['likes']=array_key_exists($key['id'],$likes_arr) ? $likes_arr[$key['id']]:'0';
                $this->user_projects_arr['project_all']["project".$key['id']]['views']=array_key_exists($key['id'],$view_arr) ? $view_arr[$key['id']]:'0';

        }

                $this->user_projects_arr['interests']= $this->PersonalInteres($user_id);
                 $this->user_projects_arr['location']=$this->PersonalLocation($user_id);


        $this->user_projects_arr['userinfo']=$userinfo->getuserinfo($user_id);


            if($identeti){
                $this->user_projects_arr['loginin']='ok';

            }
            $owner= $ownerid ==$user_id ? 1 :0;
            $friend_status='';
            if(!$owner){
                $friends_model= new Friends;
                $friend_status=$friends_model->check_friend($ownerid,$user_id);
               
            }



        return $this->render('friendpage',['owneruserid'=> $user_id,'owner'=>$owner,'friend_status'=>$friend_status,'nick'=>$friend_nick,'projects'=> array_key_exists('project_all',$this->user_projects_arr) ?$this->user_projects_arr['project_all']:0,'userinfo_arr'=>$this->user_projects_arr['userinfo'],'interests'=>$this->user_projects_arr['interests'],'location'=>$this->user_projects_arr['location']]);






    }else{

         return $this->render('error',['error'=>'Личная страница '.$friend_nick. ' не найдена']);
    }





    }

                      //личная локация пользователя//
        protected function PersonalLocation($user_id){
                $location = new Map_Coords;
                $getlocation = $location->select_map_coord($user_id);
                return $getlocation;
        }
        protected function PersonalInteres($user_id){
            $interes=new Interests;
          $rez= $interes->get_interest($user_id);
          return $rez;
            
        }

        public function actionUserinfo(){
   
        }
   
        public function actionDelinteres(){
     
                    $interes_id=$_POST['interes_id'];
                    $interests= new Interests;
                    foreach($interes_id as $key){
                        $interests->del_interest($key);
                    };
        }
       protected function replacefiles($files_arr){
            $itemdir=$_SERVER['DOCUMENT_ROOT'].'uploads/files/temp/'.Yii::$app->user->getid().'/';
            $newdir=$_SERVER['DOCUMENT_ROOT'].'uploads/files/';
            $dir_elelms_arr=scandir($itemdir);
            $dir_elelms_arr_l=sizeof($dir_elelms_arr);
//            $equakelems=array_intersect($files_arr,$dir_elelms_arr);
//            $equal_elems_arr_l=sizeof($equakelems);
            $files_arr_l=sizeof($files_arr);

            for($i=0;$i<$files_arr_l;$i++){
                   $rez=copy($itemdir.$files_arr[$i],$newdir.$files_arr[$i]);
             }
             
             for($i=0;$i<$dir_elelms_arr_l;$i++){
                 if($dir_elelms_arr[$i] != '.' && $dir_elelms_arr[$i] != '..'){
                       unlink($itemdir.$dir_elelms_arr[$i]);
                 }

             }
            if(rmdir($itemdir)){
                return 1;
            };
    
        }
     public function actionSetuser_info(){
         
         $user_id=Yii::$app->user->getid();
            //временная зона//
            //проверка на пост запрос
            if(Yii::$app->request->post()){
                
                $user_coord=new Map_coords;
    
            
                 ///стависм  тайм зону//
          if(Yii::$app->request->post('user_time_zone')){
             $time_zone=Yii::$app->request->post('user_time_zone');
             $user_coord->set_time_zone( $user_id, $time_zone); 
           }	

                //запрос на googlemap//
                    if(Yii::$app->request->post('lat') && Yii::$app->request->post('lng')){  

                        $lng= Yii::$app->request->post('lng');   
                        $lat= Yii::$app->request->post('lat');  
                        $location=Yii::$app->request->post('location');  

                        if($user_coord->find_map_coord($user_id)>0){
                           $user_coord->update_map_coord($user_id,$lat,$lng, $location);

                        }else{

                            $user_coord->insert_map_coord($user_id,$lat,$lng, $location);

                        }
                    }
                    
                        //запрос на Интересы//
                     if(Yii::$app->request->post('interes')){
          
                        $user_interes_obj=new Interests;

                         $interes= Yii::$app->request->post('interes');  
            

                        $count=$user_interes_obj->count_interests($user_id);

                         foreach($interes as $key){ 
                            $user_interes_obj->set_interests($user_id,$key);

                        };
                      
//                        //вывод обновленной инфформации//
//            
                        $interes=$user_interes_obj->get_interest(Yii::$app->user->getid());
                        return(json_encode($interes));
                    }
                    
                    elseif(Yii::$app->request->post('h')){
                  
                        $user_interes_obj=new Interests;
                        $interes=$user_interes_obj->get_interest();
                        return(json_encode($interes));
                    }
                    
                    
                if(Yii::$app->request->post('back')){
                    $filearr=Yii::$app->request->post('filearr');
                    $newback=Yii::$app->request->post('back');
                    $oldback=Yii::$app->request->post('oldback');
                    //repalce from tmpdir
                   $is_replace=$this->replacefiles($filearr);
                   if($is_replace){
                        $userinfo=new Userinfo;
                        $backset=$userinfo->setbackground($newback);
                   }
           

                 }
                 
                 
                if(Yii::$app->request->post('resetfon')){
                    $oldback=Yii::$app->request->post('oldback');
                    $userinfo=new Userinfo;
                    $backset=$userinfo->setbackground(NULL);
                    
                    $del_file_path=Yii::getAlias('@webroot').$oldback;
         
                    if(file_exists ( $del_file_path)){
                           unlink($del_file_path);
                    }

                 }
                    
                                    //запрос на ФИО или Саит или Работа и т.д//
                 if(Yii::$app->request->post('userinput')){
                            $userinfo=new Userinfo;
                

                    $rez=Yii::$app->request->post('userinput');
                    

                 foreach($rez as $key => $value){
                     
                       if($value['cur_input_value']==='emptyfield'){
                           $whitelist=['surname','name','ava','job_place','aboutme','age','website','email'];
                           $elemname=substr($value['cur_empty_name'],5,strlen($value['cur_empty_name']));
                           if(in_array($elemname,$whitelist,true)){        $userinfo->clearrow($elemname,$user_id);}
                        
                        } else{ 

                            if($value['cur_input_name']==='user-surname')

                              {$user_surname=$value['cur_input_value'];
                                                           
                                $userinfo->setsurname($user_surname);
                              }
                              
                             if($value['cur_input_name']==='user-name')

                              {$user_name=$value['cur_input_value'];
                                                           
                                $userinfo->updatename($user_name);
                              }

                            if($value['cur_input_name']==='user-job')

                              {$user_job=$value['cur_input_value'];
                                 $userinfo->setjob($user_job);
                              }


                            if($value['cur_input_name']==='user-age')

                              {$user_age=$value['cur_input_value'];
                               $userinfo->setage($user_age);
                              }


                            if($value['cur_input_name']==='user-website')

                              {$user_web=$value['cur_input_value'];
                                $userinfo->setwebsite($user_web);
                              }  
                              
                              
                            if($value['cur_input_name']==='user-email')

                              {$user_email=$value['cur_input_value'];
                                $userinfo->setemail($user_email);
                              }  
                              
        
                              
                        }
                     }
                
          
                }
                
         }

         }

}