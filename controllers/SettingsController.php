<?php
namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\art\Interests;
use app\models\art\Likes;
use app\models\art\Map_coords;
use app\models\art\Userinfo;
use app\models\art\Settings;
use app\models\art\Project;
use app\models\art\User;
use app\models\art\Friends;
use app\models\art\Setting_friendlist_showproject;
use app\models\art\Setting_friendlist_perpage;
use app\models\art\Setting_friendlist_commentproject;


Class SettingsController extends Controller {
    
    protected function getuserid($nickname){
        
         $user=new User;
            $user_id=$user->find_id_nickname($nickname);
            return $user_id;
    }
        
    
    
    
    /* обьект класса Priject*/
     private function proj(){return new Project;}
    
            public $layout='personalpage'; 
           

            public function getuser_id(){ return Yii::$app->user->getid();}

            
    public function actionGetava(){

                $avatarload=new Userinfo;

                 $ava_get=$avatarload->getAvatar();
                if($ava_get=='')
                 {
                     $ava_get='/images/no_foto.png';

                }

                return $ava_get;
    }



         
         
            public function actionGetfriends(){
                
                       $freinds_obj= new Friends;
                $friends_arr= $freinds_obj->getacceptedFriends($this->getuser_id());
                
                
            }          

            public function actionIndex(){
                $freinds_obj= new Friends;
                $friends_arr= $freinds_obj->getacceptedFriends($this->getuser_id());
                $setting_friendlist=[];
                $user_id=Yii::$app->user->getid();
                $user= new User;
      
            if($user_id) {
                $setting=new Settings;
                $settings=$setting->get_all_info($user_id);
                
    
                  
                    $perpage_friendlist= new Setting_friendlist_perpage;
                    $rez=$perpage_friendlist->getitems($user_id);
                    if(sizeof($rez)>0){
                         $setting_friendlist['perspage']=[];
                        $size=sizeof($rez);
                        for($i=0;$i<$size;$i++){
                                $setting_friendlist['perspage'][$i]=$user->find_nickname_by_id($rez[$i]['friend_id']);
                        }

                    }
              
                 
                 
                 
  
                    
                    $project_friendlist= new Setting_friendlist_showproject;
                    $rez=$project_friendlist->getitems($user_id);
                    if(sizeof($rez) > 0){
                        $setting_friendlist['project']=[];
                        $size=sizeof($rez);
                        for($i=0;$i<$size;$i++){
                            $setting_friendlist['project'][$i]=$user->find_nickname_by_id($rez[$i]['friend_id']);
                        }
                    }
       

          
                 
                 
   
                    
                    $project_friendlist= new Setting_friendlist_commentproject;
                    $rez=$project_friendlist->getitems($user_id);
                    if(sizeof($rez) > 0){
                        $setting_friendlist['comment']=[];
                        $size=sizeof($rez);
                        for($i=0;$i<$size;$i++){
                            $setting_friendlist['comment'][$i]=$user->find_nickname_by_id($rez[$i]['friend_id']);
                        }
                    }
       

              
 
                    return $this->render('index',["friendlist"=>$setting_friendlist,'friends'=>$friends_arr,'perspage_rule'=>$settings['personal_page_rule'],'project_rule'=>$settings['project_rule'],'comment_rule'=>$settings['comment_rule']]);

             }
            
            }

    public function actionCheckdata(){
        if(Yii::$app->user->identity->nickname){
                    $passw=Yii::$app->request->post('passw');
                    $passw=base64_decode($passw);
        $us=new User;

        $passw_hash=$us->getPassword(Yii::$app->user->identity->nickname);
        if(password_verify($passw,$passw_hash)){
            return 'ok';
        }else{
            return 'error';
        }
            
        }

    }
     
     public function actionGeneralSettings(){
        $proj = new Project;
        $friend = new Friends;
        $status=0;
        $status_setting=0;
        $settings= new Settings;

        $user_id=Yii::$app->user->identity->id;
        $settings_arr=Yii::$app->request->post("settings_arr");
        
         function del_friend_list($class,$post_filed,$settings_arr){
                 $user_id=Yii::$app->user->getid();
               $friendlist_perspage=new $class;
              $size=sizeof($settings_arr[$post_filed]['del']);
              $friend_id_arr=[];
              $user=new User;
              for($i=0;$i<$size;$i++){
                  $value=$settings_arr[$post_filed]['del'][$i];
                   $friend_id=$user->find_id_nickname($value);
                  if(gettype($friend_id)== 'integer'){
                      array_push($friend_id_arr,$friend_id);
                  }
              }
              if(sizeof($friend_id_arr)>0){
                 $sepp=implode(',',$friend_id_arr);
                 $friendlist_perspage->del_items($user_id,$sepp);
              }
         }
         
         function set_friend_list($class,$post_filed,$settings_arr){
                    $user_id=Yii::$app->user->getid();      
                $settings_project= new $class;
                      $size=sizeof($settings_arr[$post_filed]['friend_list']);
                      $values_arr=[];
                       $user=new User;
                    for($i=0;$i<$size;$i++){
                          $value=$settings_arr[$post_filed]['friend_list'][$i];
                          $friend_id=$user->find_id_nickname($value);
                          if(gettype($friend_id=='integer')){
                            array_push($values_arr,"(".$user_id.','.$friend_id.")");  
                          }
                     }
                     $expl_string=implode(',',$values_arr);
                     $settings_project->set_items( $expl_string); 
         }
         
         
        if(array_key_exists('delproj', $settings_arr)){
            
                $proj->del_proj($user_id);
                 $status_setting=1;
        }   
         
        if(array_key_exists('passw', $settings_arr)){
            $passw=base64_decode($settings_arr['passw']);
            $hash_passw=password_hash($passw, PASSWORD_DEFAULT);

            $user= new User;
            $rez=$user->insert_new_password($user_id,$hash_passw);
            $status=1;
        }   
      
                 
        if(array_key_exists('personalpage_show_rule', $settings_arr)){
                $item;
                $show_val=$settings_arr['personalpage_show_rule']['val'] ;
                switch($show_val){
                      
                      case('all'):
                          $item=0;break;
                      case('friend'):
                          $item=1;break;
                      case('some_friend'):
                          $item=2;break;
                      case('nobody'):
                          $item=3;break;
                      
                  }
                $settings->set_show_perspage_setting($user_id,$item);

                              //first check del statement
                
                   if(array_key_exists('del', $settings_arr['personalpage_show_rule'])){
                            del_friend_list("app\models\art\Setting_friendlist_perpage",'personalpage_show_rule',  $settings_arr);
                }
            
               //then  check add friend
               if(array_key_exists('friend_list', $settings_arr['personalpage_show_rule'])){
                        set_friend_list("app\models\art\Setting_friendlist_perpage",'personalpage_show_rule',  $settings_arr);

              }
            
              
                  $status_setting=1;
            }   

        if(array_key_exists('project_show_rule', $settings_arr)){
                $item;
                $show_val=$settings_arr['project_show_rule']['val'] ;
                  switch($show_val){
                      
                      case('define_in_project'):
                          $item=0;break;
                      case('all'):
                          $item=1;break;
                      case('friend'):
                          $item=2;break;
                      case('some_friend'):
                          $item=3;break;
                      case('nobody'):
                          $item=4;break;
                      
                  }
                  
                  $settings->set_show_project_setting($user_id,$item);
                        //first del statemnet    
                if(array_key_exists('del', $settings_arr['project_show_rule'])){
                         del_friend_list("app\models\art\Setting_friendlist_showproject",'project_show_rule',  $settings_arr);
                }
            
                if(array_key_exists('friend_list', $settings_arr['project_show_rule'])){
                         set_friend_list("app\models\art\Setting_friendlist_showproject",'project_show_rule',  $settings_arr);
                }
                           
                    $status_setting=1;
            } 
            

            if(array_key_exists('comment_show_rule', $settings_arr)){
                $item;
                $show_val=$settings_arr['comment_show_rule']['val'] ;
                     switch($show_val){
                      
                        case('define_in_project'):
                            $item=0;break;
                        case('all'):
                            $item=1;break;
                        case('friend'):
                            $item=2;break;
                         case('some_friend'):
                            $item=3;break;
                        case('nobody'):
                            $item=4;break;
                      
                    }
                $settings->set_comment_setting($user_id,$item);

               if(array_key_exists('del', $settings_arr['comment_show_rule'])){
                         del_friend_list("app\models\art\Setting_friendlist_commentproject",'comment_show_rule',  $settings_arr);
                }
            
                if(array_key_exists('friend_list', $settings_arr['comment_show_rule'])){
                         set_friend_list("app\models\art\Setting_friendlist_commentproject",'comment_show_rule',  $settings_arr);
                }
                      
                  $status_setting=1;
            } 
            
            //return rezult
            if($status && $status_setting){
                return json_encode(['passw_and_settings'=>1]);
            }else if(!$status && $status_setting){
                return json_encode(['settings'=>1]);
            }
            else{
               return json_encode(['passw'=>1]); 
            }
            
            
            
     }
     

}

