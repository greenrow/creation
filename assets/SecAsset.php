<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SecAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web/';
    public $css = [
        'css/sec_style.css',
          'css/colorpicker.css',
          'css/layout_color.css',
        'css/bootstrap-tagsinput.css'
 
      
       
    ];

    
    public $js=['js/jquery_ui.min.js','js/snap.js','js/colorpicker.js','js/bootstrap-tagsinput.min.js','js/cropper.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
