<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Comments {
    
   public function sendcomm($user_id,$proj_id,$text,$date){

      $rez=Yii::$app->db->createCommand('INSERT INTO comments(project_id,user_id,text,date) VALUES (:proj_id,:user_id,:text,:date)')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id,':text'=>$text,':date'=>$date])->execute();

    if($rez){return 1;}
}

   public function showcomm($proj_id){

       $rez=Yii::$app->db->createCommand('SELECT u.nickname,usinf.ava,comm.text,comm.date AS date FROM user u JOIN comments comm ON u.id=comm.user_id JOIN userinfo usinf ON usinf.user_id =u.id  WHERE comm.project_id=:proj_id ORDER BY date DESC')->bindValues([':proj_id'=>$proj_id])->queryAll();

        if($rez){return $rez;}else {return 0;}
}


   public function commentrule($proj_id,$user_var){

      $rez=Yii::$app->db->createCommand(' SELECT p.id FROM user u JOIN settings s ON s.user_id=u.id  JOIN project p ON p.user_id=u.id JOIN project_settings ps ON p.id=ps.proj_id
  
 WHERE  (s.comment_rule=1 OR (s.comment_rule=0 and ps.comment_rule=0))   AND p.id=:proj_id '.$user_var)->bindValues([':proj_id'=>$proj_id])->queryAll();

    if($rez){return $rez;}else {return 0;}
}






}