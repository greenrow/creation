<?php namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Project_tag{

       public function settag_proj($tag_id,$proj_id){

                Yii::$app->db->createCommand("INSERT INTO project_tag(tag_id,proj_id) VALUES(:tag_id,:proj_id)")->bindValues([':tag_id'=>$tag_id,':proj_id'=>$proj_id])->execute(); 

        }
            
       public function deltag_proj($tag_id_arr){

                Yii::$app->db->createCommand("DELETE  FROM tag  WHERE tag.id IN(".$tag_id_arr.")")->execute(); 

        }
        
          public  function get_tag($proj_id){
            $rez=Yii::$app->db->createCommand("SELECT t.name,t.id  FROM tag t,project_tag pt  WHERE pt.tag_id=t.id AND pt.proj_id=:projid")->bindValues([':projid'=>$proj_id])->queryAll(); 
       return $rez;
        }
        

}
