<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Category extends \yii\db\ActiveRecord {
    
        

function find_categ($name){
    
    
                $count=Yii::$app->db->createCommand('Select id From category WHERE name=:name')->bindValues([':name'=>$name])->query();
                return count($count);
    
}

//               public function setcateg($name){
//
//                Yii::$app->db->createCommand("INSERT INTO category(name) VALUES(:name)")->bindValues([':name'=>$name])->execute(); 
//
//        }
        
                 public function getcateg_id($name){

               $id_categ= Yii::$app->db->createCommand('Select id From category Where name=:name')->bindValues([':name'=>$name])->queryOne(); 
             return $id_categ['id'];

        }
        
                 public function getcateg($proj_id){
                $rez=Yii::$app->db->createCommand("Select cat.name,cat.id FROM category cat,categ_projects catproj  WHERE catproj.categ_id=cat.id AND catproj.proj_id=:proj_id")->bindValues([':proj_id'=>$proj_id])->queryAll(); 
                return $rez;
        }
        
        public function getall_category(){
                  $allcateg= Yii::$app->db->createCommand('Select name From category ')->queryAll(); 
             return  $allcateg;
            
        }
        
            public function view_categ_charts($user_id){
                
                $maxcount['max_count']=Yii::$app->db->createCommand('SELECT count(*) as count  FROM categ_projects catproj,project p WHERE p.user_id=:user_id AND p.id=catproj.proj_id GROUP BY catproj.categ_id ORDER by count DESC LIMIT 1 ')->bindValue(':user_id',$user_id)->queryOne();
                $allcateg['all_categ']=Yii::$app->db->createCommand('SELECT name FROM category')->queryAll();
                $rez['user_categ']= Yii::$app->db->createCommand('SELECT cat.name as name,COUNT(catproj.proj_id) as count   FROM category cat,categ_projects catproj,project p WHERE p.user_id=:user_id AND p.id=catproj.proj_id AND cat.id= catproj.categ_id GROUP BY catproj.categ_id')->bindValue(':user_id',$user_id)->query()->readAll();
            $all=array_merge($allcateg,$rez,$maxcount);
               return $all;
        
    }
    
            public function count_proj($user_id){
            
                     $active_proj['active']= Yii::$app->db->createCommand('SELECT id as active FROM project WHERE user_id=:user_id AND publish=1')->bindValues([':user_id'=>$user_id])->query()->count(); 
                     $noactive_proj['noactive']= Yii::$app->db->createCommand('SELECT id as noactive FROM project WHERE user_id=:user_id AND publish=0')->bindValues([':user_id'=>$user_id])->query()->count(); 
                     $all=  array_merge($active_proj,$noactive_proj);
                     return  $all;
        }
        
       
}
