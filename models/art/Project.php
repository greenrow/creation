<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Project extends \yii\db\ActiveRecord {
    

    public function projset($name,$ava,$bckg,$desc,$user_id,$date){

               Yii::$app->db->createCommand('INSERT INTO project(name,ava,background,description,user_id,date) VALUES(:name,:ava,:bckg,:desc,:user_id,:date)')->bindValues([':name'=>$name,':ava'=>$ava,':bckg'=>$bckg,':desc'=>$desc,':user_id'=>$user_id,':date'=>$date])->execute(); 
     
}
public function changeproject_name($projname,$projid){
    $rez=Yii::$app->db->createCommand('UPDATE project p SET name=:name WHERE p.id=:projid')->bindValues([':projid'=>$projid,':name'=>$projname])->execute();
    
}

public function changeproject_ava($projava,$projid){
    $rez=Yii::$app->db->createCommand('UPDATE project p SET ava=:ava WHERE p.id=:projid')->bindValues([':projid'=>$projid,':ava'=>$projava])->execute();
    
}
    public function getproject_view($host,$proj_id,$date){

        $check_ip=Yii::$app->db->createCommand('SELECT id FROM log_user_ip WHERE host_ip=:host AND proj_id=:proj_id')->bindValues([':host'=>$host,':proj_id'=>$proj_id])->query()->count();
        if($check_ip == 0){Yii::$app->db->createCommand('INSERT INTO log_user_ip(host_ip,date,proj_id) VALUES(:host,:date,:proj_id) ')->bindValues([':host'=>$host,':proj_id'=>$proj_id,':date'=>$date])->execute();return 'ip_insert';}
    }
    
    public function getallview(){
        $allviews=Yii::$app->db->createCommand('SELECT COUNT(id) as count,proj_id FROM log_user_ip GROUP BY proj_id ')->queryAll();
        return   $allviews;
    }
   public function getview_of_one_project($proj_id){
        $views=Yii::$app->db->createCommand('SELECT COUNT(id) as count FROM log_user_ip WHERE proj_id=:proj_id ')->bindValues([':proj_id'=>$proj_id])->queryOne();
        return   $views['count'];
    }

    public function getproject_for_news($proj_id){
        $user_id= $this->get_user_id($proj_id);
        $rez=Yii::$app->db->createCommand('SELECT usinf.ava as avatar,p.name,p.ava,p.description,p.date,u.nickname FROM project p,user u, userinfo usinf  WHERE p.id=:proj_id AND u.id=:user_id AND u.id=usinf.user_id')->bindValues([':proj_id'=>$proj_id,':user_id'=>$user_id])->queryOne();
       return $rez;
    }

    public function proj_id($user_id){
        
       $max = Yii::$app->db->createCommand('SELECT max(id) as max FROM project WHERE user_id=:user_id')->bindValue(':user_id',$user_id)->queryScalar();
       return $max;
}

    public function get_user_id($proj_id){
        $rez= Yii::$app->db->createCommand('SELECT user_id  FROM project WHERE id=:proj_id')->bindValue(':proj_id',$proj_id)->queryOne();
        return $rez['user_id'];
    }


        public function get_project_files($user_id){

           $project['proj'] = Yii::$app->db->createCommand('SELECT*  FROM project WHERE user_id=:user_id')->bindValue(':user_id',$user_id)->query()->readAll(); 
           $nick['nick']=Yii::$app->db->createCommand('SELECT nickname  FROM user WHERE id=:user_id')->bindValue(':user_id',$user_id)->queryOne(); 
           $rez=array_merge($project, $nick);
           return $rez;

        }


        public function get_projectfoto_details($proj_id,$user_id){

           $max = Yii::$app->db->createCommand('SELECT f.position,f.path,f.id FROM project p,project_foto f WHERE p.id=:proj_id AND p.user_id=:user_id AND f.project_id=p.id  ORDER BY f.position ASC')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->readAll(); 
           return $max;

        }
        
           public function get_projectarea_details($proj_id,$user_id){

           $max = Yii::$app->db->createCommand('SELECT a.position,a.textarea,a.id FROM project p,project_area a WHERE p.id=:proj_id AND p.user_id=:user_id AND a.proj_id=p.id  ORDER BY a.position ASC')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->readAll(); 
           return $max;

        }
        
        public function get_project_info($proj_id,$user_id){
            
             $max = Yii::$app->db->createCommand('SELECT * FROM project p  WHERE p.id=:proj_id AND p.user_id=:user_id ')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->read(); 
           return $max;
        }
        
   

        public function publish_proj($proj_id){
            
                $max = Yii::$app->db->createCommand('UPDATE project SET publish =1 WHERE id=:proj_id')->bindValues([':proj_id'=>$proj_id])->execute(); 
           return $max;
            
        }
        
           public function find_publish($proj_id){
            
                $max = Yii::$app->db->createCommand('SELECT publish FROM project WHERE id=:proj_id')->bindValues([':proj_id'=>$proj_id])->query(); 
           return $max;
            
        }
        //for all projects
        static $popular_query_stat='SELECT  COUNT(hip.host_ip) as count_viewed, u.nickname,p.id,p.publish,p.date,p.user_id,p.name,p.ava,p.date,u.nickname FROM project p JOIN user u ON u.id=p.user_id JOIN settings s ON s.user_id=u.id JOIN project_settings ps ON p.id=ps.proj_id  JOIN log_user_ip hip ON p.id=hip.proj_id ';
        static $likes_query_stat='SELECT COUNT(lk.user_id) as count_likes, u.nickname,p.id,p.publish,p.date,p.user_id,p.name,p.ava,p.date,u.nickname FROM project p JOIN user u ON u.id=p.user_id JOIN settings s ON s.user_id=u.id JOIN project_settings ps ON p.id=ps.proj_id  JOIN likes lk ON p.id=lk.proj_id ';
       static $project_stat_query='SELECT u.nickname,p.id,p.publish,p.date,p.user_id,p.name,p.ava,p.date,u.nickname FROM project p JOIN user u ON u.id=p.user_id JOIN settings s ON s.user_id=u.id JOIN project_settings ps ON p.id=ps.proj_id ';
        static $project_end_query=' ((s.project_rule=1 OR (s.project_rule=0 and ps.show_rule=0)) ';
        //sort filters
                public function get_all_publish_proj($user_var){
       
                     $rez= Yii::$app->db->createCommand(self::$project_stat_query.' WHERE '.self::$project_end_query.$user_var)->query()->readAll(); 
                    
               // $max = Yii::$app->db->createCommand('SELECT  u.nickname,p.id,p.publish,p.date,p.user_id,p.name,p.ava,p.date,u.nickname FROM project p,user u WHERE u.id=p.user_id '.$user_var)->query()->readAll(); 
           return $rez;
            
        }
        
           public function get_all_publish_proj_by_cat($cat_name,$user_var){
                $max = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN categ_projects catp ON catp.proj_id=p.id JOIN category cat ON cat.id=catp.categ_id  WHERE  cat.name=:cat_name AND  '.self::$project_end_query.$user_var )->bindValues([':cat_name'=>$cat_name])->query()->readAll(); 
           return $max;
            
        }
        
       public function get_all_proj_by_active($user_var){
                $max = Yii::$app->db->createCommand('SELECT  u.nickname,p.id,p.publish,p.date,p.user_id,p.name,p.ava,p.date  FROM project p,user u WHERE u.id=p.user_id AND p.publish="1" '.$user_var)->query()->readAll(); 
           return $max;
            
        }
        
       public function get_all_proj_by_noactive($user_var){
                $max = Yii::$app->db->createCommand('SELECT  u.nickname,p.id,p.publish,p.date,p.user_id,p.name,p.ava,p.date  FROM project p,user u WHERE u.id=p.user_id AND p.publish="0" '.$user_var)->query()->readAll(); 
           return $max;
            
        }
        
        
        
        public function get_all_proj_by_location($loc,$user_var){
            
                    $max = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN map_coords map ON map.user_id=p.user_id WHERE u.id=p.user_id AND  map.location=:loc AND '.self::$project_end_query.$user_var)->bindValues([':loc'=>$loc])->query()->readAll(); 
           return $max;
            
        }
        
         public function get_all_proj_by_date($date,$user_var){
            
                    $max = Yii::$app->db->createCommand(self::$project_stat_query.' WHERE u.id=p.user_id  AND p.date=:date AND'.self::$project_end_query.$user_var)->bindValues([':date'=>$date])->query()->readAll(); 
           return $max;
            
        }
        
         public function get_all_proj_by_curdate($user_var){
            
                    $max = Yii::$app->db->createCommand(self::$project_stat_query.' WHERE u.id=p.user_id  AND p.date=CURDATE() AND '.self::$project_end_query.$user_var)->query()->readAll(); 
           return $max;
            
        }
        
        public function get_all_proj_by_week($user_var){
            
                    $max = Yii::$app->db->createCommand(self::$project_stat_query.' WHERE u.id=p.user_id  AND YEAR(p.date)=YEAR(NOW()) AND WEEK(p.date)=WEEK(NOW()) AND '.self::$project_end_query.$user_var)->query()->readAll(); 
           return $max;
            
        }
        
       public function get_all_proj_by_month($user_var){
            
                    $max = Yii::$app->db->createCommand(self::$project_stat_query.' WHERE u.id=p.user_id  AND YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND '.self::$project_end_query.$user_var)->query()->readAll(); 
           return $max;
            
        }
        
        
     public function get_all_publish_proj_by_cat_and_loc($cat_name,$locname,$user_var){
                $rez = Yii::$app->db->createCommand(self::$project_stat_query.' JOIN categ_projects catp ON p.id=catp.proj_id JOIN category cat ON  cat.id=catp.categ_id JOIN map_coords map ON  map.user_id=p.user_id WHERE cat.name=:cat_name  AND map.location=:loc AND' .self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat_name,':loc'=>$locname])->query()->readAll(); 
           return $rez;
            
        }
        
            public function     get_all_proj_by_month_and_loc($locname,$user_var){
                $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN map_coords map ON  map.user_id=p.user_id WHERE map.location=:loc AND YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND '.self::$project_end_query.$user_var)->bindValues([':loc'=>$locname])->query()->readAll(); 
           return $rez;
            
        }
        
        
        public function get_all_proj_by_calendar_and_loc($loc,$date,$user_var){
           $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN map_coords map ON  map.user_id=p.user_id WHERE map.location=:loc AND p.date=:date AND '.self::$project_end_query.$user_var)->bindValues([':loc'=>$loc,':date'=>$date])->query()->readAll(); 
           return $rez;
            
            
        }
        
        public function get_all_proj_by_day_and_loc($loc,$user_var){
           $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN map_coords map ON map.user_id=p.user_id WHERE  map.location=:loc AND p.date=CURDATE() AND '.self::$project_end_query.$user_var)->bindValues([':loc'=>$loc])->query()->readAll(); 
           return $rez;  
        }
        
        public function get_all_proj_by_week_and_loc($loc,$user_var){
              $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN map_coords map ON map.user_id=p.user_id WHERE  map.location=:loc AND YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND WEEK(p.date)=WEEK(NOW()) AND '.self::$project_end_query.$user_var)->bindValues([':loc'=>$loc])->query()->readAll(); 
           return $rez; 
            
        }
    
        
        //category and date//
        
        public function get_all_proj_by_calendar_and_cat($cat,$date,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND p.date=:date  AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat,':date'=>$date])->query()->readAll(); 
           return $rez;
            
        }
        
       public function get_all_proj_by_thisday_and_cat($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name  AND p.date=CURDATE() AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        public function get_all_proj_by_week_and_cat($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND YEAR(p.date)=YEAR(NOW()) AND WEEK(p.date)=WEEK(NOW()) AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        public function get_all_proj_by_month_and_cat($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND  YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        public function get_all_proj_by_likes($user_var){
           $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' AND '.self::$project_end_query.$user_var.' GROUP BY id ORDER BY count_likes DESC')->query()->readAll(); 
           return $rez;
            
        }
        
        public function get_all_proj_by_viewed($user_var){
           $rez = Yii::$app->db->createCommand(self::$popular_query_stat.' AND '.self::$project_end_query.$user_var.' GROUP BY id ORDER BY count_viewed DESC')->query()->readAll(); 
           return $rez;
            
        }
    
       public function get_all_proj_by_calendar_and_cat_and_loc($cat,$date,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN  map_coords m ON p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE  m.location=:loc AND cat.name=:cat_name  AND p.date=:date AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat,':date'=>$date,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
        
       public function get_all_proj_by_day_and_cat_and_loc($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN  map_coords m ON p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE  m.location=:loc AND cat.name=:cat_name AND p.date=CURDATE()  AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
        
        public function get_all_proj_by_week_and_cat_and_loc($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN  map_coords m ON p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE  m.location=:loc AND cat.name=:cat_name AND YEAR(p.date)=YEAR(NOW()) AND WEEK(p.date)=WEEK(NOW())  AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
        public function get_all_proj_by_month_and_cat_and_loc($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$project_stat_query.'JOIN  map_coords m ON p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id JOIN  category cat ON cat.id=catp.categ_id WHERE  m.location=:loc AND cat.name=:cat_name AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())   AND '.self::$project_end_query.$user_var)->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
        
        //sort//
        
        /*****likes_cat***/
        public function get_all_proj_by_likes_and_cat($cat,$user_var){
                              $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' JOIN categ_projects catp ON p.id=catp.proj_id JOIN category cat ON cat.id=catp.categ_id WHERE  cat.name=:cat_name  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;  
            
        }
        /*views_cat***/
      public function get_all_proj_by_viwed_and_cat($cat,$user_var){
                              $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id JOIN category cat ON cat.id=catp.categ_id WHERE  cat.name=:cat_name  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;  
            
        }
        /***views cat loc***/
       public function get_all_proj_by_viwed_and_cat_loc($cat,$loc,$user_var){
             $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id JOIN category cat ON cat.id=catp.categ_id JOIN map_coords m ON p.user_id = m.user_id WHERE cat.name=:cat_name AND m.location=:loc AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat,":loc"=>$loc])->query()->readAll(); 
           return $rez;  
            
        }
             /***likes cat loc***/
      public function get_all_proj_by_likes_and_cat_loc($cat,$loc,$user_var){
             $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' JOIN categ_projects catp ON p.id=catp.proj_id JOIN category cat ON cat.id=catp.categ_id JOIN map_coords m ON p.user_id = m.user_id WHERE cat.name=:cat_name AND m.location=:loc AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat,":loc"=>$loc])->query()->readAll(); 
           return $rez;  
            
        }
        
        /**likes loc**/
        
            public function get_all_proj_by_likes_and_loc($loc,$user_var){
             $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' JOIN map_coords m ON p.user_id = m.user_id WHERE  m.location=:loc AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([":loc"=>$loc])->query()->readAll(); 
           return $rez;  
            
        }
        
                /**views loc**/
        
            public function get_all_proj_by_viwed_and_loc($loc,$user_var){
             $rez = Yii::$app->db->createCommand(self::$popular_query_stat.' JOIN map_coords m ON p.user_id = m.user_id WHERE  m.location=:loc  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([":loc"=>$loc])->query()->readAll(); 
           return $rez;  
            
        }
        
        
        /***************date sort category view*************/
       
       /////--month loc cat view-////
        
        public function get_all_proj_by_month_and_cat_and_loc_and_view($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  m.location=:loc AND cat.name=:cat_name  AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
               /////--month loc cat likes-////
        
       public function get_all_proj_by_month_and_cat_and_loc_and_likes($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  m.location=:loc AND cat.name=:cat_name  AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
        
                       /////--day loc cat likes-////
        
       public function get_all_proj_by_day_and_cat_and_loc_and_likes($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  u.id=p.user_id AND m.location=:loc AND cat.name=:cat_name  AND p.date=CURDATE() AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
             
       /////--day loc cat view-////
        
        public function get_all_proj_by_day_and_cat_and_loc_and_views($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE  m.location=:loc AND cat.name=:cat_name AND  p.date=CURDATE() AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
                     
       /////--week loc cat view-////
        
        public function get_all_proj_by_week_and_cat_and_loc_and_views($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE m.location=:loc AND cat.name=:cat_name  AND WEEK(p.date)=WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
            return $rez;
            
        }
        
                               /////--week loc cat likes-////
        
       public function get_all_proj_by_week_and_cat_and_loc_and_likes($cat,$loc,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  m.location=:loc AND cat.name=:cat_name AND WEEK(p.date)=WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc])->query()->readAll(); 
           return $rez;
            
        }
        
        
               /////--calendar_loc cat view-////
        
        public function get_all_proj_by_calendar_and_cat_and_loc_and_views($cat,$loc,$date,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE  m.location=:loc AND cat.name=:cat_name   AND p.date=:date  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc,':date'=>$date])->query()->readAll(); 
            return $rez;
            
        }
        
                               /////--calendar loc cat likes-////
        
       public function get_all_proj_by_calendar_and_cat_and_loc_and_likes($cat,$loc,$date,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' JOIN  map_coords m ON  p.user_id = m.user_id JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE u.id=p.user_id AND p.user_id = m.user_id AND p.id=catp.proj_id AND m.location=:loc AND cat.name=:cat_name AND cat.id=catp.categ_id AND p.id =lk.proj_id  AND p.date=:date  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat,':loc'=>$loc,':date'=>$date])->query()->readAll(); 
           return $rez;
            
        }
        /**************date sort category*******************/
        
                       /////--calendar_cat view-////
        
        public function get_all_proj_by_calendar_and_cat_and_views($cat,$date,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  cat.name=:cat_name AND  p.date=:date AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat,':date'=>$date])->query()->readAll(); 
            return $rez;
            
        }
        
                               /////--calendar cat likes-////
        
       public function get_all_proj_by_calendar_and_cat_and_likes($cat,$date,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND p.date=:date  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat,':date'=>$date])->query()->readAll(); 
           return $rez;
            
        }
        
                            /////--day cat likes-////
        
       public function get_all_proj_by_day_and_cat_and_likes($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND  p.date=CURDATE() AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
          /////--day cat views-////
 
        public function get_all_proj_by_day_and_cat_and_views($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name  AND  p.date=CURDATE() AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
                /////--month cat view-////
        
        public function get_all_proj_by_month_and_cat_and_view($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
               /////--month cat likes-////
        
       public function get_all_proj_by_month_and_cat_and_likes($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  cat.name=:cat_name AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        
        //week cat likew
        
            public function get_all_proj_by_week_and_cat_and_likes($cat,$user_var){
                    $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id  WHERE  cat.name=:cat_name AND WEEK(p.date)= WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
          //week cat views
        
            public function get_all_proj_by_week_and_cat_and_views($cat,$user_var){
                       $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'JOIN categ_projects catp ON p.id=catp.proj_id  JOIN category cat ON cat.id=catp.categ_id WHERE cat.name=:cat_name AND WEEK(p.date)= WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        //****like date loc***////
        //
            public function get_all_proj_by_likes_and_alldate_and_loc($date,$user_var,$loc){
            $query='';
            $bind_val=[':loc'=>$loc];
            if($date=='week'){
                $query='WEEK(p.date)=WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  if($date=='day'){
                $query=' p.date=CURDATE()';
            }else if($date=='month'){
                $query='MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  {
                $query='p.date=:date';
                 $bind_val=[':date'=>$date,':loc'=>$loc];
            }
            
            $rez = Yii::$app->db->createCommand(self::$likes_query_stat.' JOIN  map_coords m ON  p.user_id = m.user_id WHERE '.$query.' AND m.location=:loc AND' .self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues($bind_val)->query()->readAll(); 
           return $rez;
        }
        
               //****view date loc***////
        //
            public function get_all_proj_by_views_and_alldate_and_loc($date,$user_var,$loc){
            $query='';
               $bind_val=[':loc'=>$loc];
            if($date=='week'){
                $query='WEEK(p.date)=WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  if($date=='day'){
                $query=' p.date=CURDATE()';
            }else if($date=='month'){
                $query='MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  {
                $query='p.date=:date';
                  $bind_val=[':date'=>$date,':loc'=>$loc];
            }
            
            $rez = Yii::$app->db->createCommand(self::$popular_query_stat.' JOIN  map_coords m ON  p.user_id = m.user_id WHERE '.$query.' AND m.location=:loc AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues($bind_val)->query()->readAll(); 
           return $rez;
        }
        
        
        //***like date***/
        public function get_all_proj_by_likes_and_alldate($date,$user_var){
            $query='';
            if($date=='week'){
                $query='WEEK(p.date)=WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  if($date=='day'){
                $query=' p.date=CURDATE()';
            }else if($date=='month'){
                $query='MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  {
                $query='p.date=:date';
            }
            
            $rez = Yii::$app->db->createCommand(self::$likes_query_stat.'WHERE '.$query.' AND' .self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_likes DESC')->bindValues([':date'=>$date])->query()->readAll(); 
           return $rez;
        }
        
        //view date
                public function get_all_proj_by_views_and_alldate($date,$user_var){
            $query='';
            if($date=='week'){
                $query='WEEK(p.date)=WEEK(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  if($date=='day'){
                $query=' p.date=CURDATE()';
            }else if($date=='month'){
                $query='MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW())';
            }else  {
                $query='p.date=:date';
            }
            
            $rez = Yii::$app->db->createCommand(self::$popular_query_stat.'WHERE '.$query.'  AND '.self::$project_end_query.$user_var.' GROUP BY p.id ORDER BY count_viewed DESC')->bindValues([':date'=>$date])->query()->readAll(); 
           return $rez;
        }
        
        
        //**********************data active*********////
        
        
        ///--calendat cat active--/////
        
        public function get_all_proj_by_calendar_and_cat_and_active($cat,$date,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT  u.nickname,p.publish,p.id,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id  AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id   AND p.date=:date AND p.publish="1"'.$user_var)->bindValues([':cat_name'=>$cat,':date'=>$date])->query()->readAll(); 
           return $rez;
            
        }
        
        
                
        ///--calendat cat noactive--/////
        
        public function get_all_proj_by_calendar_and_cat_and_noactive($cat,$date,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT  u.nickname,p.publish,p.id,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id  AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id   AND p.date=:date AND p.publish="0"'.$user_var)->bindValues([':cat_name'=>$cat,':date'=>$date])->query()->readAll(); 
           return $rez;
            
        }
        
               /////--week loc cat view-////
        

        
                               /////--week loc cat likes-////
        

        
               
   
        
                       
                       /////--day cat active-////
        
       public function get_all_proj_by_day_and_cat_and_active($cat,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT  u.nickname,p.publish,p.id,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id AND p.date=CURDATE() AND p.publish="1"'.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
       public function get_all_proj_by_day_and_cat_and_noactive($cat,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT  u.nickname,p.publish,p.id,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id AND p.date=CURDATE() AND p.publish="0"'.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
             
     
      
        
        ///--month cat active--///
        
        public function get_all_proj_by_month_and_cat_and_active($cat,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT u.nickname,p.id,p.publish,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id  AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND p.publish="1" '.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        
        ///==mont cat noactive---///
        
                
        public function get_all_proj_by_month_and_cat_and_noactive($cat,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT u.nickname,p.id,p.publish,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id  AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id AND MONTH(p.date)=MONTH(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND p.publish="0" '.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
        ///--week cat noactive--///
        

        
                
        public function get_all_proj_by_week_and_cat_and_noactive($cat,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT u.nickname,p.id,p.publish,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id  AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id AND MONTH(p.date)=MONTH(NOW()) AND WEEK(p.date)=WEEK(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND p.publish="0" '.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
        
                
        ///--week cat active--///
        

        
                
        public function get_all_proj_by_week_and_cat_and_active($cat,$user_var){
                    $rez = Yii::$app->db->createCommand('SELECT u.nickname,p.id,p.publish,p.date,p.user_id AS userid,p.name,p.ava FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id  AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id AND MONTH(p.date)=MONTH(NOW()) AND WEEK(p.date)=WEEK(NOW()) AND YEAR(p.date)=YEAR(NOW()) AND p.publish="1" '.$user_var)->bindValues([':cat_name'=>$cat])->query()->readAll(); 
           return $rez;
            
        }
       public function del_proj_files($user_id){
            
                $rez= Yii::$app->db->createCommand('DELETE categ_projects catp,project_area pa, project_foto pf FROM project p LEFT JOIN categ_projects catp  ON p.id=catp.proj_id LEFT JOIN project_area pa  ON p.id=pa.proj_id LEFT JOIN project_foto pf  ON p.id=pf.project_id WHERE p.user_id=:user_id')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
            
        }
        
        public function del_proj($user_id){
            
                $rez= Yii::$app->db->createCommand('DELETE FROM project  WHERE project.user_id=:user_id')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
            
        }
        
       public function hide_proj($user_id){
            
                $rez= Yii::$app->db->createCommand('UPDATE project p SET publish=0 WHERE p.user_id=:user_id')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
            
        }
       
        public function most_viewed_projects($limit){
            
            $rez= Yii::$app->db->createCommand('SELECT COUNT(*) as count,v.proj_id as proj FROM log_user_ip v ,project p WHERE p.id=v.proj_id AND p.publish=1 GROUP BY proj ORDER BY count DESC LIMIT '.$limit)->queryAll();
            return $rez;
        }
        
            public function most_likes_projects($limit){
            
            $rez= Yii::$app->db->createCommand('SELECT COUNT(*) as count,l.proj_id as proj FROM likes l ,project p WHERE p.id=l.proj_id AND p.publish=1 GROUP BY proj ORDER BY count DESC LIMIT '.$limit)->queryAll();
            return $rez;
        }
        public function count_proj($user_id){
                     
                     $active_proj['active']= Yii::$app->db->createCommand('SELECT id as active FROM project WHERE user_id=:user_id AND publish=1')->queryAll(); 
                     $noactive_proj['noactive']= Yii::$app->db->createCommand('SELECT id as noactive FROM project WHERE user_id=:user_id AND publish=0')->bindValues([':user_id'=>$user_id])->query()->count(); 
                     $all=  array_merge($active_proj,$noactive_proj);
                     return  $all;
        }
        
        
     public function get_popular_projects($limit){

        $popular_proj['view']= Yii::$app->db->createCommand('SELECT u.nickname,usinf.ava as user_ava,p.name,p.id as proj_id, p.ava FROM userinfo usinf,user u ,project p WHERE u.id=usinf.user_id AND p.user_id=u.id AND p.publish=1 AND p.id IN(SElECT proj FROM(SELECT COUNT(p.id) as count,v.proj_id as proj,p.user_id as userid FROM log_user_ip v ,project p WHERE p.id=v.proj_id AND p.publish=1 GROUP BY proj ORDER BY count DESC LIMIT :limit)temp_tab)')->bindValue(':limit',$limit)->queryAll(); 
        $popular_proj['views_count']=$this->most_viewed_projects($limit);
        $popular_proj['like']= Yii::$app->db->createCommand('SELECT u.nickname,usinf.ava as user_ava,p.name,p.id as proj_id, p.ava FROM  userinfo usinf,user u, project p WHERE u.id=usinf.user_id AND p.user_id=u.id AND p.publish=1 AND p.id IN(SElECT proj FROM(SELECT COUNT(p.id) as count,l.proj_id as proj FROM likes l ,project p WHERE p.id=l.proj_id AND p.publish=1 GROUP BY proj ORDER BY count DESC LIMIT :limit)temp_tab)')->bindValue(':limit',$limit)->queryAll(); 
        $popular_proj['likes_count']=$this->most_likes_projects($limit);
        return $popular_proj;
    }

public function remove_project($proj_id){
    
    $rez=Yii::$app->db->createCommand("DELETE FROM project  WHERE id=:proj_id ")->bindValue(':proj_id',$proj_id)->execute();
    return $rez;
}

}