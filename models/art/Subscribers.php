<?php

namespace app\models\art;
use Yii;
use yii\base\Model;
use yii\db\Query;
class Subscribers {
    
        

    public function subscribe($subscriber_id,$owner_id){
          
              $finditems=Yii::$app->db->createCommand("SELECT * FROM subscribers  WHERE subscriber_id=:subscriber_id AND subscriber_owner=:owner_id ")->bindValues([':subscriber_id'=>$subscriber_id,':owner_id'=>$owner_id])->queryAll();
              if(sizeof($finditems)==0){
                    $rez= Yii::$app->db->createCommand("INSERT INTO subscribers(subscriber_id,subscriber_owner) VALUES(:subscriber_id,:owner_id)")->bindValues([':subscriber_id'=>$subscriber_id,':owner_id'=>$owner_id])->execute();
                    return 1;
              }else{return 0;}

          

    }
   
   
    public function find_subscribers($subscriber_owner){
       
       $rez= Yii::$app->db->createCommand("SELECT subscriber_id  FROM subscribers WHERE subscriber_owner=:subscriber_owner")->bindValues([':subscriber_owner'=>$subscriber_owner])->query()->count();
   
        if($rez>0){
             $rez= Yii::$app->db->createCommand("SELECT *  FROM subscribers WHERE subscriber_owner=:subscriber_owner")->bindValues([':subscriber_owner'=>$subscriber_owner])->queryAll();
             return $rez;
        }else{return 0;}
       
   }
   
   public function getuser_subscribers($user_id){
       $rez=Yii::$app->db->createCommand('SELECT usinf.ava, u.nickname FROM user u, userinfo usinf, subscribers s WHERE s.subscriber_id=:user_id AND u.id=s.subscriber_owner AND usinf.user_id=u.id')->bindValues([':user_id'=>$user_id])->queryAll();
       
       return $rez;
   }
   

   

}
