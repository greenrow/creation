<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Likes extends \yii\db\ActiveRecord {
    
        

    public function find_user_likes($proj_id,$user_id){
           $rez=Yii::$app->db->createCommand('SELECT id FROM likes WHERE user_id=:user_id AND proj_id=:proj_id')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->count();

        if ($rez==0){


              Yii::$app->db->createCommand("INSERT INTO likes(proj_id,user_id) VALUES(:proj_id,:user_id)")->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->execute(); 
        }else
           {


              Yii::$app->db->createCommand("DELETE  FROM likes WHERE proj_id=:proj_id AND user_id=:user_id ")->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->execute(); 
        }

    }
    
    
    
    
    

   
   public function getuserlikes($user_id){
$rez= Yii::$app->db->createCommand('SELECT p.ava, p.name, u.nickname, usinf.ava as userava FROM project p, user u, likes l, userinfo usinf  WHERE l.user_id=:user_id AND l.proj_id= p.id AND u.id=p.user_id AND usinf.user_id=u.id')->bindValues([':user_id'=>$user_id])->queryAll();
   return $rez;
   }
   
    public function get_all_likes(){
       $rez= Yii::$app->db->createCommand("SELECT count(*) as count,l.proj_id  FROM likes l,project p WHERE p.publish=1 AND p.id=l.proj_id GROUP BY l.proj_id")->queryAll();
       return $rez;
       
   }
   
    public function get_likes($proj){
       
       $rez= Yii::$app->db->createCommand("SELECT count(l.user_id) as count  FROM likes l WHERE l.proj_id=:proj_id GROUP BY l.proj_id")->bindValues([':proj_id'=>$proj])->queryOne();
       return $rez['count'];
       
   }
   

}
