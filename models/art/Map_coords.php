<?php

namespace app\models\art;

use Yii;
use yii\base\Model;


Class Map_coords extends \yii\db\ActiveRecord {
    
           public function get_time_zone($user_id){
           $rez=Yii::$app->db->createCommand('SELECT time_zone FROM map_coords  WHERE user_id=:user_id')->bindValues([':user_id'=>$user_id])->queryOne();  
                 return $rez['time_zone'];
        }
    
        public function set_time_zone($user_id,$time_zone){
           $rez=Yii::$app->db->createCommand('UPDATE map_coords SET time_zone=:time_zone WHERE user_id=:user_id')->bindValues([':user_id'=>$user_id,':time_zone'=>$time_zone])->execute();  
        
        }

         public function find_map_coord($user_id){
           $rez=Yii::$app->db->createCommand('SELECT id FROM map_coords WHERE user_id=:user_id ')->bindValues([':user_id'=>$user_id])->query()->count();
return $rez;

        }
    
    public function insert_map_coord($user_id,$lat,$lng,$loc){
    $rez=Yii::$app->db->createCommand('INSERT INTO map_coords(user_id,lat,lng,location)VALUES (:user_id,:lat,:lng,:loc)')->bindValues([':user_id'=>$user_id,':lat'=>$lat,':lng'=>$lng,':loc'=>$loc])->execute();

}

public function update_map_coord($user_id,$lat,$lng,$loc){
    $rez=Yii::$app->db->createCommand('UPDATE map_coords SET lat=:lat,lng=:lng,location=:loc WHERE user_id=:user_id')->bindValues([':user_id'=>$user_id,':lat'=>$lat,':lng'=>$lng,':loc'=>$loc])->execute();
    
    
}


         public function select_map_coord($user_id){
           $rez=Yii::$app->db->createCommand('SELECT * FROM map_coords WHERE user_id=:user_id ')->bindValues([':user_id'=>$user_id])->queryOne();
return $rez;

        }



}
