<?php
namespace app\models\art;
use Yii;
use yii\base\Model;
use yii\db\Query;

class Project_area extends \yii\db\ActiveRecord {
    
            public $path;
            public $position;



            public function setarea($arealist){

                Yii::$app->db->createCommand("INSERT INTO project_area(position,textarea,proj_id) VALUES ".$arealist)->execute(); 

        }
        
           public function delarea($area_arr,$proj_id){

                Yii::$app->db->createCommand("DELETE FROM project_area WHERE proj_id=:proj_id AND id IN(:area_arr)")->bindValues([':area_arr'=>$area_arr,':proj_id'=>$proj_id])->execute(); 

        }
        
      public function delarea_list($area_list,$proj_id){

                Yii::$app->db->createCommand("DELETE FROM project_area WHERE proj_id=:proj_id AND id IN(".$area_list.")")->bindValues([':proj_id'=>$proj_id])->execute(); 

        }
        
        public function update_area_pos($itemarr){
            
              Yii::$app->db->createCommand("INSERT INTO project_area (id,position,textarea,proj_id) VALUES ".$itemarr." ON DUPLICATE KEY UPDATE  project_area.position = VALUES(position)")->execute(); 
        }
        
               public function update_area_val($itemarr){
            
              Yii::$app->db->createCommand("INSERT INTO project_area (id,position,textarea,proj_id) VALUES ".$itemarr." ON DUPLICATE KEY UPDATE  project_area.textarea = VALUES(textarea)")->execute(); 
        }
}
