<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Project_foto extends \yii\db\ActiveRecord {
    
    public $path;
    public $position;
    
       public function setfoto($fotolist){
        
        Yii::$app->db->createCommand("INSERT INTO project_foto(path,position,project_id) VALUES ".$fotolist)->execute(); 
        
}

       public function delfoto($foto_id_arr,$proj_id){
        
        Yii::$app->db->createCommand("DELETE FROM project_foto WHERE project_id=:proj_id AND id IN(".$foto_id_arr.")")->bindValues([':proj_id'=>$proj_id])->execute(); 
        
        }
        
      public function update_foto_pos($itemarr){
            
              Yii::$app->db->createCommand("INSERT INTO project_foto (id,path,project_id,position) VALUES ".$itemarr." ON DUPLICATE KEY UPDATE  project_foto.position = VALUES(position)")->execute(); 
        }
}
