<?php
namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query; 

class Bookmarks{
    
   public function setprojectbookmark($project_id,$user_id){
       
    $rez=Yii::$app->db->createCommand("INSERT INTO bookmarks(project_id,user_id) VALUES (:project_id, :user_id)")->bindValues([':project_id'=>$project_id,':user_id'=>$user_id])->execute();

}


public function findbookmark($user_id,$proj_id){
    
   $rez= Yii::$app->db->createCommand('SELECT * FROM bookmarks WHERE project_id=:proj_id AND  user_id=:user_id')->bindValues([':proj_id'=>$proj_id,':user_id'=>$user_id])->query()->count();
if($rez==0){return 0;}else{return 1;}
}

public function getbookmarksfromuser($user_id){
    
   $proj_rez= Yii::$app->db->createCommand('SELECT p.ava, p.name, u.nickname, usinf.ava as userava FROM project p, user u, bookmarks b, userinfo usinf  WHERE b.user_id=:user_id AND p.id= b.project_id AND u.id=p.user_id AND usinf.user_id=u.id')->bindValues([':user_id'=>$user_id])->queryAll();
   return $proj_rez;
}


public function show_no_submitted_friends($user_id){
    
  
 
}



}