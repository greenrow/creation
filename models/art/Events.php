<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Events {
    
   public function setevent_addfriend($user_id){

       $count=Yii::$app->db->createCommand('SELECT user_id FROM events WHERE user_id=:user_id')->bindValues([':user_id'=>$user_id])->query()->count();
    
       if ($count ==0){ Yii::$app->db->createCommand("INSERT INTO events(user_id) VALUES(:user_id)")->bindValues([':user_id'=>$user_id])->execute();}
       else{
           $rez= Yii::$app->db->createCommand("UPDATE events SET friends = friends+1 WHERE user_id=:user_id ")->bindValues([':user_id'=>$user_id])->execute();   
       }

}

    
   public function setevent_addmessage($user_id){
       
       $count=Yii::$app->db->createCommand('SELECT user_id FROM events WHERE user_id=:user_id')->bindValues([':user_id'=>$user_id])->query()->count();
    
       if ($count ==0){ Yii::$app->db->createCommand("INSERT INTO events(user_id,message) VALUES(:user_id,message+1)")->bindValues([':user_id'=>$user_id])->execute();}
       else{
           $rez= Yii::$app->db->createCommand("UPDATE events SET message = message+1 WHERE user_id=:user_id ")->bindValues([':user_id'=>$user_id])->execute();   
       }

}
public function show_all_events($user_id){

    $no_viewed_message=Yii::$app->db->createCommand("SELECT MAX(m.friend_date) as date,m.sender_id AS id, COUNT(m.viewed) AS count,usinfo.ava,u.login FROM message m,user u, userinfo usinfo WHERE m.sender_id IN(SELECT DISTINCT m.sender_id FROM message WHERE m.receiver_id=:user_id) AND m.receiver_id=:user_id AND usinfo.user_id =m.sender_id AND u.id = m.sender_id AND m.viewed=0 GROUP BY m.sender_id ;")->bindValues([':user_id'=>$user_id])->queryAll();
    $no_submitted_friends=Yii::$app->db->createCommand("SELECT f.date as date,f.user_id AS id, usinfo.ava,u.login FROM friends f,user u, userinfo usinfo WHERE f.user_id IN(SELECT DISTINCT f.user_id FROM message WHERE f.user_id=:user_id) AND f.user_id=:user_id AND usinfo.user_id =f.user_id  AND u.id = f.user_id  AND f.submit_friend=0 GROUP BY f.user_id ;")->bindValues([':user_id'=>$user_id])->queryAll();
    $allevents=array_merge($no_viewed_message,$no_submitted_friends);
 return $allevents;
   
}

public function show_count_events($user_id){
    
    $messagecount=Yii::$app->db->createCommand("SELECT COUNT(viewed) AS messcount FROM message WHERE receiver_id =:user_id AND viewed=0")->bindValues([':user_id'=>$user_id])->queryOne();
      $friendcount=Yii::$app->db->createCommand("SELECT COUNT(submit_friend) AS friendcount FROM friends WHERE user_id=:user_id AND submit_friend=0")->bindValues([':user_id'=>$user_id])->queryOne();
    $rez=$messagecount['messcount']+$friendcount['friendcount'];

 return $rez;
    
}

public function no_viewed_message($user_id){
    
 $messagecount=Yii::$app->db->createCommand("SELECT COUNT(viewed) AS messcount FROM message WHERE receiver_id =:user_id")->bindValues([':user_id'=>$user_id])->queryOne();

     if ($messagecount['messcount']>0){   return   $messagecount['messcount'];}
}



public function show_no_submitted_friends($user_id){
    
    $friendcount=Yii::$app->db->createCommand("SELECT COUNT(submit_friend) AS friendcount FROM friends WHERE user_id =:user_id AND submit_friend =0")->bindValues([':user_id'=>$user_id])->queryOne();
    if ($friendcount['friendcount'] >0){   return     $friendcount['friendcount'];}
 
}



}