<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Categ_projects extends \yii\db\ActiveRecord {
    
        
 public function del_category($categ_id_arr,$proj_id){
     
     $rez=Yii::$app->db->createCommand('DELETE FROM categ_projects WHERE proj_id=:proj_id AND categ_id IN('.$categ_id_arr.')')->bindValues([':proj_id'=>$proj_id])->execute(); 
 }


               public function setcateg_proj($categ_id,$proj_id){

                Yii::$app->db->createCommand("INSERT INTO categ_projects(categ_id,proj_id) VALUES(:categ_id,:proj_id)")->bindValues([':categ_id'=>$categ_id,':proj_id'=>$proj_id])->execute(); 

        }
        
           public function get_cat($proj_id){
            $rez=Yii::$app->db->createCommand("SELECT cat.name FROM category cat,categ_projects proj_cat WHERE proj_cat.categ_id=cat.id AND proj_cat.proj_id=:projid")->bindValues([':projid'=>$proj_id])->query()->readAll(); 
       return $rez;
        }
        
  
}