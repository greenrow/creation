<?php namespace app\models\art;
use Yii;
use yii\base\Model;
use yii\db\Query;

Class Recoverpssw{
    
    
      public function getuserid($email){
        
        
         $userid=Yii::$app->db->createCommand('SELECT id FROM user WHERE email=:email')->bindValues([':email'=>$email])->queryOne();
        return $userid['id'];
    }
    
    public function getusernick($email){
        
        
         $userid=Yii::$app->db->createCommand('SELECT nickname FROM user WHERE email=:email')->bindValues([':email'=>$email])->queryOne();
        return $userid['nickname'];
    }
    
    
    
    public function setdata($userid,$hash,$date){
        
          $userid=Yii::$app->db->createCommand('INSERT INTO recoverpssw(user_id,hash,date) VALUES(:userid,:hash,:date)')->bindValues([':userid'=>$userid,':hash'=>$hash,':date'=>$date])->execute();
        
        
    }
}