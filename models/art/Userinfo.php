<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Userinfo extends \yii\db\ActiveRecord {
    

    
     public $file;
     public $job_place;
     public $name;
    public $age;
    public $user_id;
    public $aboutme;
    public  $website;
    public static function tablename(){

        return 'userinfo';
    }


    public function rules(){


           return [

                [['file','job_place','age','user_id','aboutme','website'],'safe']


            ];
    }


    public function load_ava($file){


        $rez= Yii::$app->db->createCommand('SELECT ava FROM userinfo WHERE user_id=:user_id AND ava !="" ')->bindValue('user_id',Yii::$app->user->getid())->query()->count();

        if ($rez==0){

        Yii::$app->db->createCommand("INSERT INTO userinfo(user_id,ava) VALUES(:user_id,:file)")->bindValues([':user_id'=>Yii::$app->user->getid(),':file'=>$file])->execute();
    }else{
        Yii::$app->db->createCommand('UPDATE userinfo SET ava=:file WHERE user_id=:user_id')->bindValues([':user_id'=>Yii::$app->user->getid(),':file'=>$file])->execute();
    }
    }

    public function getAvatar(){


        $rez=Userinfo::find()->where(['user_id'=>Yii::$app->user->getid()])->asArray()->one();
    return $rez['ava'];

    }
    
    //поля инпут//
    
          public function setbackground($back){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET background=:back WHERE user_id=:user_id")->bindValues([':back'=>$back,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
    
              public function setuser_id_in_info(){
        
          Yii::$app->db->createCommand("INSERT INTO userinfo(user_id) VALUES(:user_id)")->bindValues([':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
    

    
        public function setsurname($surname){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET surname=:surname WHERE user_id=:user_id")->bindValues([':surname'=>$surname,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
            public function updatename($name){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET name=:name WHERE user_id=:user_id")->bindValues([':name'=>$name,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    

    
    public function setage($age){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET age=:age WHERE user_id=:user_id")->bindValues([':age'=>$age,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
        public function setwebsite($web){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET website=:web WHERE user_id=:user_id")->bindValues([':web'=>$web,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
            public function setemail($email){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET email=:email WHERE user_id=:user_id")->bindValues([':email'=>$email,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
            public function setjob($job){
        
          Yii::$app->db->createCommand("UPDATE userinfo SET job_place=:job WHERE user_id=:user_id")->bindValues([':job'=>$job,':user_id'=>Yii::$app->user->getid()])->execute();
    }
    
    
    
    
        public function getuserinfo($user_id){
        
            $user_info=Yii::$app->db->createCommand("SELECT * FROM userinfo WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->queryOne();

            $user_location=Yii::$app->db->createCommand("SELECT location FROM map_coords WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->queryOne();
              $user_nick=Yii::$app->db->createCommand("SELECT nickname FROM user WHERE id=:user_id")->bindValues([':user_id'=>$user_id])->queryOne();


            $user_info['location']= $user_location['location'];
               $user_info['nickname']=  $user_nick['nickname'];
            
             return        $user_info;
    }
    
    public function set_general_settings($coll){
      Yii::$app->db->createCommand("UPDATE userinfo SET :coll=0 WHERE user_id=:user_id")->bindValues([':coll'=>$coll,':user_id'=>Yii::$app->user->getid()])->execute();
        
    }
    public function clearrow($namefield,$user_id){
    
      Yii::$app->db->createCommand("UPDATE userinfo SET {$namefield} = NULL WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->execute();
    }
}
