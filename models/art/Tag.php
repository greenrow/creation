<?php namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Tag{
    
        

        public function find_tag_count($name){
                $count=Yii::$app->db->createCommand('Select id From tag WHERE name=:name')->bindValues([':name'=>$name])->query();
                return count($count);
        }

        public  function settag($name){
                Yii::$app->db->createCommand("INSERT INTO tag(name) VALUES(:name)")->bindValues([':name'=>$name])->execute(); 
        }
        
        public  function gettag_id($name){
            $tag_id= Yii::$app->db->createCommand('Select id From tag Where name=:name')->bindValues([':name'=>$name])->queryOne(); 
             return $tag_id['id'];
        }
        
        public function gettag_name($proj_id){
                $rez=Yii::$app->db->createCommand("Select tag.name FROM tag,project_tag projtag  WHERE projtag.tag_id=tag.id AND projtag.proj_id=:proj_id")->bindValues([':proj_id'=>$proj_id])->query(); 
                return $rez;
        }


        public function removetag($tagarr){
            
              Yii::$app->db->createCommand("DELETE FROM tag WHERE id IN(:tagarr)")->bindValue(':tagarr',$tagarr)->execute(); 
        }
        
}
        
