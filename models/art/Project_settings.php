<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Project_settings{
    
    public function set($proj_id,$showrule,$comment_rule,$age_rez){
        
        $count= Yii::$app->db->createCommand('SELECT id FROM project_settings WHERE proj_id=:proj_id')->bindValue(':proj_id',$proj_id)->query()->count();
        
        if($count ==0){
            
          $rez= Yii::$app->db->createCommand('INSERT INTO project_settings(proj_id,show_rule,comment_rule,age_rule) VALUES (:proj_id,:showrule,:comment_rule,:age_rule)')->bindValues([':proj_id'=>$proj_id,':showrule'=>$showrule,':comment_rule'=>$comment_rule,':age_rule'=>$age_rez])->execute();  
        }else{
            
            $rez= Yii::$app->db->createCommand('UPDATE project_settings SET show_rule=:showrule, comment_rule=:comment_rule, age_rule=:age_rule WHERE proj_id=:proj_id')->bindValues([':proj_id'=>$proj_id,':showrule'=>$showrule,':comment_rule'=>$comment_rule,':age_rule'=>$age_rez])->execute();
        }
        
    }
    
    
        public function get($proj_id){
        $rez= Yii::$app->db->createCommand('SELECT * FROM project_settings WHERE proj_id=:proj_id')->bindValues([':proj_id'=>$proj_id])->queryOne();
        return $rez;
    }
  
}