<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Settings{
    



        
public function set_user_id($user_id){
    
               
                $rez = Yii::$app->db->createCommand('INSERT INTO settings(user_id) VALUES (:user_id) ')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
    
}

       public function getuserid($user_id){
        
          $rez=Yii::$app->db->createCommand("SELECT COUNT(id) AS COUNT FROM general_settings WHERE info_user_id=:user_id")->bindValues([':user_id'=>$user_id])->query()->read();
          return $rez['COUNT'];
    }
    
    public function get_all_info($user_id){
           $rez=Yii::$app->db->createCommand("SELECT * FROM settings WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->queryOne();
          return $rez;
        
    }
    
    public function set_show_perspage_setting($user_id,$val){
        
              $rez = Yii::$app->db->createCommand('UPDATE settings SET personal_page_rule =:val WHERE user_id=:user_id ')->bindValues([':user_id'=>$user_id,':val'=>$val])->execute(); 
  
    }
     public function set_show_project_setting($user_id,$val){
        
              $rez = Yii::$app->db->createCommand('UPDATE settings SET project_rule=:val WHERE user_id=:user_id ')->bindValues([':user_id'=>$user_id,':val'=>$val])->execute(); 
  
    }
    
    public function set_comment_setting($user_id,$val){
        
              $rez = Yii::$app->db->createCommand('UPDATE settings SET comment_rule=:val WHERE user_id=:user_id ')->bindValues([':user_id'=>$user_id,':val'=>$val])->execute(); 
  
    }
    public function get_all_project_rules(){
        
        
        $rez= Yii::$app->db->createCommand('SELECT * FROM Settings')->queryAll();
        return $rez;
    }
    
    
}
