<?php

namespace app\models\art;

use Yii;


Class Interests extends \yii\db\ActiveRecord {
    
        

    
           public function count_interests($user_id){
     
$rez= Yii::$app->db->createCommand("SELECT id FROM interests WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->query()->count();
       return $rez;
        }
          
          public  function set_interests($user_id,$interes){
     
              Yii::$app->db->createCommand("INSERT INTO interests (user_id,interes_val) VALUES(:user_id,:interes_val)")
                      ->bindValues([':user_id'=>$user_id,':interes_val'=>$interes])->execute(); 
        }
        
        
        
                 public  function update_interests($user_id,$interes){
     
              Yii::$app->db->createCommand("UPDATE interests SET interes_val=:interes WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id,':interes'=>$interes])->execute(); 
        }
           
   
   
   public function get_interest($user_id){
              $rez= Yii::$app->db->createCommand("SELECT * FROM interests WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->queryAll();
       return $rez;
       
   }
   
   
      public function del_interest($interes_id){
       
       $rez= Yii::$app->db->createCommand("DELETE  FROM interests WHERE id=:interes_id")->bindValues([':interes_id'=>$interes_id])->execute();
    
       
   }

}
