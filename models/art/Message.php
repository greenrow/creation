<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Message extends \yii\db\ActiveRecord {
    
    
    public static function tableName()
    {
        return 'message';
    }
    
    
   public function insertMessage($sender_id,$receiver_id,$text,$date,$friend_date,$message_id){
       

    
  Yii::$app->db->createCommand("INSERT INTO message(sender_id,receiver_id,text,date,friend_date,message_id) VALUES(:sender_id,:receiver_id,:text,:date,:friend_date,:message_id)")->bindValues([':sender_id'=>$sender_id,':receiver_id'=>$receiver_id,':text'=>$text,':date'=>$date,'friend_date'=>$friend_date,'message_id'=>$message_id])->execute();
     

}

   public function updateViewMessage($my_id,$who_send_id,$text){
       

    
  Yii::$app->db->createCommand("UPDATE message SET viewed=0 WHERE")->bindValues([':user_id'=>$user_id,':who_send_id'=>$who_send_id,':text'=>$text])->execute();
     

}

public function getmessages($user_id,$friend_id){
    
    $my_messages=Yii::$app->db->createCommand("SELECT u.nickname,usinfo.ava,m.id,m.text,m.sender_id,m.receiver_id,m.date AS date,m.viewed FROM user u,message m,userinfo usinfo WHERE m.sender_id=:user_id AND m.sender_id=usinfo.user_id  AND m.receiver_id=:friend_id AND m.sender_id=u.id")->bindValues([':user_id'=>$user_id,':friend_id'=>$friend_id])->queryAll();
    $friend_message=Yii::$app->db->createCommand("SELECT u.nickname,usinfo.ava,m.id,m.text,m.sender_id,m.receiver_id,m.friend_date AS date,m.viewed FROM user u,message m,userinfo usinfo WHERE m.sender_id=:friend_id AND m.sender_id=usinfo.user_id  AND m.receiver_id=:user_id  AND m.sender_id=u.id")->bindValues([':user_id'=>$user_id,':friend_id'=>$friend_id])->queryAll();
     /*сортируем*/
    $nickname=$this->find_nickname_by_id($friend_id);
//    if(sizeof($my_messages)>0 & sizeof($friend_message)>0 ){array_multisort($my_messages,SORT_ASC,$friend_message);}
 
    $allmessage['mes']=array_merge($my_messages,$friend_message);
    if(sizeof($allmessage)>0){
        $allmessage['nickname']=$nickname;
        array_multisort($allmessage,SORT_ASC);
        return  $allmessage;
    }

    
}

    public function find_nickname_by_id($id){
        
        $user =Yii::$app->db->createCommand('SELECT nickname FROM user WHERE id=:id')->bindvalue(':id',$id)->queryOne();
  

        return $user;
    }



public function getmessagelist($user_id){
    $messagelist=Yii::$app->db->createCommand("SELECT m.receiver_id AS friend_id,MAX(m.friend_date) as date,m.sender_id AS id,m.text ,m.viewed AS view,usinfo.ava,u.nickname FROM message m,user u, "
            . "userinfo usinfo WHERE  m.friend_date IN (SELECT MAX(m.friend_date) FROM message m WHERE m.receiver_id=:user_id  AND m.sender_id IN(SELECT  m.sender_id FROM message WHERE m.receiver_id=:user_id) GROUP BY m.sender_id)"
            . " AND  m.receiver_id=:user_id AND usinfo.user_id =m.sender_id  AND u.id = m.sender_id GROUP BY m.sender_id;")->bindValues([':user_id'=>$user_id])->queryAll();

    return   $messagelist;
}

}