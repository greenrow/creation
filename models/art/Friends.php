<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Friends {
    
    public function getnamebyid($user_id){
        $rez=ii::$app->db->createCommand('SELECT nickname u FROM user JOIN friends f ON f.friWHERE user_id=:user_id AND friend_id=:friend_id')->bindValues([':user_id'=>$user_id,':friend_id'=>$friend_id])->query()->count();
        
        
    }
    
    public function setfriend($user_id,$friend_id,$friend_date){
       
                   $find_this_friend=Yii::$app->db->createCommand('SELECT friend_id FROM friends WHERE user_id=:user_id AND friend_id=:friend_id')->bindValues([':user_id'=>$user_id,':friend_id'=>$friend_id])->query()->count();
                   if($find_this_friend ==0){
       
                        $rez= Yii::$app->db->createCommand("INSERT INTO friends(user_id,friend_id,date) VALUES(:user_id,:friend_id,:friend_date) ")->bindValues([':user_id'=>$user_id,':friend_id'=>$friend_id,':friend_date'=>$friend_date])->execute(); 
                        return $rez;
           
                   }else{
                       return false;
                   }
       
   }
   public function getacceptedFriends($user_id){
                     $rez= Yii::$app->db->createCommand('SELECT u.id,u.nickname,ui.ava FROM user u JOIN userinfo ui ON ui.user_id= u.id JOIN  friends f ON u.id=f.user_id OR  u.id=f.friend_id WHERE u.id != :user_id  AND (f.friend_id=:user_id OR f.user_id=:user_id) AND f.submit_friend="1"  ')->bindValue(':user_id',$user_id)->queryAll();
        return $rez;
   }
   public function getfriends($user_id){
       $allfreinds=[];
          $allfreinds['accept_friends']=Yii::$app->db->createCommand('SELECT f.submit_friend,f.user_id,u.nickname,ui.ava FROM user u,userinfo ui,friends f  WHERE f.friend_id=:user_id AND u.id=f.user_id AND ui.user_id=f.user_id')->bindValue(':user_id',$user_id)->queryAll();
       $allfreinds['request_friends']= Yii::$app->db->createCommand('SELECT f.submit_friend,f.friend_id,u.nickname,ui.ava FROM user u,userinfo ui,friends f  WHERE f.user_id=:user_id AND u.id=f.friend_id AND ui.user_id=f.friend_id')->bindValue(':user_id',$user_id)->queryAll();
              $late1= Yii::$app->db->createCommand('SELECT f.submit_friend,f.user_id,u.nickname,ui.ava FROM user u,userinfo ui,friends f  WHERE f.friend_id=:user_id AND u.id=f.user_id AND ui.user_id=f.user_id AND f.submit_friend="1" ORDER BY f.date LIMIT 7')->bindValue(':user_id',$user_id)->queryAll();
               $late2= Yii::$app->db->createCommand('SELECT f.submit_friend,f.friend_id,u.nickname,ui.ava FROM user u,userinfo ui,friends f  WHERE f.user_id=:user_id AND u.id=f.friend_id AND ui.user_id=f.friend_id AND f.submit_friend="1" ORDER BY f.date LIMIT 7')->bindValue(':user_id',$user_id)->queryAll();
       $lately=array_merge($late1,  $late2);
         $allfreinds['lately']=$lately;
          return   $allfreinds;
   }
   public function check_friend($userid,$friendid){
       
       $query=Yii::$app->db->createCommand('Select submit_friend as rez FROM friends WHERE user_id=:user_id or user_id=:friend_id AND friend_id=:friend_id or friend_id=:user_id')->bindValues([":user_id"=>$userid,":friend_id"=>$friendid])->queryOne();
       if ($query['rez'] == null){$query['rez']=3;}
       return $query['rez'];
   }
   
      public function acceptfriends($friend_id,$user_id){
       
       $rez=Yii::$app->db->createCommand('UPDATE friends SET submit_friend=1 WHERE friend_id=:user_id AND user_id=:friend_id')->bindValues([':friend_id'=>$friend_id,':user_id'=>$user_id])->execute();
        $rez=Yii::$app->db->createCommand('UPDATE friends SET submit_friend=1 WHERE friend_id=:friend_id AND user_id=:user_id')->bindValues([':friend_id'=>$friend_id,':user_id'=>$user_id])->execute();
       return $rez;
   }


}