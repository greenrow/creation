<?php

namespace app\models\art;
use Yii;
use yii\base\Model;
use yii\db\Query;
class News {

    public function set_news_project($user_id,$project_id){
        
        $rez= Yii::$app->db->createCommand("INSERT INTO news(user_id,project_id) VALUES(:user_id,:project_id)")->bindValues([':user_id'=>$user_id,':project_id'=>$project_id])->execute();
       
   }

   
       public function getnews($user_id){
        
        $rez= Yii::$app->db->createCommand("SELECT * FROM news WHERE user_id=:user_id")->bindValues([':user_id'=>$user_id])->queryAll();
        return $rez;
       
   }


}
